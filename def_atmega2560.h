#ifndef __TOS_DEF_ATMEGA2560_H__
#define __TOS_DEF_ATMEGA2560_H__

#ifndef __AVR_ATmega2560__
	#error "MCU is not ATmega2560"
#endif

#ifndef __TOS_LIB_PIN_H__
	#error "#include <tos/lib_pin.h>"
#endif

// TIMER

#define PIN_OC0A(_A)     __JOIN2(B7, _A)
#define PIN_OC0B(_A)     __JOIN2(G5, _A)
#define PIN_OC1A(_A)     __JOIN2(B5, _A)
#define PIN_OC1B(_A)     __JOIN2(B6, _A)
#define PIN_OC1C(_A)     __JOIN2(B7, _A)
#define PIN_ICP1(_A)     __JOIN2(D4, _A)
#define PIN_OC2A(_A)     __JOIN2(B3, _A)
#define PIN_OC2B(_A)     __JOIN2(H6, _A)
#define PIN_OC3A(_A)     __JOIN2(E3, _A)
#define PIN_OC3B(_A)     __JOIN2(E4, _A)
#define PIN_OC3C(_A)     __JOIN2(E5, _A)
#define PIN_ICP3(_A)     __JOIN2(E7, _A)
#define PIN_OC4A(_A)     __JOIN2(H3, _A)
#define PIN_OC4B(_A)     __JOIN2(H4, _A)
#define PIN_OC4C(_A)     __JOIN2(H5, _A)
#define PIN_ICP4(_A)     __JOIN2(L0, _A)
#define PIN_OC5A(_A)     __JOIN2(L3, _A)
#define PIN_OC5B(_A)     __JOIN2(L4, _A)
#define PIN_OC5C(_A)     __JOIN2(L5, _A)
#define PIN_ICP5(_A)     __JOIN2(L1, _A)

// USART

#define PIN_RXD0(_A)     __JOIN2(E0, _A)
#define PIN_TXD0(_A)     __JOIN2(E1, _A)
#define PIN_XCK0(_A)     __JOIN2(E2, _A)
#define PIN_RXD1(_A)     __JOIN2(D2, _A)
#define PIN_TXD1(_A)     __JOIN2(D3, _A)
#define PIN_XCK1(_A)     __JOIN2(D5, _A)
#define PIN_RXD2(_A)     __JOIN2(H0, _A)
#define PIN_TXD2(_A)     __JOIN2(H1, _A)
#define PIN_XCK2(_A)     __JOIN2(H2, _A)
#define PIN_RXD3(_A)     __JOIN2(J0, _A)
#define PIN_TXD3(_A)     __JOIN2(J1, _A)
#define PIN_XCK3(_A)     __JOIN2(J2, _A)

// SPI

#define PIN_SCK(_A)      __JOIN2(B1, _A)
#define PIN_SS(_A)       __JOIN2(B0, _A)
#define PIN_MOSI(_A)     __JOIN2(B2, _A)
#define PIN_MISO(_A)     __JOIN2(B3, _A)

// TWI

#define PIN_SDA(_A)      __JOIN2(D1, _A)
#define PIN_SCL(_A)      __JOIN2(D0, _A)

// INT

#define PIN_INT0(_A)     __JOIN2(D0, _A)
#define PIN_INT1(_A)     __JOIN2(D1, _A)
#define PIN_INT2(_A)     __JOIN2(D2, _A)
#define PIN_INT3(_A)     __JOIN2(D3, _A)
#define PIN_INT4(_A)     __JOIN2(E4, _A)
#define PIN_INT5(_A)     __JOIN2(E5, _A)
#define PIN_INT6(_A)     __JOIN2(E6, _A)
#define PIN_INT7(_A)     __JOIN2(E7, _A)

// PCINT

#define PIN_PCINT0(_A)   __JOIN2(H6, _A)
#define PIN_PCINT1(_A)   __JOIN2(B0, _A)
#define PIN_PCINT2(_A)   __JOIN2(B1, _A)
#define PIN_PCINT3(_A)   __JOIN2(B2, _A)
#define PIN_PCINT4(_A)   __JOIN2(B4, _A)
#define PIN_PCINT5(_A)   __JOIN2(B5, _A)
#define PIN_PCINT6(_A)   __JOIN2(B6, _A)
#define PIN_PCINT7(_A)   __JOIN2(B7, _A)
#define PIN_PCINT9(_A)   __JOIN2(J0, _A)
#define PIN_PCINT10(_A)  __JOIN2(J1, _A)
#define PIN_PCINT11(_A)  __JOIN2(J2, _A)
#define PIN_PCINT12(_A)  __JOIN2(J3, _A)
#define PIN_PCINT13(_A)  __JOIN2(J4, _A)
#define PIN_PCINT14(_A)  __JOIN2(J5, _A)
#define PIN_PCINT15(_A)  __JOIN2(J6, _A)
#define PIN_PCINT16(_A)  __JOIN2(K0, _A)
#define PIN_PCINT17(_A)  __JOIN2(K1, _A)
#define PIN_PCINT18(_A)  __JOIN2(K2, _A)
#define PIN_PCINT19(_A)  __JOIN2(K3, _A)
#define PIN_PCINT20(_A)  __JOIN2(K4, _A)
#define PIN_PCINT21(_A)  __JOIN2(K5, _A)
#define PIN_PCINT22(_A)  __JOIN2(K6, _A)
#define PIN_PCINT23(_A)  __JOIN2(K7, _A)

// ADC

#define PIN_ADC0(_A)     __JOIN2(F0, _A)
#define PIN_ADC1(_A)     __JOIN2(F1, _A)
#define PIN_ADC2(_A)     __JOIN2(F2, _A)
#define PIN_ADC3(_A)     __JOIN2(F3, _A)
#define PIN_ADC4(_A)     __JOIN2(F4, _A)
#define PIN_ADC5(_A)     __JOIN2(F5, _A)
#define PIN_ADC6(_A)     __JOIN2(F6, _A)
#define PIN_ADC7(_A)     __JOIN2(F7, _A)
#define PIN_ADC8(_A)     __JOIN2(K0, _A)
#define PIN_ADC9(_A)     __JOIN2(K1, _A)
#define PIN_ADC10(_A)    __JOIN2(K2, _A)
#define PIN_ADC11(_A)    __JOIN2(K3, _A)
#define PIN_ADC12(_A)    __JOIN2(K4, _A)
#define PIN_ADC13(_A)    __JOIN2(K5, _A)
#define PIN_ADC14(_A)    __JOIN2(K6, _A)
#define PIN_ADC15(_A)    __JOIN2(K7, _A)

#endif // __TOS_DEF_ATMEGA328P_H__

#include "config.h"

// F_CPU 16MHz
#define TIMER_CS   0b00000011
#define TIMER_OCR  25

__attribute__((OS_task))
int main()
{
	TCNT0 = 0;
	OCR0A = ( TIMER_OCR - 1 );
	TIMSK0
		= ( 0 << OCIE0B )
		| ( 1 << OCIE0A )
		| ( 0 << TOIE0  );
	TCCR0A
		= ( 0 << COM0A1 )
		| ( 0 << COM0A0 )
		| ( 0 << COM0B1 )
		| ( 0 << COM0B0 )
		| ( 1 << WGM01  )
		| ( 0 << WGM00  );
	TCCR0B
		= ( 0 << FOC0A  )
		| ( 0 << FOC0B  )
		| ( 0 << WGM02  )
		| ( TIMER_CS << CS00   );
}

#define BLINK_A_PIN    B4H
#define BLINK_A_DELAY  MSEC(500)

THREAD(blink_a, 0x10)
{
	DRIVER(BLINK_A_PIN, OUT);
	ON(BLINK_A_PIN);
	while( 1 )
	{
		sleep(BLINK_A_DELAY);
		TOGGLE(BLINK_A_PIN);
	}
}

#define BLINK_B_PIN    B5H
#define BLINK_B_DELAY  SEC(1)

THREAD(blink_b, 0x10)
{
	DRIVER(BLINK_B_PIN, OUT);
	ON(BLINK_B_PIN);
	while( 1 )
	{
		sleep(BLINK_B_DELAY);
		TOGGLE(BLINK_B_PIN);
	}
}

THREAD_LIST
(
	THREAD_ENTRY(blink_a),
	THREAD_ENTRY(blink_b),
)

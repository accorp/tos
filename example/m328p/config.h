#ifndef __CONFIG_H__
#define __CONFIG_H__

#define THREAD_EXT_SIZE  5
#define STACK_BASE_SIZE  0
#include <tos/kernel.h>

#define TIMER_VECT      TIMER0_COMPA_vect
#define TIMER_FREQ      10000L
#include <tos/ext_timer.h>

#include <tos/lib_pin.h>
#include <tos/def_atmega328p.h>

#endif // __CONFIG_H__

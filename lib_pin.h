#ifndef __TOS_LIB_PIN_H__
#define __TOS_LIB_PIN_H__

// GENERATED FILE. DO NOT EDIT.

#define BITNUM(_pin)                 BITNUM_IMP(_pin)
#define BITMASK(_pin)                BITMASK_IMP(_pin)
#define DRIVER(_pin, _mode)          DRIVER_IMP(_pin, _mode)
#define ON(_pin)                     ON_IMP(_pin)
#define OFF(_pin)                    OFF_IMP(_pin)
#define ACTIVE(_pin)                 ACTIVE_IMP(_pin)
#define LATCH(_pin)                  LATCH_IMP(_pin)
#define TOGGLE(_pin)                 TOGGLE_IMP(_pin)
#define DDR(_pin)                    DDR_IMP(_pin)
#define PIN(_pin)                    PIN_IMP(_pin)
#define PORT(_pin)                   PORT_IMP(_pin)
#define LEVEL(_pin)                  LEVEL_IMP(_pin)

#define BITNUM_IMP(_pin)            _GEN_BITNUM_  ## _pin
#define BITMASK_IMP(_pin)           _GEN_BITMASK_ ## _pin
#define DRIVER_IMP(_pin, _mode)     _GEN_DRIVER_  ## _pin ## _ ## _mode
#define ON_IMP(_pin)                _GEN_ON_      ## _pin
#define OFF_IMP(_pin)               _GEN_OFF_     ## _pin
#define ACTIVE_IMP(_pin)            _GEN_ACTIVE_  ## _pin
#define LATCH_IMP(_pin)             _GEN_LATCH_   ## _pin
#define TOGGLE_IMP(_pin)            _GEN_TOGGLE_  ## _pin
#define DDR_IMP(_pin)               _GEN_DDR_     ## _pin
#define PIN_IMP(_pin)               _GEN_PIN_     ## _pin
#define PORT_IMP(_pin)              _GEN_PORT_    ## _pin
#define LEVEL_IMP(_pin)             _GEN_LEVEL_   ## _pin

#define _GEN_BITNUM_A0H                              0
#define _GEN_BITMASK_A0H                       (1 << 0)
#define _GEN_DRIVER_A0H_OUT         (DDRA  |=  (1 << 0))
#define _GEN_DRIVER_A0H_IN          (DDRA  &=~ (1 << 0))
#define _GEN_DRIVER_A0H_PULLUP      (PORTA |=  (1 << 0))
#define _GEN_DRIVER_A0H_HIZ         (PORTA &=~ (1 << 0))
#define _GEN_ON_A0H                 (PORTA |=  (1 << 0))
#define _GEN_OFF_A0H                (PORTA &=~ (1 << 0))
#define _GEN_ACTIVE_A0H             (PINA  &   (1 << 0))
#define _GEN_LATCH_A0H              (PORTA &   (1 << 0))
#define _GEN_TOGGLE_A0H             (PINA   =  (1 << 0))
#define _GEN_DDR_A0H                 DDRA
#define _GEN_PIN_A0H                 PINA
#define _GEN_PORT_A0H                PORTA
#define _GEN_LEVEL_A0H               true
#define _GEN_BITNUM_A0L                              0
#define _GEN_BITMASK_A0L                       (1 << 0)
#define _GEN_DRIVER_A0L_OUT         (DDRA  |=  (1 << 0))
#define _GEN_DRIVER_A0L_IN          (DDRA  &=~ (1 << 0))
#define _GEN_DRIVER_A0L_PULLUP      (PORTA |=  (1 << 0))
#define _GEN_DRIVER_A0L_HIZ         (PORTA &=~ (1 << 0))
#define _GEN_ON_A0L                 (PORTA &=~ (1 << 0))
#define _GEN_OFF_A0L                (PORTA |=  (1 << 0))
#define _GEN_ACTIVE_A0L            !(PINA  &   (1 << 0))
#define _GEN_LATCH_A0L             !(PORTA &   (1 << 0))
#define _GEN_TOGGLE_A0L             (PINA   =  (1 << 0))
#define _GEN_DDR_A0L                 DDRA
#define _GEN_PIN_A0L                 PINA
#define _GEN_PORT_A0L                PORTA
#define _GEN_LEVEL_A0L               false

#define _GEN_BITNUM_A1H                              1
#define _GEN_BITMASK_A1H                       (1 << 1)
#define _GEN_DRIVER_A1H_OUT         (DDRA  |=  (1 << 1))
#define _GEN_DRIVER_A1H_IN          (DDRA  &=~ (1 << 1))
#define _GEN_DRIVER_A1H_PULLUP      (PORTA |=  (1 << 1))
#define _GEN_DRIVER_A1H_HIZ         (PORTA &=~ (1 << 1))
#define _GEN_ON_A1H                 (PORTA |=  (1 << 1))
#define _GEN_OFF_A1H                (PORTA &=~ (1 << 1))
#define _GEN_ACTIVE_A1H             (PINA  &   (1 << 1))
#define _GEN_LATCH_A1H              (PORTA &   (1 << 1))
#define _GEN_TOGGLE_A1H             (PINA   =  (1 << 1))
#define _GEN_DDR_A1H                 DDRA
#define _GEN_PIN_A1H                 PINA
#define _GEN_PORT_A1H                PORTA
#define _GEN_LEVEL_A1H               true
#define _GEN_BITNUM_A1L                              1
#define _GEN_BITMASK_A1L                       (1 << 1)
#define _GEN_DRIVER_A1L_OUT         (DDRA  |=  (1 << 1))
#define _GEN_DRIVER_A1L_IN          (DDRA  &=~ (1 << 1))
#define _GEN_DRIVER_A1L_PULLUP      (PORTA |=  (1 << 1))
#define _GEN_DRIVER_A1L_HIZ         (PORTA &=~ (1 << 1))
#define _GEN_ON_A1L                 (PORTA &=~ (1 << 1))
#define _GEN_OFF_A1L                (PORTA |=  (1 << 1))
#define _GEN_ACTIVE_A1L            !(PINA  &   (1 << 1))
#define _GEN_LATCH_A1L             !(PORTA &   (1 << 1))
#define _GEN_TOGGLE_A1L             (PINA   =  (1 << 1))
#define _GEN_DDR_A1L                 DDRA
#define _GEN_PIN_A1L                 PINA
#define _GEN_PORT_A1L                PORTA
#define _GEN_LEVEL_A1L               false

#define _GEN_BITNUM_A2H                              2
#define _GEN_BITMASK_A2H                       (1 << 2)
#define _GEN_DRIVER_A2H_OUT         (DDRA  |=  (1 << 2))
#define _GEN_DRIVER_A2H_IN          (DDRA  &=~ (1 << 2))
#define _GEN_DRIVER_A2H_PULLUP      (PORTA |=  (1 << 2))
#define _GEN_DRIVER_A2H_HIZ         (PORTA &=~ (1 << 2))
#define _GEN_ON_A2H                 (PORTA |=  (1 << 2))
#define _GEN_OFF_A2H                (PORTA &=~ (1 << 2))
#define _GEN_ACTIVE_A2H             (PINA  &   (1 << 2))
#define _GEN_LATCH_A2H              (PORTA &   (1 << 2))
#define _GEN_TOGGLE_A2H             (PINA   =  (1 << 2))
#define _GEN_DDR_A2H                 DDRA
#define _GEN_PIN_A2H                 PINA
#define _GEN_PORT_A2H                PORTA
#define _GEN_LEVEL_A2H               true
#define _GEN_BITNUM_A2L                              2
#define _GEN_BITMASK_A2L                       (1 << 2)
#define _GEN_DRIVER_A2L_OUT         (DDRA  |=  (1 << 2))
#define _GEN_DRIVER_A2L_IN          (DDRA  &=~ (1 << 2))
#define _GEN_DRIVER_A2L_PULLUP      (PORTA |=  (1 << 2))
#define _GEN_DRIVER_A2L_HIZ         (PORTA &=~ (1 << 2))
#define _GEN_ON_A2L                 (PORTA &=~ (1 << 2))
#define _GEN_OFF_A2L                (PORTA |=  (1 << 2))
#define _GEN_ACTIVE_A2L            !(PINA  &   (1 << 2))
#define _GEN_LATCH_A2L             !(PORTA &   (1 << 2))
#define _GEN_TOGGLE_A2L             (PINA   =  (1 << 2))
#define _GEN_DDR_A2L                 DDRA
#define _GEN_PIN_A2L                 PINA
#define _GEN_PORT_A2L                PORTA
#define _GEN_LEVEL_A2L               false

#define _GEN_BITNUM_A3H                              3
#define _GEN_BITMASK_A3H                       (1 << 3)
#define _GEN_DRIVER_A3H_OUT         (DDRA  |=  (1 << 3))
#define _GEN_DRIVER_A3H_IN          (DDRA  &=~ (1 << 3))
#define _GEN_DRIVER_A3H_PULLUP      (PORTA |=  (1 << 3))
#define _GEN_DRIVER_A3H_HIZ         (PORTA &=~ (1 << 3))
#define _GEN_ON_A3H                 (PORTA |=  (1 << 3))
#define _GEN_OFF_A3H                (PORTA &=~ (1 << 3))
#define _GEN_ACTIVE_A3H             (PINA  &   (1 << 3))
#define _GEN_LATCH_A3H              (PORTA &   (1 << 3))
#define _GEN_TOGGLE_A3H             (PINA   =  (1 << 3))
#define _GEN_DDR_A3H                 DDRA
#define _GEN_PIN_A3H                 PINA
#define _GEN_PORT_A3H                PORTA
#define _GEN_LEVEL_A3H               true
#define _GEN_BITNUM_A3L                              3
#define _GEN_BITMASK_A3L                       (1 << 3)
#define _GEN_DRIVER_A3L_OUT         (DDRA  |=  (1 << 3))
#define _GEN_DRIVER_A3L_IN          (DDRA  &=~ (1 << 3))
#define _GEN_DRIVER_A3L_PULLUP      (PORTA |=  (1 << 3))
#define _GEN_DRIVER_A3L_HIZ         (PORTA &=~ (1 << 3))
#define _GEN_ON_A3L                 (PORTA &=~ (1 << 3))
#define _GEN_OFF_A3L                (PORTA |=  (1 << 3))
#define _GEN_ACTIVE_A3L            !(PINA  &   (1 << 3))
#define _GEN_LATCH_A3L             !(PORTA &   (1 << 3))
#define _GEN_TOGGLE_A3L             (PINA   =  (1 << 3))
#define _GEN_DDR_A3L                 DDRA
#define _GEN_PIN_A3L                 PINA
#define _GEN_PORT_A3L                PORTA
#define _GEN_LEVEL_A3L               false

#define _GEN_BITNUM_A4H                              4
#define _GEN_BITMASK_A4H                       (1 << 4)
#define _GEN_DRIVER_A4H_OUT         (DDRA  |=  (1 << 4))
#define _GEN_DRIVER_A4H_IN          (DDRA  &=~ (1 << 4))
#define _GEN_DRIVER_A4H_PULLUP      (PORTA |=  (1 << 4))
#define _GEN_DRIVER_A4H_HIZ         (PORTA &=~ (1 << 4))
#define _GEN_ON_A4H                 (PORTA |=  (1 << 4))
#define _GEN_OFF_A4H                (PORTA &=~ (1 << 4))
#define _GEN_ACTIVE_A4H             (PINA  &   (1 << 4))
#define _GEN_LATCH_A4H              (PORTA &   (1 << 4))
#define _GEN_TOGGLE_A4H             (PINA   =  (1 << 4))
#define _GEN_DDR_A4H                 DDRA
#define _GEN_PIN_A4H                 PINA
#define _GEN_PORT_A4H                PORTA
#define _GEN_LEVEL_A4H               true
#define _GEN_BITNUM_A4L                              4
#define _GEN_BITMASK_A4L                       (1 << 4)
#define _GEN_DRIVER_A4L_OUT         (DDRA  |=  (1 << 4))
#define _GEN_DRIVER_A4L_IN          (DDRA  &=~ (1 << 4))
#define _GEN_DRIVER_A4L_PULLUP      (PORTA |=  (1 << 4))
#define _GEN_DRIVER_A4L_HIZ         (PORTA &=~ (1 << 4))
#define _GEN_ON_A4L                 (PORTA &=~ (1 << 4))
#define _GEN_OFF_A4L                (PORTA |=  (1 << 4))
#define _GEN_ACTIVE_A4L            !(PINA  &   (1 << 4))
#define _GEN_LATCH_A4L             !(PORTA &   (1 << 4))
#define _GEN_TOGGLE_A4L             (PINA   =  (1 << 4))
#define _GEN_DDR_A4L                 DDRA
#define _GEN_PIN_A4L                 PINA
#define _GEN_PORT_A4L                PORTA
#define _GEN_LEVEL_A4L               false

#define _GEN_BITNUM_A5H                              5
#define _GEN_BITMASK_A5H                       (1 << 5)
#define _GEN_DRIVER_A5H_OUT         (DDRA  |=  (1 << 5))
#define _GEN_DRIVER_A5H_IN          (DDRA  &=~ (1 << 5))
#define _GEN_DRIVER_A5H_PULLUP      (PORTA |=  (1 << 5))
#define _GEN_DRIVER_A5H_HIZ         (PORTA &=~ (1 << 5))
#define _GEN_ON_A5H                 (PORTA |=  (1 << 5))
#define _GEN_OFF_A5H                (PORTA &=~ (1 << 5))
#define _GEN_ACTIVE_A5H             (PINA  &   (1 << 5))
#define _GEN_LATCH_A5H              (PORTA &   (1 << 5))
#define _GEN_TOGGLE_A5H             (PINA   =  (1 << 5))
#define _GEN_DDR_A5H                 DDRA
#define _GEN_PIN_A5H                 PINA
#define _GEN_PORT_A5H                PORTA
#define _GEN_LEVEL_A5H               true
#define _GEN_BITNUM_A5L                              5
#define _GEN_BITMASK_A5L                       (1 << 5)
#define _GEN_DRIVER_A5L_OUT         (DDRA  |=  (1 << 5))
#define _GEN_DRIVER_A5L_IN          (DDRA  &=~ (1 << 5))
#define _GEN_DRIVER_A5L_PULLUP      (PORTA |=  (1 << 5))
#define _GEN_DRIVER_A5L_HIZ         (PORTA &=~ (1 << 5))
#define _GEN_ON_A5L                 (PORTA &=~ (1 << 5))
#define _GEN_OFF_A5L                (PORTA |=  (1 << 5))
#define _GEN_ACTIVE_A5L            !(PINA  &   (1 << 5))
#define _GEN_LATCH_A5L             !(PORTA &   (1 << 5))
#define _GEN_TOGGLE_A5L             (PINA   =  (1 << 5))
#define _GEN_DDR_A5L                 DDRA
#define _GEN_PIN_A5L                 PINA
#define _GEN_PORT_A5L                PORTA
#define _GEN_LEVEL_A5L               false

#define _GEN_BITNUM_A6H                              6
#define _GEN_BITMASK_A6H                       (1 << 6)
#define _GEN_DRIVER_A6H_OUT         (DDRA  |=  (1 << 6))
#define _GEN_DRIVER_A6H_IN          (DDRA  &=~ (1 << 6))
#define _GEN_DRIVER_A6H_PULLUP      (PORTA |=  (1 << 6))
#define _GEN_DRIVER_A6H_HIZ         (PORTA &=~ (1 << 6))
#define _GEN_ON_A6H                 (PORTA |=  (1 << 6))
#define _GEN_OFF_A6H                (PORTA &=~ (1 << 6))
#define _GEN_ACTIVE_A6H             (PINA  &   (1 << 6))
#define _GEN_LATCH_A6H              (PORTA &   (1 << 6))
#define _GEN_TOGGLE_A6H             (PINA   =  (1 << 6))
#define _GEN_DDR_A6H                 DDRA
#define _GEN_PIN_A6H                 PINA
#define _GEN_PORT_A6H                PORTA
#define _GEN_LEVEL_A6H               true
#define _GEN_BITNUM_A6L                              6
#define _GEN_BITMASK_A6L                       (1 << 6)
#define _GEN_DRIVER_A6L_OUT         (DDRA  |=  (1 << 6))
#define _GEN_DRIVER_A6L_IN          (DDRA  &=~ (1 << 6))
#define _GEN_DRIVER_A6L_PULLUP      (PORTA |=  (1 << 6))
#define _GEN_DRIVER_A6L_HIZ         (PORTA &=~ (1 << 6))
#define _GEN_ON_A6L                 (PORTA &=~ (1 << 6))
#define _GEN_OFF_A6L                (PORTA |=  (1 << 6))
#define _GEN_ACTIVE_A6L            !(PINA  &   (1 << 6))
#define _GEN_LATCH_A6L             !(PORTA &   (1 << 6))
#define _GEN_TOGGLE_A6L             (PINA   =  (1 << 6))
#define _GEN_DDR_A6L                 DDRA
#define _GEN_PIN_A6L                 PINA
#define _GEN_PORT_A6L                PORTA
#define _GEN_LEVEL_A6L               false

#define _GEN_BITNUM_A7H                              7
#define _GEN_BITMASK_A7H                       (1 << 7)
#define _GEN_DRIVER_A7H_OUT         (DDRA  |=  (1 << 7))
#define _GEN_DRIVER_A7H_IN          (DDRA  &=~ (1 << 7))
#define _GEN_DRIVER_A7H_PULLUP      (PORTA |=  (1 << 7))
#define _GEN_DRIVER_A7H_HIZ         (PORTA &=~ (1 << 7))
#define _GEN_ON_A7H                 (PORTA |=  (1 << 7))
#define _GEN_OFF_A7H                (PORTA &=~ (1 << 7))
#define _GEN_ACTIVE_A7H             (PINA  &   (1 << 7))
#define _GEN_LATCH_A7H              (PORTA &   (1 << 7))
#define _GEN_TOGGLE_A7H             (PINA   =  (1 << 7))
#define _GEN_DDR_A7H                 DDRA
#define _GEN_PIN_A7H                 PINA
#define _GEN_PORT_A7H                PORTA
#define _GEN_LEVEL_A7H               true
#define _GEN_BITNUM_A7L                              7
#define _GEN_BITMASK_A7L                       (1 << 7)
#define _GEN_DRIVER_A7L_OUT         (DDRA  |=  (1 << 7))
#define _GEN_DRIVER_A7L_IN          (DDRA  &=~ (1 << 7))
#define _GEN_DRIVER_A7L_PULLUP      (PORTA |=  (1 << 7))
#define _GEN_DRIVER_A7L_HIZ         (PORTA &=~ (1 << 7))
#define _GEN_ON_A7L                 (PORTA &=~ (1 << 7))
#define _GEN_OFF_A7L                (PORTA |=  (1 << 7))
#define _GEN_ACTIVE_A7L            !(PINA  &   (1 << 7))
#define _GEN_LATCH_A7L             !(PORTA &   (1 << 7))
#define _GEN_TOGGLE_A7L             (PINA   =  (1 << 7))
#define _GEN_DDR_A7L                 DDRA
#define _GEN_PIN_A7L                 PINA
#define _GEN_PORT_A7L                PORTA
#define _GEN_LEVEL_A7L               false

#define _GEN_BITNUM_B0H                              0
#define _GEN_BITMASK_B0H                       (1 << 0)
#define _GEN_DRIVER_B0H_OUT         (DDRB  |=  (1 << 0))
#define _GEN_DRIVER_B0H_IN          (DDRB  &=~ (1 << 0))
#define _GEN_DRIVER_B0H_PULLUP      (PORTB |=  (1 << 0))
#define _GEN_DRIVER_B0H_HIZ         (PORTB &=~ (1 << 0))
#define _GEN_ON_B0H                 (PORTB |=  (1 << 0))
#define _GEN_OFF_B0H                (PORTB &=~ (1 << 0))
#define _GEN_ACTIVE_B0H             (PINB  &   (1 << 0))
#define _GEN_LATCH_B0H              (PORTB &   (1 << 0))
#define _GEN_TOGGLE_B0H             (PINB   =  (1 << 0))
#define _GEN_DDR_B0H                 DDRB
#define _GEN_PIN_B0H                 PINB
#define _GEN_PORT_B0H                PORTB
#define _GEN_LEVEL_B0H               true
#define _GEN_BITNUM_B0L                              0
#define _GEN_BITMASK_B0L                       (1 << 0)
#define _GEN_DRIVER_B0L_OUT         (DDRB  |=  (1 << 0))
#define _GEN_DRIVER_B0L_IN          (DDRB  &=~ (1 << 0))
#define _GEN_DRIVER_B0L_PULLUP      (PORTB |=  (1 << 0))
#define _GEN_DRIVER_B0L_HIZ         (PORTB &=~ (1 << 0))
#define _GEN_ON_B0L                 (PORTB &=~ (1 << 0))
#define _GEN_OFF_B0L                (PORTB |=  (1 << 0))
#define _GEN_ACTIVE_B0L            !(PINB  &   (1 << 0))
#define _GEN_LATCH_B0L             !(PORTB &   (1 << 0))
#define _GEN_TOGGLE_B0L             (PINB   =  (1 << 0))
#define _GEN_DDR_B0L                 DDRB
#define _GEN_PIN_B0L                 PINB
#define _GEN_PORT_B0L                PORTB
#define _GEN_LEVEL_B0L               false

#define _GEN_BITNUM_B1H                              1
#define _GEN_BITMASK_B1H                       (1 << 1)
#define _GEN_DRIVER_B1H_OUT         (DDRB  |=  (1 << 1))
#define _GEN_DRIVER_B1H_IN          (DDRB  &=~ (1 << 1))
#define _GEN_DRIVER_B1H_PULLUP      (PORTB |=  (1 << 1))
#define _GEN_DRIVER_B1H_HIZ         (PORTB &=~ (1 << 1))
#define _GEN_ON_B1H                 (PORTB |=  (1 << 1))
#define _GEN_OFF_B1H                (PORTB &=~ (1 << 1))
#define _GEN_ACTIVE_B1H             (PINB  &   (1 << 1))
#define _GEN_LATCH_B1H              (PORTB &   (1 << 1))
#define _GEN_TOGGLE_B1H             (PINB   =  (1 << 1))
#define _GEN_DDR_B1H                 DDRB
#define _GEN_PIN_B1H                 PINB
#define _GEN_PORT_B1H                PORTB
#define _GEN_LEVEL_B1H               true
#define _GEN_BITNUM_B1L                              1
#define _GEN_BITMASK_B1L                       (1 << 1)
#define _GEN_DRIVER_B1L_OUT         (DDRB  |=  (1 << 1))
#define _GEN_DRIVER_B1L_IN          (DDRB  &=~ (1 << 1))
#define _GEN_DRIVER_B1L_PULLUP      (PORTB |=  (1 << 1))
#define _GEN_DRIVER_B1L_HIZ         (PORTB &=~ (1 << 1))
#define _GEN_ON_B1L                 (PORTB &=~ (1 << 1))
#define _GEN_OFF_B1L                (PORTB |=  (1 << 1))
#define _GEN_ACTIVE_B1L            !(PINB  &   (1 << 1))
#define _GEN_LATCH_B1L             !(PORTB &   (1 << 1))
#define _GEN_TOGGLE_B1L             (PINB   =  (1 << 1))
#define _GEN_DDR_B1L                 DDRB
#define _GEN_PIN_B1L                 PINB
#define _GEN_PORT_B1L                PORTB
#define _GEN_LEVEL_B1L               false

#define _GEN_BITNUM_B2H                              2
#define _GEN_BITMASK_B2H                       (1 << 2)
#define _GEN_DRIVER_B2H_OUT         (DDRB  |=  (1 << 2))
#define _GEN_DRIVER_B2H_IN          (DDRB  &=~ (1 << 2))
#define _GEN_DRIVER_B2H_PULLUP      (PORTB |=  (1 << 2))
#define _GEN_DRIVER_B2H_HIZ         (PORTB &=~ (1 << 2))
#define _GEN_ON_B2H                 (PORTB |=  (1 << 2))
#define _GEN_OFF_B2H                (PORTB &=~ (1 << 2))
#define _GEN_ACTIVE_B2H             (PINB  &   (1 << 2))
#define _GEN_LATCH_B2H              (PORTB &   (1 << 2))
#define _GEN_TOGGLE_B2H             (PINB   =  (1 << 2))
#define _GEN_DDR_B2H                 DDRB
#define _GEN_PIN_B2H                 PINB
#define _GEN_PORT_B2H                PORTB
#define _GEN_LEVEL_B2H               true
#define _GEN_BITNUM_B2L                              2
#define _GEN_BITMASK_B2L                       (1 << 2)
#define _GEN_DRIVER_B2L_OUT         (DDRB  |=  (1 << 2))
#define _GEN_DRIVER_B2L_IN          (DDRB  &=~ (1 << 2))
#define _GEN_DRIVER_B2L_PULLUP      (PORTB |=  (1 << 2))
#define _GEN_DRIVER_B2L_HIZ         (PORTB &=~ (1 << 2))
#define _GEN_ON_B2L                 (PORTB &=~ (1 << 2))
#define _GEN_OFF_B2L                (PORTB |=  (1 << 2))
#define _GEN_ACTIVE_B2L            !(PINB  &   (1 << 2))
#define _GEN_LATCH_B2L             !(PORTB &   (1 << 2))
#define _GEN_TOGGLE_B2L             (PINB   =  (1 << 2))
#define _GEN_DDR_B2L                 DDRB
#define _GEN_PIN_B2L                 PINB
#define _GEN_PORT_B2L                PORTB
#define _GEN_LEVEL_B2L               false

#define _GEN_BITNUM_B3H                              3
#define _GEN_BITMASK_B3H                       (1 << 3)
#define _GEN_DRIVER_B3H_OUT         (DDRB  |=  (1 << 3))
#define _GEN_DRIVER_B3H_IN          (DDRB  &=~ (1 << 3))
#define _GEN_DRIVER_B3H_PULLUP      (PORTB |=  (1 << 3))
#define _GEN_DRIVER_B3H_HIZ         (PORTB &=~ (1 << 3))
#define _GEN_ON_B3H                 (PORTB |=  (1 << 3))
#define _GEN_OFF_B3H                (PORTB &=~ (1 << 3))
#define _GEN_ACTIVE_B3H             (PINB  &   (1 << 3))
#define _GEN_LATCH_B3H              (PORTB &   (1 << 3))
#define _GEN_TOGGLE_B3H             (PINB   =  (1 << 3))
#define _GEN_DDR_B3H                 DDRB
#define _GEN_PIN_B3H                 PINB
#define _GEN_PORT_B3H                PORTB
#define _GEN_LEVEL_B3H               true
#define _GEN_BITNUM_B3L                              3
#define _GEN_BITMASK_B3L                       (1 << 3)
#define _GEN_DRIVER_B3L_OUT         (DDRB  |=  (1 << 3))
#define _GEN_DRIVER_B3L_IN          (DDRB  &=~ (1 << 3))
#define _GEN_DRIVER_B3L_PULLUP      (PORTB |=  (1 << 3))
#define _GEN_DRIVER_B3L_HIZ         (PORTB &=~ (1 << 3))
#define _GEN_ON_B3L                 (PORTB &=~ (1 << 3))
#define _GEN_OFF_B3L                (PORTB |=  (1 << 3))
#define _GEN_ACTIVE_B3L            !(PINB  &   (1 << 3))
#define _GEN_LATCH_B3L             !(PORTB &   (1 << 3))
#define _GEN_TOGGLE_B3L             (PINB   =  (1 << 3))
#define _GEN_DDR_B3L                 DDRB
#define _GEN_PIN_B3L                 PINB
#define _GEN_PORT_B3L                PORTB
#define _GEN_LEVEL_B3L               false

#define _GEN_BITNUM_B4H                              4
#define _GEN_BITMASK_B4H                       (1 << 4)
#define _GEN_DRIVER_B4H_OUT         (DDRB  |=  (1 << 4))
#define _GEN_DRIVER_B4H_IN          (DDRB  &=~ (1 << 4))
#define _GEN_DRIVER_B4H_PULLUP      (PORTB |=  (1 << 4))
#define _GEN_DRIVER_B4H_HIZ         (PORTB &=~ (1 << 4))
#define _GEN_ON_B4H                 (PORTB |=  (1 << 4))
#define _GEN_OFF_B4H                (PORTB &=~ (1 << 4))
#define _GEN_ACTIVE_B4H             (PINB  &   (1 << 4))
#define _GEN_LATCH_B4H              (PORTB &   (1 << 4))
#define _GEN_TOGGLE_B4H             (PINB   =  (1 << 4))
#define _GEN_DDR_B4H                 DDRB
#define _GEN_PIN_B4H                 PINB
#define _GEN_PORT_B4H                PORTB
#define _GEN_LEVEL_B4H               true
#define _GEN_BITNUM_B4L                              4
#define _GEN_BITMASK_B4L                       (1 << 4)
#define _GEN_DRIVER_B4L_OUT         (DDRB  |=  (1 << 4))
#define _GEN_DRIVER_B4L_IN          (DDRB  &=~ (1 << 4))
#define _GEN_DRIVER_B4L_PULLUP      (PORTB |=  (1 << 4))
#define _GEN_DRIVER_B4L_HIZ         (PORTB &=~ (1 << 4))
#define _GEN_ON_B4L                 (PORTB &=~ (1 << 4))
#define _GEN_OFF_B4L                (PORTB |=  (1 << 4))
#define _GEN_ACTIVE_B4L            !(PINB  &   (1 << 4))
#define _GEN_LATCH_B4L             !(PORTB &   (1 << 4))
#define _GEN_TOGGLE_B4L             (PINB   =  (1 << 4))
#define _GEN_DDR_B4L                 DDRB
#define _GEN_PIN_B4L                 PINB
#define _GEN_PORT_B4L                PORTB
#define _GEN_LEVEL_B4L               false

#define _GEN_BITNUM_B5H                              5
#define _GEN_BITMASK_B5H                       (1 << 5)
#define _GEN_DRIVER_B5H_OUT         (DDRB  |=  (1 << 5))
#define _GEN_DRIVER_B5H_IN          (DDRB  &=~ (1 << 5))
#define _GEN_DRIVER_B5H_PULLUP      (PORTB |=  (1 << 5))
#define _GEN_DRIVER_B5H_HIZ         (PORTB &=~ (1 << 5))
#define _GEN_ON_B5H                 (PORTB |=  (1 << 5))
#define _GEN_OFF_B5H                (PORTB &=~ (1 << 5))
#define _GEN_ACTIVE_B5H             (PINB  &   (1 << 5))
#define _GEN_LATCH_B5H              (PORTB &   (1 << 5))
#define _GEN_TOGGLE_B5H             (PINB   =  (1 << 5))
#define _GEN_DDR_B5H                 DDRB
#define _GEN_PIN_B5H                 PINB
#define _GEN_PORT_B5H                PORTB
#define _GEN_LEVEL_B5H               true
#define _GEN_BITNUM_B5L                              5
#define _GEN_BITMASK_B5L                       (1 << 5)
#define _GEN_DRIVER_B5L_OUT         (DDRB  |=  (1 << 5))
#define _GEN_DRIVER_B5L_IN          (DDRB  &=~ (1 << 5))
#define _GEN_DRIVER_B5L_PULLUP      (PORTB |=  (1 << 5))
#define _GEN_DRIVER_B5L_HIZ         (PORTB &=~ (1 << 5))
#define _GEN_ON_B5L                 (PORTB &=~ (1 << 5))
#define _GEN_OFF_B5L                (PORTB |=  (1 << 5))
#define _GEN_ACTIVE_B5L            !(PINB  &   (1 << 5))
#define _GEN_LATCH_B5L             !(PORTB &   (1 << 5))
#define _GEN_TOGGLE_B5L             (PINB   =  (1 << 5))
#define _GEN_DDR_B5L                 DDRB
#define _GEN_PIN_B5L                 PINB
#define _GEN_PORT_B5L                PORTB
#define _GEN_LEVEL_B5L               false

#define _GEN_BITNUM_B6H                              6
#define _GEN_BITMASK_B6H                       (1 << 6)
#define _GEN_DRIVER_B6H_OUT         (DDRB  |=  (1 << 6))
#define _GEN_DRIVER_B6H_IN          (DDRB  &=~ (1 << 6))
#define _GEN_DRIVER_B6H_PULLUP      (PORTB |=  (1 << 6))
#define _GEN_DRIVER_B6H_HIZ         (PORTB &=~ (1 << 6))
#define _GEN_ON_B6H                 (PORTB |=  (1 << 6))
#define _GEN_OFF_B6H                (PORTB &=~ (1 << 6))
#define _GEN_ACTIVE_B6H             (PINB  &   (1 << 6))
#define _GEN_LATCH_B6H              (PORTB &   (1 << 6))
#define _GEN_TOGGLE_B6H             (PINB   =  (1 << 6))
#define _GEN_DDR_B6H                 DDRB
#define _GEN_PIN_B6H                 PINB
#define _GEN_PORT_B6H                PORTB
#define _GEN_LEVEL_B6H               true
#define _GEN_BITNUM_B6L                              6
#define _GEN_BITMASK_B6L                       (1 << 6)
#define _GEN_DRIVER_B6L_OUT         (DDRB  |=  (1 << 6))
#define _GEN_DRIVER_B6L_IN          (DDRB  &=~ (1 << 6))
#define _GEN_DRIVER_B6L_PULLUP      (PORTB |=  (1 << 6))
#define _GEN_DRIVER_B6L_HIZ         (PORTB &=~ (1 << 6))
#define _GEN_ON_B6L                 (PORTB &=~ (1 << 6))
#define _GEN_OFF_B6L                (PORTB |=  (1 << 6))
#define _GEN_ACTIVE_B6L            !(PINB  &   (1 << 6))
#define _GEN_LATCH_B6L             !(PORTB &   (1 << 6))
#define _GEN_TOGGLE_B6L             (PINB   =  (1 << 6))
#define _GEN_DDR_B6L                 DDRB
#define _GEN_PIN_B6L                 PINB
#define _GEN_PORT_B6L                PORTB
#define _GEN_LEVEL_B6L               false

#define _GEN_BITNUM_B7H                              7
#define _GEN_BITMASK_B7H                       (1 << 7)
#define _GEN_DRIVER_B7H_OUT         (DDRB  |=  (1 << 7))
#define _GEN_DRIVER_B7H_IN          (DDRB  &=~ (1 << 7))
#define _GEN_DRIVER_B7H_PULLUP      (PORTB |=  (1 << 7))
#define _GEN_DRIVER_B7H_HIZ         (PORTB &=~ (1 << 7))
#define _GEN_ON_B7H                 (PORTB |=  (1 << 7))
#define _GEN_OFF_B7H                (PORTB &=~ (1 << 7))
#define _GEN_ACTIVE_B7H             (PINB  &   (1 << 7))
#define _GEN_LATCH_B7H              (PORTB &   (1 << 7))
#define _GEN_TOGGLE_B7H             (PINB   =  (1 << 7))
#define _GEN_DDR_B7H                 DDRB
#define _GEN_PIN_B7H                 PINB
#define _GEN_PORT_B7H                PORTB
#define _GEN_LEVEL_B7H               true
#define _GEN_BITNUM_B7L                              7
#define _GEN_BITMASK_B7L                       (1 << 7)
#define _GEN_DRIVER_B7L_OUT         (DDRB  |=  (1 << 7))
#define _GEN_DRIVER_B7L_IN          (DDRB  &=~ (1 << 7))
#define _GEN_DRIVER_B7L_PULLUP      (PORTB |=  (1 << 7))
#define _GEN_DRIVER_B7L_HIZ         (PORTB &=~ (1 << 7))
#define _GEN_ON_B7L                 (PORTB &=~ (1 << 7))
#define _GEN_OFF_B7L                (PORTB |=  (1 << 7))
#define _GEN_ACTIVE_B7L            !(PINB  &   (1 << 7))
#define _GEN_LATCH_B7L             !(PORTB &   (1 << 7))
#define _GEN_TOGGLE_B7L             (PINB   =  (1 << 7))
#define _GEN_DDR_B7L                 DDRB
#define _GEN_PIN_B7L                 PINB
#define _GEN_PORT_B7L                PORTB
#define _GEN_LEVEL_B7L               false

#define _GEN_BITNUM_C0H                              0
#define _GEN_BITMASK_C0H                       (1 << 0)
#define _GEN_DRIVER_C0H_OUT         (DDRC  |=  (1 << 0))
#define _GEN_DRIVER_C0H_IN          (DDRC  &=~ (1 << 0))
#define _GEN_DRIVER_C0H_PULLUP      (PORTC |=  (1 << 0))
#define _GEN_DRIVER_C0H_HIZ         (PORTC &=~ (1 << 0))
#define _GEN_ON_C0H                 (PORTC |=  (1 << 0))
#define _GEN_OFF_C0H                (PORTC &=~ (1 << 0))
#define _GEN_ACTIVE_C0H             (PINC  &   (1 << 0))
#define _GEN_LATCH_C0H              (PORTC &   (1 << 0))
#define _GEN_TOGGLE_C0H             (PINC   =  (1 << 0))
#define _GEN_DDR_C0H                 DDRC
#define _GEN_PIN_C0H                 PINC
#define _GEN_PORT_C0H                PORTC
#define _GEN_LEVEL_C0H               true
#define _GEN_BITNUM_C0L                              0
#define _GEN_BITMASK_C0L                       (1 << 0)
#define _GEN_DRIVER_C0L_OUT         (DDRC  |=  (1 << 0))
#define _GEN_DRIVER_C0L_IN          (DDRC  &=~ (1 << 0))
#define _GEN_DRIVER_C0L_PULLUP      (PORTC |=  (1 << 0))
#define _GEN_DRIVER_C0L_HIZ         (PORTC &=~ (1 << 0))
#define _GEN_ON_C0L                 (PORTC &=~ (1 << 0))
#define _GEN_OFF_C0L                (PORTC |=  (1 << 0))
#define _GEN_ACTIVE_C0L            !(PINC  &   (1 << 0))
#define _GEN_LATCH_C0L             !(PORTC &   (1 << 0))
#define _GEN_TOGGLE_C0L             (PINC   =  (1 << 0))
#define _GEN_DDR_C0L                 DDRC
#define _GEN_PIN_C0L                 PINC
#define _GEN_PORT_C0L                PORTC
#define _GEN_LEVEL_C0L               false

#define _GEN_BITNUM_C1H                              1
#define _GEN_BITMASK_C1H                       (1 << 1)
#define _GEN_DRIVER_C1H_OUT         (DDRC  |=  (1 << 1))
#define _GEN_DRIVER_C1H_IN          (DDRC  &=~ (1 << 1))
#define _GEN_DRIVER_C1H_PULLUP      (PORTC |=  (1 << 1))
#define _GEN_DRIVER_C1H_HIZ         (PORTC &=~ (1 << 1))
#define _GEN_ON_C1H                 (PORTC |=  (1 << 1))
#define _GEN_OFF_C1H                (PORTC &=~ (1 << 1))
#define _GEN_ACTIVE_C1H             (PINC  &   (1 << 1))
#define _GEN_LATCH_C1H              (PORTC &   (1 << 1))
#define _GEN_TOGGLE_C1H             (PINC   =  (1 << 1))
#define _GEN_DDR_C1H                 DDRC
#define _GEN_PIN_C1H                 PINC
#define _GEN_PORT_C1H                PORTC
#define _GEN_LEVEL_C1H               true
#define _GEN_BITNUM_C1L                              1
#define _GEN_BITMASK_C1L                       (1 << 1)
#define _GEN_DRIVER_C1L_OUT         (DDRC  |=  (1 << 1))
#define _GEN_DRIVER_C1L_IN          (DDRC  &=~ (1 << 1))
#define _GEN_DRIVER_C1L_PULLUP      (PORTC |=  (1 << 1))
#define _GEN_DRIVER_C1L_HIZ         (PORTC &=~ (1 << 1))
#define _GEN_ON_C1L                 (PORTC &=~ (1 << 1))
#define _GEN_OFF_C1L                (PORTC |=  (1 << 1))
#define _GEN_ACTIVE_C1L            !(PINC  &   (1 << 1))
#define _GEN_LATCH_C1L             !(PORTC &   (1 << 1))
#define _GEN_TOGGLE_C1L             (PINC   =  (1 << 1))
#define _GEN_DDR_C1L                 DDRC
#define _GEN_PIN_C1L                 PINC
#define _GEN_PORT_C1L                PORTC
#define _GEN_LEVEL_C1L               false

#define _GEN_BITNUM_C2H                              2
#define _GEN_BITMASK_C2H                       (1 << 2)
#define _GEN_DRIVER_C2H_OUT         (DDRC  |=  (1 << 2))
#define _GEN_DRIVER_C2H_IN          (DDRC  &=~ (1 << 2))
#define _GEN_DRIVER_C2H_PULLUP      (PORTC |=  (1 << 2))
#define _GEN_DRIVER_C2H_HIZ         (PORTC &=~ (1 << 2))
#define _GEN_ON_C2H                 (PORTC |=  (1 << 2))
#define _GEN_OFF_C2H                (PORTC &=~ (1 << 2))
#define _GEN_ACTIVE_C2H             (PINC  &   (1 << 2))
#define _GEN_LATCH_C2H              (PORTC &   (1 << 2))
#define _GEN_TOGGLE_C2H             (PINC   =  (1 << 2))
#define _GEN_DDR_C2H                 DDRC
#define _GEN_PIN_C2H                 PINC
#define _GEN_PORT_C2H                PORTC
#define _GEN_LEVEL_C2H               true
#define _GEN_BITNUM_C2L                              2
#define _GEN_BITMASK_C2L                       (1 << 2)
#define _GEN_DRIVER_C2L_OUT         (DDRC  |=  (1 << 2))
#define _GEN_DRIVER_C2L_IN          (DDRC  &=~ (1 << 2))
#define _GEN_DRIVER_C2L_PULLUP      (PORTC |=  (1 << 2))
#define _GEN_DRIVER_C2L_HIZ         (PORTC &=~ (1 << 2))
#define _GEN_ON_C2L                 (PORTC &=~ (1 << 2))
#define _GEN_OFF_C2L                (PORTC |=  (1 << 2))
#define _GEN_ACTIVE_C2L            !(PINC  &   (1 << 2))
#define _GEN_LATCH_C2L             !(PORTC &   (1 << 2))
#define _GEN_TOGGLE_C2L             (PINC   =  (1 << 2))
#define _GEN_DDR_C2L                 DDRC
#define _GEN_PIN_C2L                 PINC
#define _GEN_PORT_C2L                PORTC
#define _GEN_LEVEL_C2L               false

#define _GEN_BITNUM_C3H                              3
#define _GEN_BITMASK_C3H                       (1 << 3)
#define _GEN_DRIVER_C3H_OUT         (DDRC  |=  (1 << 3))
#define _GEN_DRIVER_C3H_IN          (DDRC  &=~ (1 << 3))
#define _GEN_DRIVER_C3H_PULLUP      (PORTC |=  (1 << 3))
#define _GEN_DRIVER_C3H_HIZ         (PORTC &=~ (1 << 3))
#define _GEN_ON_C3H                 (PORTC |=  (1 << 3))
#define _GEN_OFF_C3H                (PORTC &=~ (1 << 3))
#define _GEN_ACTIVE_C3H             (PINC  &   (1 << 3))
#define _GEN_LATCH_C3H              (PORTC &   (1 << 3))
#define _GEN_TOGGLE_C3H             (PINC   =  (1 << 3))
#define _GEN_DDR_C3H                 DDRC
#define _GEN_PIN_C3H                 PINC
#define _GEN_PORT_C3H                PORTC
#define _GEN_LEVEL_C3H               true
#define _GEN_BITNUM_C3L                              3
#define _GEN_BITMASK_C3L                       (1 << 3)
#define _GEN_DRIVER_C3L_OUT         (DDRC  |=  (1 << 3))
#define _GEN_DRIVER_C3L_IN          (DDRC  &=~ (1 << 3))
#define _GEN_DRIVER_C3L_PULLUP      (PORTC |=  (1 << 3))
#define _GEN_DRIVER_C3L_HIZ         (PORTC &=~ (1 << 3))
#define _GEN_ON_C3L                 (PORTC &=~ (1 << 3))
#define _GEN_OFF_C3L                (PORTC |=  (1 << 3))
#define _GEN_ACTIVE_C3L            !(PINC  &   (1 << 3))
#define _GEN_LATCH_C3L             !(PORTC &   (1 << 3))
#define _GEN_TOGGLE_C3L             (PINC   =  (1 << 3))
#define _GEN_DDR_C3L                 DDRC
#define _GEN_PIN_C3L                 PINC
#define _GEN_PORT_C3L                PORTC
#define _GEN_LEVEL_C3L               false

#define _GEN_BITNUM_C4H                              4
#define _GEN_BITMASK_C4H                       (1 << 4)
#define _GEN_DRIVER_C4H_OUT         (DDRC  |=  (1 << 4))
#define _GEN_DRIVER_C4H_IN          (DDRC  &=~ (1 << 4))
#define _GEN_DRIVER_C4H_PULLUP      (PORTC |=  (1 << 4))
#define _GEN_DRIVER_C4H_HIZ         (PORTC &=~ (1 << 4))
#define _GEN_ON_C4H                 (PORTC |=  (1 << 4))
#define _GEN_OFF_C4H                (PORTC &=~ (1 << 4))
#define _GEN_ACTIVE_C4H             (PINC  &   (1 << 4))
#define _GEN_LATCH_C4H              (PORTC &   (1 << 4))
#define _GEN_TOGGLE_C4H             (PINC   =  (1 << 4))
#define _GEN_DDR_C4H                 DDRC
#define _GEN_PIN_C4H                 PINC
#define _GEN_PORT_C4H                PORTC
#define _GEN_LEVEL_C4H               true
#define _GEN_BITNUM_C4L                              4
#define _GEN_BITMASK_C4L                       (1 << 4)
#define _GEN_DRIVER_C4L_OUT         (DDRC  |=  (1 << 4))
#define _GEN_DRIVER_C4L_IN          (DDRC  &=~ (1 << 4))
#define _GEN_DRIVER_C4L_PULLUP      (PORTC |=  (1 << 4))
#define _GEN_DRIVER_C4L_HIZ         (PORTC &=~ (1 << 4))
#define _GEN_ON_C4L                 (PORTC &=~ (1 << 4))
#define _GEN_OFF_C4L                (PORTC |=  (1 << 4))
#define _GEN_ACTIVE_C4L            !(PINC  &   (1 << 4))
#define _GEN_LATCH_C4L             !(PORTC &   (1 << 4))
#define _GEN_TOGGLE_C4L             (PINC   =  (1 << 4))
#define _GEN_DDR_C4L                 DDRC
#define _GEN_PIN_C4L                 PINC
#define _GEN_PORT_C4L                PORTC
#define _GEN_LEVEL_C4L               false

#define _GEN_BITNUM_C5H                              5
#define _GEN_BITMASK_C5H                       (1 << 5)
#define _GEN_DRIVER_C5H_OUT         (DDRC  |=  (1 << 5))
#define _GEN_DRIVER_C5H_IN          (DDRC  &=~ (1 << 5))
#define _GEN_DRIVER_C5H_PULLUP      (PORTC |=  (1 << 5))
#define _GEN_DRIVER_C5H_HIZ         (PORTC &=~ (1 << 5))
#define _GEN_ON_C5H                 (PORTC |=  (1 << 5))
#define _GEN_OFF_C5H                (PORTC &=~ (1 << 5))
#define _GEN_ACTIVE_C5H             (PINC  &   (1 << 5))
#define _GEN_LATCH_C5H              (PORTC &   (1 << 5))
#define _GEN_TOGGLE_C5H             (PINC   =  (1 << 5))
#define _GEN_DDR_C5H                 DDRC
#define _GEN_PIN_C5H                 PINC
#define _GEN_PORT_C5H                PORTC
#define _GEN_LEVEL_C5H               true
#define _GEN_BITNUM_C5L                              5
#define _GEN_BITMASK_C5L                       (1 << 5)
#define _GEN_DRIVER_C5L_OUT         (DDRC  |=  (1 << 5))
#define _GEN_DRIVER_C5L_IN          (DDRC  &=~ (1 << 5))
#define _GEN_DRIVER_C5L_PULLUP      (PORTC |=  (1 << 5))
#define _GEN_DRIVER_C5L_HIZ         (PORTC &=~ (1 << 5))
#define _GEN_ON_C5L                 (PORTC &=~ (1 << 5))
#define _GEN_OFF_C5L                (PORTC |=  (1 << 5))
#define _GEN_ACTIVE_C5L            !(PINC  &   (1 << 5))
#define _GEN_LATCH_C5L             !(PORTC &   (1 << 5))
#define _GEN_TOGGLE_C5L             (PINC   =  (1 << 5))
#define _GEN_DDR_C5L                 DDRC
#define _GEN_PIN_C5L                 PINC
#define _GEN_PORT_C5L                PORTC
#define _GEN_LEVEL_C5L               false

#define _GEN_BITNUM_C6H                              6
#define _GEN_BITMASK_C6H                       (1 << 6)
#define _GEN_DRIVER_C6H_OUT         (DDRC  |=  (1 << 6))
#define _GEN_DRIVER_C6H_IN          (DDRC  &=~ (1 << 6))
#define _GEN_DRIVER_C6H_PULLUP      (PORTC |=  (1 << 6))
#define _GEN_DRIVER_C6H_HIZ         (PORTC &=~ (1 << 6))
#define _GEN_ON_C6H                 (PORTC |=  (1 << 6))
#define _GEN_OFF_C6H                (PORTC &=~ (1 << 6))
#define _GEN_ACTIVE_C6H             (PINC  &   (1 << 6))
#define _GEN_LATCH_C6H              (PORTC &   (1 << 6))
#define _GEN_TOGGLE_C6H             (PINC   =  (1 << 6))
#define _GEN_DDR_C6H                 DDRC
#define _GEN_PIN_C6H                 PINC
#define _GEN_PORT_C6H                PORTC
#define _GEN_LEVEL_C6H               true
#define _GEN_BITNUM_C6L                              6
#define _GEN_BITMASK_C6L                       (1 << 6)
#define _GEN_DRIVER_C6L_OUT         (DDRC  |=  (1 << 6))
#define _GEN_DRIVER_C6L_IN          (DDRC  &=~ (1 << 6))
#define _GEN_DRIVER_C6L_PULLUP      (PORTC |=  (1 << 6))
#define _GEN_DRIVER_C6L_HIZ         (PORTC &=~ (1 << 6))
#define _GEN_ON_C6L                 (PORTC &=~ (1 << 6))
#define _GEN_OFF_C6L                (PORTC |=  (1 << 6))
#define _GEN_ACTIVE_C6L            !(PINC  &   (1 << 6))
#define _GEN_LATCH_C6L             !(PORTC &   (1 << 6))
#define _GEN_TOGGLE_C6L             (PINC   =  (1 << 6))
#define _GEN_DDR_C6L                 DDRC
#define _GEN_PIN_C6L                 PINC
#define _GEN_PORT_C6L                PORTC
#define _GEN_LEVEL_C6L               false

#define _GEN_BITNUM_C7H                              7
#define _GEN_BITMASK_C7H                       (1 << 7)
#define _GEN_DRIVER_C7H_OUT         (DDRC  |=  (1 << 7))
#define _GEN_DRIVER_C7H_IN          (DDRC  &=~ (1 << 7))
#define _GEN_DRIVER_C7H_PULLUP      (PORTC |=  (1 << 7))
#define _GEN_DRIVER_C7H_HIZ         (PORTC &=~ (1 << 7))
#define _GEN_ON_C7H                 (PORTC |=  (1 << 7))
#define _GEN_OFF_C7H                (PORTC &=~ (1 << 7))
#define _GEN_ACTIVE_C7H             (PINC  &   (1 << 7))
#define _GEN_LATCH_C7H              (PORTC &   (1 << 7))
#define _GEN_TOGGLE_C7H             (PINC   =  (1 << 7))
#define _GEN_DDR_C7H                 DDRC
#define _GEN_PIN_C7H                 PINC
#define _GEN_PORT_C7H                PORTC
#define _GEN_LEVEL_C7H               true
#define _GEN_BITNUM_C7L                              7
#define _GEN_BITMASK_C7L                       (1 << 7)
#define _GEN_DRIVER_C7L_OUT         (DDRC  |=  (1 << 7))
#define _GEN_DRIVER_C7L_IN          (DDRC  &=~ (1 << 7))
#define _GEN_DRIVER_C7L_PULLUP      (PORTC |=  (1 << 7))
#define _GEN_DRIVER_C7L_HIZ         (PORTC &=~ (1 << 7))
#define _GEN_ON_C7L                 (PORTC &=~ (1 << 7))
#define _GEN_OFF_C7L                (PORTC |=  (1 << 7))
#define _GEN_ACTIVE_C7L            !(PINC  &   (1 << 7))
#define _GEN_LATCH_C7L             !(PORTC &   (1 << 7))
#define _GEN_TOGGLE_C7L             (PINC   =  (1 << 7))
#define _GEN_DDR_C7L                 DDRC
#define _GEN_PIN_C7L                 PINC
#define _GEN_PORT_C7L                PORTC
#define _GEN_LEVEL_C7L               false

#define _GEN_BITNUM_D0H                              0
#define _GEN_BITMASK_D0H                       (1 << 0)
#define _GEN_DRIVER_D0H_OUT         (DDRD  |=  (1 << 0))
#define _GEN_DRIVER_D0H_IN          (DDRD  &=~ (1 << 0))
#define _GEN_DRIVER_D0H_PULLUP      (PORTD |=  (1 << 0))
#define _GEN_DRIVER_D0H_HIZ         (PORTD &=~ (1 << 0))
#define _GEN_ON_D0H                 (PORTD |=  (1 << 0))
#define _GEN_OFF_D0H                (PORTD &=~ (1 << 0))
#define _GEN_ACTIVE_D0H             (PIND  &   (1 << 0))
#define _GEN_LATCH_D0H              (PORTD &   (1 << 0))
#define _GEN_TOGGLE_D0H             (PIND   =  (1 << 0))
#define _GEN_DDR_D0H                 DDRD
#define _GEN_PIN_D0H                 PIND
#define _GEN_PORT_D0H                PORTD
#define _GEN_LEVEL_D0H               true
#define _GEN_BITNUM_D0L                              0
#define _GEN_BITMASK_D0L                       (1 << 0)
#define _GEN_DRIVER_D0L_OUT         (DDRD  |=  (1 << 0))
#define _GEN_DRIVER_D0L_IN          (DDRD  &=~ (1 << 0))
#define _GEN_DRIVER_D0L_PULLUP      (PORTD |=  (1 << 0))
#define _GEN_DRIVER_D0L_HIZ         (PORTD &=~ (1 << 0))
#define _GEN_ON_D0L                 (PORTD &=~ (1 << 0))
#define _GEN_OFF_D0L                (PORTD |=  (1 << 0))
#define _GEN_ACTIVE_D0L            !(PIND  &   (1 << 0))
#define _GEN_LATCH_D0L             !(PORTD &   (1 << 0))
#define _GEN_TOGGLE_D0L             (PIND   =  (1 << 0))
#define _GEN_DDR_D0L                 DDRD
#define _GEN_PIN_D0L                 PIND
#define _GEN_PORT_D0L                PORTD
#define _GEN_LEVEL_D0L               false

#define _GEN_BITNUM_D1H                              1
#define _GEN_BITMASK_D1H                       (1 << 1)
#define _GEN_DRIVER_D1H_OUT         (DDRD  |=  (1 << 1))
#define _GEN_DRIVER_D1H_IN          (DDRD  &=~ (1 << 1))
#define _GEN_DRIVER_D1H_PULLUP      (PORTD |=  (1 << 1))
#define _GEN_DRIVER_D1H_HIZ         (PORTD &=~ (1 << 1))
#define _GEN_ON_D1H                 (PORTD |=  (1 << 1))
#define _GEN_OFF_D1H                (PORTD &=~ (1 << 1))
#define _GEN_ACTIVE_D1H             (PIND  &   (1 << 1))
#define _GEN_LATCH_D1H              (PORTD &   (1 << 1))
#define _GEN_TOGGLE_D1H             (PIND   =  (1 << 1))
#define _GEN_DDR_D1H                 DDRD
#define _GEN_PIN_D1H                 PIND
#define _GEN_PORT_D1H                PORTD
#define _GEN_LEVEL_D1H               true
#define _GEN_BITNUM_D1L                              1
#define _GEN_BITMASK_D1L                       (1 << 1)
#define _GEN_DRIVER_D1L_OUT         (DDRD  |=  (1 << 1))
#define _GEN_DRIVER_D1L_IN          (DDRD  &=~ (1 << 1))
#define _GEN_DRIVER_D1L_PULLUP      (PORTD |=  (1 << 1))
#define _GEN_DRIVER_D1L_HIZ         (PORTD &=~ (1 << 1))
#define _GEN_ON_D1L                 (PORTD &=~ (1 << 1))
#define _GEN_OFF_D1L                (PORTD |=  (1 << 1))
#define _GEN_ACTIVE_D1L            !(PIND  &   (1 << 1))
#define _GEN_LATCH_D1L             !(PORTD &   (1 << 1))
#define _GEN_TOGGLE_D1L             (PIND   =  (1 << 1))
#define _GEN_DDR_D1L                 DDRD
#define _GEN_PIN_D1L                 PIND
#define _GEN_PORT_D1L                PORTD
#define _GEN_LEVEL_D1L               false

#define _GEN_BITNUM_D2H                              2
#define _GEN_BITMASK_D2H                       (1 << 2)
#define _GEN_DRIVER_D2H_OUT         (DDRD  |=  (1 << 2))
#define _GEN_DRIVER_D2H_IN          (DDRD  &=~ (1 << 2))
#define _GEN_DRIVER_D2H_PULLUP      (PORTD |=  (1 << 2))
#define _GEN_DRIVER_D2H_HIZ         (PORTD &=~ (1 << 2))
#define _GEN_ON_D2H                 (PORTD |=  (1 << 2))
#define _GEN_OFF_D2H                (PORTD &=~ (1 << 2))
#define _GEN_ACTIVE_D2H             (PIND  &   (1 << 2))
#define _GEN_LATCH_D2H              (PORTD &   (1 << 2))
#define _GEN_TOGGLE_D2H             (PIND   =  (1 << 2))
#define _GEN_DDR_D2H                 DDRD
#define _GEN_PIN_D2H                 PIND
#define _GEN_PORT_D2H                PORTD
#define _GEN_LEVEL_D2H               true
#define _GEN_BITNUM_D2L                              2
#define _GEN_BITMASK_D2L                       (1 << 2)
#define _GEN_DRIVER_D2L_OUT         (DDRD  |=  (1 << 2))
#define _GEN_DRIVER_D2L_IN          (DDRD  &=~ (1 << 2))
#define _GEN_DRIVER_D2L_PULLUP      (PORTD |=  (1 << 2))
#define _GEN_DRIVER_D2L_HIZ         (PORTD &=~ (1 << 2))
#define _GEN_ON_D2L                 (PORTD &=~ (1 << 2))
#define _GEN_OFF_D2L                (PORTD |=  (1 << 2))
#define _GEN_ACTIVE_D2L            !(PIND  &   (1 << 2))
#define _GEN_LATCH_D2L             !(PORTD &   (1 << 2))
#define _GEN_TOGGLE_D2L             (PIND   =  (1 << 2))
#define _GEN_DDR_D2L                 DDRD
#define _GEN_PIN_D2L                 PIND
#define _GEN_PORT_D2L                PORTD
#define _GEN_LEVEL_D2L               false

#define _GEN_BITNUM_D3H                              3
#define _GEN_BITMASK_D3H                       (1 << 3)
#define _GEN_DRIVER_D3H_OUT         (DDRD  |=  (1 << 3))
#define _GEN_DRIVER_D3H_IN          (DDRD  &=~ (1 << 3))
#define _GEN_DRIVER_D3H_PULLUP      (PORTD |=  (1 << 3))
#define _GEN_DRIVER_D3H_HIZ         (PORTD &=~ (1 << 3))
#define _GEN_ON_D3H                 (PORTD |=  (1 << 3))
#define _GEN_OFF_D3H                (PORTD &=~ (1 << 3))
#define _GEN_ACTIVE_D3H             (PIND  &   (1 << 3))
#define _GEN_LATCH_D3H              (PORTD &   (1 << 3))
#define _GEN_TOGGLE_D3H             (PIND   =  (1 << 3))
#define _GEN_DDR_D3H                 DDRD
#define _GEN_PIN_D3H                 PIND
#define _GEN_PORT_D3H                PORTD
#define _GEN_LEVEL_D3H               true
#define _GEN_BITNUM_D3L                              3
#define _GEN_BITMASK_D3L                       (1 << 3)
#define _GEN_DRIVER_D3L_OUT         (DDRD  |=  (1 << 3))
#define _GEN_DRIVER_D3L_IN          (DDRD  &=~ (1 << 3))
#define _GEN_DRIVER_D3L_PULLUP      (PORTD |=  (1 << 3))
#define _GEN_DRIVER_D3L_HIZ         (PORTD &=~ (1 << 3))
#define _GEN_ON_D3L                 (PORTD &=~ (1 << 3))
#define _GEN_OFF_D3L                (PORTD |=  (1 << 3))
#define _GEN_ACTIVE_D3L            !(PIND  &   (1 << 3))
#define _GEN_LATCH_D3L             !(PORTD &   (1 << 3))
#define _GEN_TOGGLE_D3L             (PIND   =  (1 << 3))
#define _GEN_DDR_D3L                 DDRD
#define _GEN_PIN_D3L                 PIND
#define _GEN_PORT_D3L                PORTD
#define _GEN_LEVEL_D3L               false

#define _GEN_BITNUM_D4H                              4
#define _GEN_BITMASK_D4H                       (1 << 4)
#define _GEN_DRIVER_D4H_OUT         (DDRD  |=  (1 << 4))
#define _GEN_DRIVER_D4H_IN          (DDRD  &=~ (1 << 4))
#define _GEN_DRIVER_D4H_PULLUP      (PORTD |=  (1 << 4))
#define _GEN_DRIVER_D4H_HIZ         (PORTD &=~ (1 << 4))
#define _GEN_ON_D4H                 (PORTD |=  (1 << 4))
#define _GEN_OFF_D4H                (PORTD &=~ (1 << 4))
#define _GEN_ACTIVE_D4H             (PIND  &   (1 << 4))
#define _GEN_LATCH_D4H              (PORTD &   (1 << 4))
#define _GEN_TOGGLE_D4H             (PIND   =  (1 << 4))
#define _GEN_DDR_D4H                 DDRD
#define _GEN_PIN_D4H                 PIND
#define _GEN_PORT_D4H                PORTD
#define _GEN_LEVEL_D4H               true
#define _GEN_BITNUM_D4L                              4
#define _GEN_BITMASK_D4L                       (1 << 4)
#define _GEN_DRIVER_D4L_OUT         (DDRD  |=  (1 << 4))
#define _GEN_DRIVER_D4L_IN          (DDRD  &=~ (1 << 4))
#define _GEN_DRIVER_D4L_PULLUP      (PORTD |=  (1 << 4))
#define _GEN_DRIVER_D4L_HIZ         (PORTD &=~ (1 << 4))
#define _GEN_ON_D4L                 (PORTD &=~ (1 << 4))
#define _GEN_OFF_D4L                (PORTD |=  (1 << 4))
#define _GEN_ACTIVE_D4L            !(PIND  &   (1 << 4))
#define _GEN_LATCH_D4L             !(PORTD &   (1 << 4))
#define _GEN_TOGGLE_D4L             (PIND   =  (1 << 4))
#define _GEN_DDR_D4L                 DDRD
#define _GEN_PIN_D4L                 PIND
#define _GEN_PORT_D4L                PORTD
#define _GEN_LEVEL_D4L               false

#define _GEN_BITNUM_D5H                              5
#define _GEN_BITMASK_D5H                       (1 << 5)
#define _GEN_DRIVER_D5H_OUT         (DDRD  |=  (1 << 5))
#define _GEN_DRIVER_D5H_IN          (DDRD  &=~ (1 << 5))
#define _GEN_DRIVER_D5H_PULLUP      (PORTD |=  (1 << 5))
#define _GEN_DRIVER_D5H_HIZ         (PORTD &=~ (1 << 5))
#define _GEN_ON_D5H                 (PORTD |=  (1 << 5))
#define _GEN_OFF_D5H                (PORTD &=~ (1 << 5))
#define _GEN_ACTIVE_D5H             (PIND  &   (1 << 5))
#define _GEN_LATCH_D5H              (PORTD &   (1 << 5))
#define _GEN_TOGGLE_D5H             (PIND   =  (1 << 5))
#define _GEN_DDR_D5H                 DDRD
#define _GEN_PIN_D5H                 PIND
#define _GEN_PORT_D5H                PORTD
#define _GEN_LEVEL_D5H               true
#define _GEN_BITNUM_D5L                              5
#define _GEN_BITMASK_D5L                       (1 << 5)
#define _GEN_DRIVER_D5L_OUT         (DDRD  |=  (1 << 5))
#define _GEN_DRIVER_D5L_IN          (DDRD  &=~ (1 << 5))
#define _GEN_DRIVER_D5L_PULLUP      (PORTD |=  (1 << 5))
#define _GEN_DRIVER_D5L_HIZ         (PORTD &=~ (1 << 5))
#define _GEN_ON_D5L                 (PORTD &=~ (1 << 5))
#define _GEN_OFF_D5L                (PORTD |=  (1 << 5))
#define _GEN_ACTIVE_D5L            !(PIND  &   (1 << 5))
#define _GEN_LATCH_D5L             !(PORTD &   (1 << 5))
#define _GEN_TOGGLE_D5L             (PIND   =  (1 << 5))
#define _GEN_DDR_D5L                 DDRD
#define _GEN_PIN_D5L                 PIND
#define _GEN_PORT_D5L                PORTD
#define _GEN_LEVEL_D5L               false

#define _GEN_BITNUM_D6H                              6
#define _GEN_BITMASK_D6H                       (1 << 6)
#define _GEN_DRIVER_D6H_OUT         (DDRD  |=  (1 << 6))
#define _GEN_DRIVER_D6H_IN          (DDRD  &=~ (1 << 6))
#define _GEN_DRIVER_D6H_PULLUP      (PORTD |=  (1 << 6))
#define _GEN_DRIVER_D6H_HIZ         (PORTD &=~ (1 << 6))
#define _GEN_ON_D6H                 (PORTD |=  (1 << 6))
#define _GEN_OFF_D6H                (PORTD &=~ (1 << 6))
#define _GEN_ACTIVE_D6H             (PIND  &   (1 << 6))
#define _GEN_LATCH_D6H              (PORTD &   (1 << 6))
#define _GEN_TOGGLE_D6H             (PIND   =  (1 << 6))
#define _GEN_DDR_D6H                 DDRD
#define _GEN_PIN_D6H                 PIND
#define _GEN_PORT_D6H                PORTD
#define _GEN_LEVEL_D6H               true
#define _GEN_BITNUM_D6L                              6
#define _GEN_BITMASK_D6L                       (1 << 6)
#define _GEN_DRIVER_D6L_OUT         (DDRD  |=  (1 << 6))
#define _GEN_DRIVER_D6L_IN          (DDRD  &=~ (1 << 6))
#define _GEN_DRIVER_D6L_PULLUP      (PORTD |=  (1 << 6))
#define _GEN_DRIVER_D6L_HIZ         (PORTD &=~ (1 << 6))
#define _GEN_ON_D6L                 (PORTD &=~ (1 << 6))
#define _GEN_OFF_D6L                (PORTD |=  (1 << 6))
#define _GEN_ACTIVE_D6L            !(PIND  &   (1 << 6))
#define _GEN_LATCH_D6L             !(PORTD &   (1 << 6))
#define _GEN_TOGGLE_D6L             (PIND   =  (1 << 6))
#define _GEN_DDR_D6L                 DDRD
#define _GEN_PIN_D6L                 PIND
#define _GEN_PORT_D6L                PORTD
#define _GEN_LEVEL_D6L               false

#define _GEN_BITNUM_D7H                              7
#define _GEN_BITMASK_D7H                       (1 << 7)
#define _GEN_DRIVER_D7H_OUT         (DDRD  |=  (1 << 7))
#define _GEN_DRIVER_D7H_IN          (DDRD  &=~ (1 << 7))
#define _GEN_DRIVER_D7H_PULLUP      (PORTD |=  (1 << 7))
#define _GEN_DRIVER_D7H_HIZ         (PORTD &=~ (1 << 7))
#define _GEN_ON_D7H                 (PORTD |=  (1 << 7))
#define _GEN_OFF_D7H                (PORTD &=~ (1 << 7))
#define _GEN_ACTIVE_D7H             (PIND  &   (1 << 7))
#define _GEN_LATCH_D7H              (PORTD &   (1 << 7))
#define _GEN_TOGGLE_D7H             (PIND   =  (1 << 7))
#define _GEN_DDR_D7H                 DDRD
#define _GEN_PIN_D7H                 PIND
#define _GEN_PORT_D7H                PORTD
#define _GEN_LEVEL_D7H               true
#define _GEN_BITNUM_D7L                              7
#define _GEN_BITMASK_D7L                       (1 << 7)
#define _GEN_DRIVER_D7L_OUT         (DDRD  |=  (1 << 7))
#define _GEN_DRIVER_D7L_IN          (DDRD  &=~ (1 << 7))
#define _GEN_DRIVER_D7L_PULLUP      (PORTD |=  (1 << 7))
#define _GEN_DRIVER_D7L_HIZ         (PORTD &=~ (1 << 7))
#define _GEN_ON_D7L                 (PORTD &=~ (1 << 7))
#define _GEN_OFF_D7L                (PORTD |=  (1 << 7))
#define _GEN_ACTIVE_D7L            !(PIND  &   (1 << 7))
#define _GEN_LATCH_D7L             !(PORTD &   (1 << 7))
#define _GEN_TOGGLE_D7L             (PIND   =  (1 << 7))
#define _GEN_DDR_D7L                 DDRD
#define _GEN_PIN_D7L                 PIND
#define _GEN_PORT_D7L                PORTD
#define _GEN_LEVEL_D7L               false

#define _GEN_BITNUM_E0H                              0
#define _GEN_BITMASK_E0H                       (1 << 0)
#define _GEN_DRIVER_E0H_OUT         (DDRE  |=  (1 << 0))
#define _GEN_DRIVER_E0H_IN          (DDRE  &=~ (1 << 0))
#define _GEN_DRIVER_E0H_PULLUP      (PORTE |=  (1 << 0))
#define _GEN_DRIVER_E0H_HIZ         (PORTE &=~ (1 << 0))
#define _GEN_ON_E0H                 (PORTE |=  (1 << 0))
#define _GEN_OFF_E0H                (PORTE &=~ (1 << 0))
#define _GEN_ACTIVE_E0H             (PINE  &   (1 << 0))
#define _GEN_LATCH_E0H              (PORTE &   (1 << 0))
#define _GEN_TOGGLE_E0H             (PINE   =  (1 << 0))
#define _GEN_DDR_E0H                 DDRE
#define _GEN_PIN_E0H                 PINE
#define _GEN_PORT_E0H                PORTE
#define _GEN_LEVEL_E0H               true
#define _GEN_BITNUM_E0L                              0
#define _GEN_BITMASK_E0L                       (1 << 0)
#define _GEN_DRIVER_E0L_OUT         (DDRE  |=  (1 << 0))
#define _GEN_DRIVER_E0L_IN          (DDRE  &=~ (1 << 0))
#define _GEN_DRIVER_E0L_PULLUP      (PORTE |=  (1 << 0))
#define _GEN_DRIVER_E0L_HIZ         (PORTE &=~ (1 << 0))
#define _GEN_ON_E0L                 (PORTE &=~ (1 << 0))
#define _GEN_OFF_E0L                (PORTE |=  (1 << 0))
#define _GEN_ACTIVE_E0L            !(PINE  &   (1 << 0))
#define _GEN_LATCH_E0L             !(PORTE &   (1 << 0))
#define _GEN_TOGGLE_E0L             (PINE   =  (1 << 0))
#define _GEN_DDR_E0L                 DDRE
#define _GEN_PIN_E0L                 PINE
#define _GEN_PORT_E0L                PORTE
#define _GEN_LEVEL_E0L               false

#define _GEN_BITNUM_E1H                              1
#define _GEN_BITMASK_E1H                       (1 << 1)
#define _GEN_DRIVER_E1H_OUT         (DDRE  |=  (1 << 1))
#define _GEN_DRIVER_E1H_IN          (DDRE  &=~ (1 << 1))
#define _GEN_DRIVER_E1H_PULLUP      (PORTE |=  (1 << 1))
#define _GEN_DRIVER_E1H_HIZ         (PORTE &=~ (1 << 1))
#define _GEN_ON_E1H                 (PORTE |=  (1 << 1))
#define _GEN_OFF_E1H                (PORTE &=~ (1 << 1))
#define _GEN_ACTIVE_E1H             (PINE  &   (1 << 1))
#define _GEN_LATCH_E1H              (PORTE &   (1 << 1))
#define _GEN_TOGGLE_E1H             (PINE   =  (1 << 1))
#define _GEN_DDR_E1H                 DDRE
#define _GEN_PIN_E1H                 PINE
#define _GEN_PORT_E1H                PORTE
#define _GEN_LEVEL_E1H               true
#define _GEN_BITNUM_E1L                              1
#define _GEN_BITMASK_E1L                       (1 << 1)
#define _GEN_DRIVER_E1L_OUT         (DDRE  |=  (1 << 1))
#define _GEN_DRIVER_E1L_IN          (DDRE  &=~ (1 << 1))
#define _GEN_DRIVER_E1L_PULLUP      (PORTE |=  (1 << 1))
#define _GEN_DRIVER_E1L_HIZ         (PORTE &=~ (1 << 1))
#define _GEN_ON_E1L                 (PORTE &=~ (1 << 1))
#define _GEN_OFF_E1L                (PORTE |=  (1 << 1))
#define _GEN_ACTIVE_E1L            !(PINE  &   (1 << 1))
#define _GEN_LATCH_E1L             !(PORTE &   (1 << 1))
#define _GEN_TOGGLE_E1L             (PINE   =  (1 << 1))
#define _GEN_DDR_E1L                 DDRE
#define _GEN_PIN_E1L                 PINE
#define _GEN_PORT_E1L                PORTE
#define _GEN_LEVEL_E1L               false

#define _GEN_BITNUM_E2H                              2
#define _GEN_BITMASK_E2H                       (1 << 2)
#define _GEN_DRIVER_E2H_OUT         (DDRE  |=  (1 << 2))
#define _GEN_DRIVER_E2H_IN          (DDRE  &=~ (1 << 2))
#define _GEN_DRIVER_E2H_PULLUP      (PORTE |=  (1 << 2))
#define _GEN_DRIVER_E2H_HIZ         (PORTE &=~ (1 << 2))
#define _GEN_ON_E2H                 (PORTE |=  (1 << 2))
#define _GEN_OFF_E2H                (PORTE &=~ (1 << 2))
#define _GEN_ACTIVE_E2H             (PINE  &   (1 << 2))
#define _GEN_LATCH_E2H              (PORTE &   (1 << 2))
#define _GEN_TOGGLE_E2H             (PINE   =  (1 << 2))
#define _GEN_DDR_E2H                 DDRE
#define _GEN_PIN_E2H                 PINE
#define _GEN_PORT_E2H                PORTE
#define _GEN_LEVEL_E2H               true
#define _GEN_BITNUM_E2L                              2
#define _GEN_BITMASK_E2L                       (1 << 2)
#define _GEN_DRIVER_E2L_OUT         (DDRE  |=  (1 << 2))
#define _GEN_DRIVER_E2L_IN          (DDRE  &=~ (1 << 2))
#define _GEN_DRIVER_E2L_PULLUP      (PORTE |=  (1 << 2))
#define _GEN_DRIVER_E2L_HIZ         (PORTE &=~ (1 << 2))
#define _GEN_ON_E2L                 (PORTE &=~ (1 << 2))
#define _GEN_OFF_E2L                (PORTE |=  (1 << 2))
#define _GEN_ACTIVE_E2L            !(PINE  &   (1 << 2))
#define _GEN_LATCH_E2L             !(PORTE &   (1 << 2))
#define _GEN_TOGGLE_E2L             (PINE   =  (1 << 2))
#define _GEN_DDR_E2L                 DDRE
#define _GEN_PIN_E2L                 PINE
#define _GEN_PORT_E2L                PORTE
#define _GEN_LEVEL_E2L               false

#define _GEN_BITNUM_E3H                              3
#define _GEN_BITMASK_E3H                       (1 << 3)
#define _GEN_DRIVER_E3H_OUT         (DDRE  |=  (1 << 3))
#define _GEN_DRIVER_E3H_IN          (DDRE  &=~ (1 << 3))
#define _GEN_DRIVER_E3H_PULLUP      (PORTE |=  (1 << 3))
#define _GEN_DRIVER_E3H_HIZ         (PORTE &=~ (1 << 3))
#define _GEN_ON_E3H                 (PORTE |=  (1 << 3))
#define _GEN_OFF_E3H                (PORTE &=~ (1 << 3))
#define _GEN_ACTIVE_E3H             (PINE  &   (1 << 3))
#define _GEN_LATCH_E3H              (PORTE &   (1 << 3))
#define _GEN_TOGGLE_E3H             (PINE   =  (1 << 3))
#define _GEN_DDR_E3H                 DDRE
#define _GEN_PIN_E3H                 PINE
#define _GEN_PORT_E3H                PORTE
#define _GEN_LEVEL_E3H               true
#define _GEN_BITNUM_E3L                              3
#define _GEN_BITMASK_E3L                       (1 << 3)
#define _GEN_DRIVER_E3L_OUT         (DDRE  |=  (1 << 3))
#define _GEN_DRIVER_E3L_IN          (DDRE  &=~ (1 << 3))
#define _GEN_DRIVER_E3L_PULLUP      (PORTE |=  (1 << 3))
#define _GEN_DRIVER_E3L_HIZ         (PORTE &=~ (1 << 3))
#define _GEN_ON_E3L                 (PORTE &=~ (1 << 3))
#define _GEN_OFF_E3L                (PORTE |=  (1 << 3))
#define _GEN_ACTIVE_E3L            !(PINE  &   (1 << 3))
#define _GEN_LATCH_E3L             !(PORTE &   (1 << 3))
#define _GEN_TOGGLE_E3L             (PINE   =  (1 << 3))
#define _GEN_DDR_E3L                 DDRE
#define _GEN_PIN_E3L                 PINE
#define _GEN_PORT_E3L                PORTE
#define _GEN_LEVEL_E3L               false

#define _GEN_BITNUM_E4H                              4
#define _GEN_BITMASK_E4H                       (1 << 4)
#define _GEN_DRIVER_E4H_OUT         (DDRE  |=  (1 << 4))
#define _GEN_DRIVER_E4H_IN          (DDRE  &=~ (1 << 4))
#define _GEN_DRIVER_E4H_PULLUP      (PORTE |=  (1 << 4))
#define _GEN_DRIVER_E4H_HIZ         (PORTE &=~ (1 << 4))
#define _GEN_ON_E4H                 (PORTE |=  (1 << 4))
#define _GEN_OFF_E4H                (PORTE &=~ (1 << 4))
#define _GEN_ACTIVE_E4H             (PINE  &   (1 << 4))
#define _GEN_LATCH_E4H              (PORTE &   (1 << 4))
#define _GEN_TOGGLE_E4H             (PINE   =  (1 << 4))
#define _GEN_DDR_E4H                 DDRE
#define _GEN_PIN_E4H                 PINE
#define _GEN_PORT_E4H                PORTE
#define _GEN_LEVEL_E4H               true
#define _GEN_BITNUM_E4L                              4
#define _GEN_BITMASK_E4L                       (1 << 4)
#define _GEN_DRIVER_E4L_OUT         (DDRE  |=  (1 << 4))
#define _GEN_DRIVER_E4L_IN          (DDRE  &=~ (1 << 4))
#define _GEN_DRIVER_E4L_PULLUP      (PORTE |=  (1 << 4))
#define _GEN_DRIVER_E4L_HIZ         (PORTE &=~ (1 << 4))
#define _GEN_ON_E4L                 (PORTE &=~ (1 << 4))
#define _GEN_OFF_E4L                (PORTE |=  (1 << 4))
#define _GEN_ACTIVE_E4L            !(PINE  &   (1 << 4))
#define _GEN_LATCH_E4L             !(PORTE &   (1 << 4))
#define _GEN_TOGGLE_E4L             (PINE   =  (1 << 4))
#define _GEN_DDR_E4L                 DDRE
#define _GEN_PIN_E4L                 PINE
#define _GEN_PORT_E4L                PORTE
#define _GEN_LEVEL_E4L               false

#define _GEN_BITNUM_E5H                              5
#define _GEN_BITMASK_E5H                       (1 << 5)
#define _GEN_DRIVER_E5H_OUT         (DDRE  |=  (1 << 5))
#define _GEN_DRIVER_E5H_IN          (DDRE  &=~ (1 << 5))
#define _GEN_DRIVER_E5H_PULLUP      (PORTE |=  (1 << 5))
#define _GEN_DRIVER_E5H_HIZ         (PORTE &=~ (1 << 5))
#define _GEN_ON_E5H                 (PORTE |=  (1 << 5))
#define _GEN_OFF_E5H                (PORTE &=~ (1 << 5))
#define _GEN_ACTIVE_E5H             (PINE  &   (1 << 5))
#define _GEN_LATCH_E5H              (PORTE &   (1 << 5))
#define _GEN_TOGGLE_E5H             (PINE   =  (1 << 5))
#define _GEN_DDR_E5H                 DDRE
#define _GEN_PIN_E5H                 PINE
#define _GEN_PORT_E5H                PORTE
#define _GEN_LEVEL_E5H               true
#define _GEN_BITNUM_E5L                              5
#define _GEN_BITMASK_E5L                       (1 << 5)
#define _GEN_DRIVER_E5L_OUT         (DDRE  |=  (1 << 5))
#define _GEN_DRIVER_E5L_IN          (DDRE  &=~ (1 << 5))
#define _GEN_DRIVER_E5L_PULLUP      (PORTE |=  (1 << 5))
#define _GEN_DRIVER_E5L_HIZ         (PORTE &=~ (1 << 5))
#define _GEN_ON_E5L                 (PORTE &=~ (1 << 5))
#define _GEN_OFF_E5L                (PORTE |=  (1 << 5))
#define _GEN_ACTIVE_E5L            !(PINE  &   (1 << 5))
#define _GEN_LATCH_E5L             !(PORTE &   (1 << 5))
#define _GEN_TOGGLE_E5L             (PINE   =  (1 << 5))
#define _GEN_DDR_E5L                 DDRE
#define _GEN_PIN_E5L                 PINE
#define _GEN_PORT_E5L                PORTE
#define _GEN_LEVEL_E5L               false

#define _GEN_BITNUM_E6H                              6
#define _GEN_BITMASK_E6H                       (1 << 6)
#define _GEN_DRIVER_E6H_OUT         (DDRE  |=  (1 << 6))
#define _GEN_DRIVER_E6H_IN          (DDRE  &=~ (1 << 6))
#define _GEN_DRIVER_E6H_PULLUP      (PORTE |=  (1 << 6))
#define _GEN_DRIVER_E6H_HIZ         (PORTE &=~ (1 << 6))
#define _GEN_ON_E6H                 (PORTE |=  (1 << 6))
#define _GEN_OFF_E6H                (PORTE &=~ (1 << 6))
#define _GEN_ACTIVE_E6H             (PINE  &   (1 << 6))
#define _GEN_LATCH_E6H              (PORTE &   (1 << 6))
#define _GEN_TOGGLE_E6H             (PINE   =  (1 << 6))
#define _GEN_DDR_E6H                 DDRE
#define _GEN_PIN_E6H                 PINE
#define _GEN_PORT_E6H                PORTE
#define _GEN_LEVEL_E6H               true
#define _GEN_BITNUM_E6L                              6
#define _GEN_BITMASK_E6L                       (1 << 6)
#define _GEN_DRIVER_E6L_OUT         (DDRE  |=  (1 << 6))
#define _GEN_DRIVER_E6L_IN          (DDRE  &=~ (1 << 6))
#define _GEN_DRIVER_E6L_PULLUP      (PORTE |=  (1 << 6))
#define _GEN_DRIVER_E6L_HIZ         (PORTE &=~ (1 << 6))
#define _GEN_ON_E6L                 (PORTE &=~ (1 << 6))
#define _GEN_OFF_E6L                (PORTE |=  (1 << 6))
#define _GEN_ACTIVE_E6L            !(PINE  &   (1 << 6))
#define _GEN_LATCH_E6L             !(PORTE &   (1 << 6))
#define _GEN_TOGGLE_E6L             (PINE   =  (1 << 6))
#define _GEN_DDR_E6L                 DDRE
#define _GEN_PIN_E6L                 PINE
#define _GEN_PORT_E6L                PORTE
#define _GEN_LEVEL_E6L               false

#define _GEN_BITNUM_E7H                              7
#define _GEN_BITMASK_E7H                       (1 << 7)
#define _GEN_DRIVER_E7H_OUT         (DDRE  |=  (1 << 7))
#define _GEN_DRIVER_E7H_IN          (DDRE  &=~ (1 << 7))
#define _GEN_DRIVER_E7H_PULLUP      (PORTE |=  (1 << 7))
#define _GEN_DRIVER_E7H_HIZ         (PORTE &=~ (1 << 7))
#define _GEN_ON_E7H                 (PORTE |=  (1 << 7))
#define _GEN_OFF_E7H                (PORTE &=~ (1 << 7))
#define _GEN_ACTIVE_E7H             (PINE  &   (1 << 7))
#define _GEN_LATCH_E7H              (PORTE &   (1 << 7))
#define _GEN_TOGGLE_E7H             (PINE   =  (1 << 7))
#define _GEN_DDR_E7H                 DDRE
#define _GEN_PIN_E7H                 PINE
#define _GEN_PORT_E7H                PORTE
#define _GEN_LEVEL_E7H               true
#define _GEN_BITNUM_E7L                              7
#define _GEN_BITMASK_E7L                       (1 << 7)
#define _GEN_DRIVER_E7L_OUT         (DDRE  |=  (1 << 7))
#define _GEN_DRIVER_E7L_IN          (DDRE  &=~ (1 << 7))
#define _GEN_DRIVER_E7L_PULLUP      (PORTE |=  (1 << 7))
#define _GEN_DRIVER_E7L_HIZ         (PORTE &=~ (1 << 7))
#define _GEN_ON_E7L                 (PORTE &=~ (1 << 7))
#define _GEN_OFF_E7L                (PORTE |=  (1 << 7))
#define _GEN_ACTIVE_E7L            !(PINE  &   (1 << 7))
#define _GEN_LATCH_E7L             !(PORTE &   (1 << 7))
#define _GEN_TOGGLE_E7L             (PINE   =  (1 << 7))
#define _GEN_DDR_E7L                 DDRE
#define _GEN_PIN_E7L                 PINE
#define _GEN_PORT_E7L                PORTE
#define _GEN_LEVEL_E7L               false

#define _GEN_BITNUM_F0H                              0
#define _GEN_BITMASK_F0H                       (1 << 0)
#define _GEN_DRIVER_F0H_OUT         (DDRF  |=  (1 << 0))
#define _GEN_DRIVER_F0H_IN          (DDRF  &=~ (1 << 0))
#define _GEN_DRIVER_F0H_PULLUP      (PORTF |=  (1 << 0))
#define _GEN_DRIVER_F0H_HIZ         (PORTF &=~ (1 << 0))
#define _GEN_ON_F0H                 (PORTF |=  (1 << 0))
#define _GEN_OFF_F0H                (PORTF &=~ (1 << 0))
#define _GEN_ACTIVE_F0H             (PINF  &   (1 << 0))
#define _GEN_LATCH_F0H              (PORTF &   (1 << 0))
#define _GEN_TOGGLE_F0H             (PINF   =  (1 << 0))
#define _GEN_DDR_F0H                 DDRF
#define _GEN_PIN_F0H                 PINF
#define _GEN_PORT_F0H                PORTF
#define _GEN_LEVEL_F0H               true
#define _GEN_BITNUM_F0L                              0
#define _GEN_BITMASK_F0L                       (1 << 0)
#define _GEN_DRIVER_F0L_OUT         (DDRF  |=  (1 << 0))
#define _GEN_DRIVER_F0L_IN          (DDRF  &=~ (1 << 0))
#define _GEN_DRIVER_F0L_PULLUP      (PORTF |=  (1 << 0))
#define _GEN_DRIVER_F0L_HIZ         (PORTF &=~ (1 << 0))
#define _GEN_ON_F0L                 (PORTF &=~ (1 << 0))
#define _GEN_OFF_F0L                (PORTF |=  (1 << 0))
#define _GEN_ACTIVE_F0L            !(PINF  &   (1 << 0))
#define _GEN_LATCH_F0L             !(PORTF &   (1 << 0))
#define _GEN_TOGGLE_F0L             (PINF   =  (1 << 0))
#define _GEN_DDR_F0L                 DDRF
#define _GEN_PIN_F0L                 PINF
#define _GEN_PORT_F0L                PORTF
#define _GEN_LEVEL_F0L               false

#define _GEN_BITNUM_F1H                              1
#define _GEN_BITMASK_F1H                       (1 << 1)
#define _GEN_DRIVER_F1H_OUT         (DDRF  |=  (1 << 1))
#define _GEN_DRIVER_F1H_IN          (DDRF  &=~ (1 << 1))
#define _GEN_DRIVER_F1H_PULLUP      (PORTF |=  (1 << 1))
#define _GEN_DRIVER_F1H_HIZ         (PORTF &=~ (1 << 1))
#define _GEN_ON_F1H                 (PORTF |=  (1 << 1))
#define _GEN_OFF_F1H                (PORTF &=~ (1 << 1))
#define _GEN_ACTIVE_F1H             (PINF  &   (1 << 1))
#define _GEN_LATCH_F1H              (PORTF &   (1 << 1))
#define _GEN_TOGGLE_F1H             (PINF   =  (1 << 1))
#define _GEN_DDR_F1H                 DDRF
#define _GEN_PIN_F1H                 PINF
#define _GEN_PORT_F1H                PORTF
#define _GEN_LEVEL_F1H               true
#define _GEN_BITNUM_F1L                              1
#define _GEN_BITMASK_F1L                       (1 << 1)
#define _GEN_DRIVER_F1L_OUT         (DDRF  |=  (1 << 1))
#define _GEN_DRIVER_F1L_IN          (DDRF  &=~ (1 << 1))
#define _GEN_DRIVER_F1L_PULLUP      (PORTF |=  (1 << 1))
#define _GEN_DRIVER_F1L_HIZ         (PORTF &=~ (1 << 1))
#define _GEN_ON_F1L                 (PORTF &=~ (1 << 1))
#define _GEN_OFF_F1L                (PORTF |=  (1 << 1))
#define _GEN_ACTIVE_F1L            !(PINF  &   (1 << 1))
#define _GEN_LATCH_F1L             !(PORTF &   (1 << 1))
#define _GEN_TOGGLE_F1L             (PINF   =  (1 << 1))
#define _GEN_DDR_F1L                 DDRF
#define _GEN_PIN_F1L                 PINF
#define _GEN_PORT_F1L                PORTF
#define _GEN_LEVEL_F1L               false

#define _GEN_BITNUM_F2H                              2
#define _GEN_BITMASK_F2H                       (1 << 2)
#define _GEN_DRIVER_F2H_OUT         (DDRF  |=  (1 << 2))
#define _GEN_DRIVER_F2H_IN          (DDRF  &=~ (1 << 2))
#define _GEN_DRIVER_F2H_PULLUP      (PORTF |=  (1 << 2))
#define _GEN_DRIVER_F2H_HIZ         (PORTF &=~ (1 << 2))
#define _GEN_ON_F2H                 (PORTF |=  (1 << 2))
#define _GEN_OFF_F2H                (PORTF &=~ (1 << 2))
#define _GEN_ACTIVE_F2H             (PINF  &   (1 << 2))
#define _GEN_LATCH_F2H              (PORTF &   (1 << 2))
#define _GEN_TOGGLE_F2H             (PINF   =  (1 << 2))
#define _GEN_DDR_F2H                 DDRF
#define _GEN_PIN_F2H                 PINF
#define _GEN_PORT_F2H                PORTF
#define _GEN_LEVEL_F2H               true
#define _GEN_BITNUM_F2L                              2
#define _GEN_BITMASK_F2L                       (1 << 2)
#define _GEN_DRIVER_F2L_OUT         (DDRF  |=  (1 << 2))
#define _GEN_DRIVER_F2L_IN          (DDRF  &=~ (1 << 2))
#define _GEN_DRIVER_F2L_PULLUP      (PORTF |=  (1 << 2))
#define _GEN_DRIVER_F2L_HIZ         (PORTF &=~ (1 << 2))
#define _GEN_ON_F2L                 (PORTF &=~ (1 << 2))
#define _GEN_OFF_F2L                (PORTF |=  (1 << 2))
#define _GEN_ACTIVE_F2L            !(PINF  &   (1 << 2))
#define _GEN_LATCH_F2L             !(PORTF &   (1 << 2))
#define _GEN_TOGGLE_F2L             (PINF   =  (1 << 2))
#define _GEN_DDR_F2L                 DDRF
#define _GEN_PIN_F2L                 PINF
#define _GEN_PORT_F2L                PORTF
#define _GEN_LEVEL_F2L               false

#define _GEN_BITNUM_F3H                              3
#define _GEN_BITMASK_F3H                       (1 << 3)
#define _GEN_DRIVER_F3H_OUT         (DDRF  |=  (1 << 3))
#define _GEN_DRIVER_F3H_IN          (DDRF  &=~ (1 << 3))
#define _GEN_DRIVER_F3H_PULLUP      (PORTF |=  (1 << 3))
#define _GEN_DRIVER_F3H_HIZ         (PORTF &=~ (1 << 3))
#define _GEN_ON_F3H                 (PORTF |=  (1 << 3))
#define _GEN_OFF_F3H                (PORTF &=~ (1 << 3))
#define _GEN_ACTIVE_F3H             (PINF  &   (1 << 3))
#define _GEN_LATCH_F3H              (PORTF &   (1 << 3))
#define _GEN_TOGGLE_F3H             (PINF   =  (1 << 3))
#define _GEN_DDR_F3H                 DDRF
#define _GEN_PIN_F3H                 PINF
#define _GEN_PORT_F3H                PORTF
#define _GEN_LEVEL_F3H               true
#define _GEN_BITNUM_F3L                              3
#define _GEN_BITMASK_F3L                       (1 << 3)
#define _GEN_DRIVER_F3L_OUT         (DDRF  |=  (1 << 3))
#define _GEN_DRIVER_F3L_IN          (DDRF  &=~ (1 << 3))
#define _GEN_DRIVER_F3L_PULLUP      (PORTF |=  (1 << 3))
#define _GEN_DRIVER_F3L_HIZ         (PORTF &=~ (1 << 3))
#define _GEN_ON_F3L                 (PORTF &=~ (1 << 3))
#define _GEN_OFF_F3L                (PORTF |=  (1 << 3))
#define _GEN_ACTIVE_F3L            !(PINF  &   (1 << 3))
#define _GEN_LATCH_F3L             !(PORTF &   (1 << 3))
#define _GEN_TOGGLE_F3L             (PINF   =  (1 << 3))
#define _GEN_DDR_F3L                 DDRF
#define _GEN_PIN_F3L                 PINF
#define _GEN_PORT_F3L                PORTF
#define _GEN_LEVEL_F3L               false

#define _GEN_BITNUM_F4H                              4
#define _GEN_BITMASK_F4H                       (1 << 4)
#define _GEN_DRIVER_F4H_OUT         (DDRF  |=  (1 << 4))
#define _GEN_DRIVER_F4H_IN          (DDRF  &=~ (1 << 4))
#define _GEN_DRIVER_F4H_PULLUP      (PORTF |=  (1 << 4))
#define _GEN_DRIVER_F4H_HIZ         (PORTF &=~ (1 << 4))
#define _GEN_ON_F4H                 (PORTF |=  (1 << 4))
#define _GEN_OFF_F4H                (PORTF &=~ (1 << 4))
#define _GEN_ACTIVE_F4H             (PINF  &   (1 << 4))
#define _GEN_LATCH_F4H              (PORTF &   (1 << 4))
#define _GEN_TOGGLE_F4H             (PINF   =  (1 << 4))
#define _GEN_DDR_F4H                 DDRF
#define _GEN_PIN_F4H                 PINF
#define _GEN_PORT_F4H                PORTF
#define _GEN_LEVEL_F4H               true
#define _GEN_BITNUM_F4L                              4
#define _GEN_BITMASK_F4L                       (1 << 4)
#define _GEN_DRIVER_F4L_OUT         (DDRF  |=  (1 << 4))
#define _GEN_DRIVER_F4L_IN          (DDRF  &=~ (1 << 4))
#define _GEN_DRIVER_F4L_PULLUP      (PORTF |=  (1 << 4))
#define _GEN_DRIVER_F4L_HIZ         (PORTF &=~ (1 << 4))
#define _GEN_ON_F4L                 (PORTF &=~ (1 << 4))
#define _GEN_OFF_F4L                (PORTF |=  (1 << 4))
#define _GEN_ACTIVE_F4L            !(PINF  &   (1 << 4))
#define _GEN_LATCH_F4L             !(PORTF &   (1 << 4))
#define _GEN_TOGGLE_F4L             (PINF   =  (1 << 4))
#define _GEN_DDR_F4L                 DDRF
#define _GEN_PIN_F4L                 PINF
#define _GEN_PORT_F4L                PORTF
#define _GEN_LEVEL_F4L               false

#define _GEN_BITNUM_F5H                              5
#define _GEN_BITMASK_F5H                       (1 << 5)
#define _GEN_DRIVER_F5H_OUT         (DDRF  |=  (1 << 5))
#define _GEN_DRIVER_F5H_IN          (DDRF  &=~ (1 << 5))
#define _GEN_DRIVER_F5H_PULLUP      (PORTF |=  (1 << 5))
#define _GEN_DRIVER_F5H_HIZ         (PORTF &=~ (1 << 5))
#define _GEN_ON_F5H                 (PORTF |=  (1 << 5))
#define _GEN_OFF_F5H                (PORTF &=~ (1 << 5))
#define _GEN_ACTIVE_F5H             (PINF  &   (1 << 5))
#define _GEN_LATCH_F5H              (PORTF &   (1 << 5))
#define _GEN_TOGGLE_F5H             (PINF   =  (1 << 5))
#define _GEN_DDR_F5H                 DDRF
#define _GEN_PIN_F5H                 PINF
#define _GEN_PORT_F5H                PORTF
#define _GEN_LEVEL_F5H               true
#define _GEN_BITNUM_F5L                              5
#define _GEN_BITMASK_F5L                       (1 << 5)
#define _GEN_DRIVER_F5L_OUT         (DDRF  |=  (1 << 5))
#define _GEN_DRIVER_F5L_IN          (DDRF  &=~ (1 << 5))
#define _GEN_DRIVER_F5L_PULLUP      (PORTF |=  (1 << 5))
#define _GEN_DRIVER_F5L_HIZ         (PORTF &=~ (1 << 5))
#define _GEN_ON_F5L                 (PORTF &=~ (1 << 5))
#define _GEN_OFF_F5L                (PORTF |=  (1 << 5))
#define _GEN_ACTIVE_F5L            !(PINF  &   (1 << 5))
#define _GEN_LATCH_F5L             !(PORTF &   (1 << 5))
#define _GEN_TOGGLE_F5L             (PINF   =  (1 << 5))
#define _GEN_DDR_F5L                 DDRF
#define _GEN_PIN_F5L                 PINF
#define _GEN_PORT_F5L                PORTF
#define _GEN_LEVEL_F5L               false

#define _GEN_BITNUM_F6H                              6
#define _GEN_BITMASK_F6H                       (1 << 6)
#define _GEN_DRIVER_F6H_OUT         (DDRF  |=  (1 << 6))
#define _GEN_DRIVER_F6H_IN          (DDRF  &=~ (1 << 6))
#define _GEN_DRIVER_F6H_PULLUP      (PORTF |=  (1 << 6))
#define _GEN_DRIVER_F6H_HIZ         (PORTF &=~ (1 << 6))
#define _GEN_ON_F6H                 (PORTF |=  (1 << 6))
#define _GEN_OFF_F6H                (PORTF &=~ (1 << 6))
#define _GEN_ACTIVE_F6H             (PINF  &   (1 << 6))
#define _GEN_LATCH_F6H              (PORTF &   (1 << 6))
#define _GEN_TOGGLE_F6H             (PINF   =  (1 << 6))
#define _GEN_DDR_F6H                 DDRF
#define _GEN_PIN_F6H                 PINF
#define _GEN_PORT_F6H                PORTF
#define _GEN_LEVEL_F6H               true
#define _GEN_BITNUM_F6L                              6
#define _GEN_BITMASK_F6L                       (1 << 6)
#define _GEN_DRIVER_F6L_OUT         (DDRF  |=  (1 << 6))
#define _GEN_DRIVER_F6L_IN          (DDRF  &=~ (1 << 6))
#define _GEN_DRIVER_F6L_PULLUP      (PORTF |=  (1 << 6))
#define _GEN_DRIVER_F6L_HIZ         (PORTF &=~ (1 << 6))
#define _GEN_ON_F6L                 (PORTF &=~ (1 << 6))
#define _GEN_OFF_F6L                (PORTF |=  (1 << 6))
#define _GEN_ACTIVE_F6L            !(PINF  &   (1 << 6))
#define _GEN_LATCH_F6L             !(PORTF &   (1 << 6))
#define _GEN_TOGGLE_F6L             (PINF   =  (1 << 6))
#define _GEN_DDR_F6L                 DDRF
#define _GEN_PIN_F6L                 PINF
#define _GEN_PORT_F6L                PORTF
#define _GEN_LEVEL_F6L               false

#define _GEN_BITNUM_F7H                              7
#define _GEN_BITMASK_F7H                       (1 << 7)
#define _GEN_DRIVER_F7H_OUT         (DDRF  |=  (1 << 7))
#define _GEN_DRIVER_F7H_IN          (DDRF  &=~ (1 << 7))
#define _GEN_DRIVER_F7H_PULLUP      (PORTF |=  (1 << 7))
#define _GEN_DRIVER_F7H_HIZ         (PORTF &=~ (1 << 7))
#define _GEN_ON_F7H                 (PORTF |=  (1 << 7))
#define _GEN_OFF_F7H                (PORTF &=~ (1 << 7))
#define _GEN_ACTIVE_F7H             (PINF  &   (1 << 7))
#define _GEN_LATCH_F7H              (PORTF &   (1 << 7))
#define _GEN_TOGGLE_F7H             (PINF   =  (1 << 7))
#define _GEN_DDR_F7H                 DDRF
#define _GEN_PIN_F7H                 PINF
#define _GEN_PORT_F7H                PORTF
#define _GEN_LEVEL_F7H               true
#define _GEN_BITNUM_F7L                              7
#define _GEN_BITMASK_F7L                       (1 << 7)
#define _GEN_DRIVER_F7L_OUT         (DDRF  |=  (1 << 7))
#define _GEN_DRIVER_F7L_IN          (DDRF  &=~ (1 << 7))
#define _GEN_DRIVER_F7L_PULLUP      (PORTF |=  (1 << 7))
#define _GEN_DRIVER_F7L_HIZ         (PORTF &=~ (1 << 7))
#define _GEN_ON_F7L                 (PORTF &=~ (1 << 7))
#define _GEN_OFF_F7L                (PORTF |=  (1 << 7))
#define _GEN_ACTIVE_F7L            !(PINF  &   (1 << 7))
#define _GEN_LATCH_F7L             !(PORTF &   (1 << 7))
#define _GEN_TOGGLE_F7L             (PINF   =  (1 << 7))
#define _GEN_DDR_F7L                 DDRF
#define _GEN_PIN_F7L                 PINF
#define _GEN_PORT_F7L                PORTF
#define _GEN_LEVEL_F7L               false

#define _GEN_BITNUM_G0H                              0
#define _GEN_BITMASK_G0H                       (1 << 0)
#define _GEN_DRIVER_G0H_OUT         (DDRG  |=  (1 << 0))
#define _GEN_DRIVER_G0H_IN          (DDRG  &=~ (1 << 0))
#define _GEN_DRIVER_G0H_PULLUP      (PORTG |=  (1 << 0))
#define _GEN_DRIVER_G0H_HIZ         (PORTG &=~ (1 << 0))
#define _GEN_ON_G0H                 (PORTG |=  (1 << 0))
#define _GEN_OFF_G0H                (PORTG &=~ (1 << 0))
#define _GEN_ACTIVE_G0H             (PING  &   (1 << 0))
#define _GEN_LATCH_G0H              (PORTG &   (1 << 0))
#define _GEN_TOGGLE_G0H             (PING   =  (1 << 0))
#define _GEN_DDR_G0H                 DDRG
#define _GEN_PIN_G0H                 PING
#define _GEN_PORT_G0H                PORTG
#define _GEN_LEVEL_G0H               true
#define _GEN_BITNUM_G0L                              0
#define _GEN_BITMASK_G0L                       (1 << 0)
#define _GEN_DRIVER_G0L_OUT         (DDRG  |=  (1 << 0))
#define _GEN_DRIVER_G0L_IN          (DDRG  &=~ (1 << 0))
#define _GEN_DRIVER_G0L_PULLUP      (PORTG |=  (1 << 0))
#define _GEN_DRIVER_G0L_HIZ         (PORTG &=~ (1 << 0))
#define _GEN_ON_G0L                 (PORTG &=~ (1 << 0))
#define _GEN_OFF_G0L                (PORTG |=  (1 << 0))
#define _GEN_ACTIVE_G0L            !(PING  &   (1 << 0))
#define _GEN_LATCH_G0L             !(PORTG &   (1 << 0))
#define _GEN_TOGGLE_G0L             (PING   =  (1 << 0))
#define _GEN_DDR_G0L                 DDRG
#define _GEN_PIN_G0L                 PING
#define _GEN_PORT_G0L                PORTG
#define _GEN_LEVEL_G0L               false

#define _GEN_BITNUM_G1H                              1
#define _GEN_BITMASK_G1H                       (1 << 1)
#define _GEN_DRIVER_G1H_OUT         (DDRG  |=  (1 << 1))
#define _GEN_DRIVER_G1H_IN          (DDRG  &=~ (1 << 1))
#define _GEN_DRIVER_G1H_PULLUP      (PORTG |=  (1 << 1))
#define _GEN_DRIVER_G1H_HIZ         (PORTG &=~ (1 << 1))
#define _GEN_ON_G1H                 (PORTG |=  (1 << 1))
#define _GEN_OFF_G1H                (PORTG &=~ (1 << 1))
#define _GEN_ACTIVE_G1H             (PING  &   (1 << 1))
#define _GEN_LATCH_G1H              (PORTG &   (1 << 1))
#define _GEN_TOGGLE_G1H             (PING   =  (1 << 1))
#define _GEN_DDR_G1H                 DDRG
#define _GEN_PIN_G1H                 PING
#define _GEN_PORT_G1H                PORTG
#define _GEN_LEVEL_G1H               true
#define _GEN_BITNUM_G1L                              1
#define _GEN_BITMASK_G1L                       (1 << 1)
#define _GEN_DRIVER_G1L_OUT         (DDRG  |=  (1 << 1))
#define _GEN_DRIVER_G1L_IN          (DDRG  &=~ (1 << 1))
#define _GEN_DRIVER_G1L_PULLUP      (PORTG |=  (1 << 1))
#define _GEN_DRIVER_G1L_HIZ         (PORTG &=~ (1 << 1))
#define _GEN_ON_G1L                 (PORTG &=~ (1 << 1))
#define _GEN_OFF_G1L                (PORTG |=  (1 << 1))
#define _GEN_ACTIVE_G1L            !(PING  &   (1 << 1))
#define _GEN_LATCH_G1L             !(PORTG &   (1 << 1))
#define _GEN_TOGGLE_G1L             (PING   =  (1 << 1))
#define _GEN_DDR_G1L                 DDRG
#define _GEN_PIN_G1L                 PING
#define _GEN_PORT_G1L                PORTG
#define _GEN_LEVEL_G1L               false

#define _GEN_BITNUM_G2H                              2
#define _GEN_BITMASK_G2H                       (1 << 2)
#define _GEN_DRIVER_G2H_OUT         (DDRG  |=  (1 << 2))
#define _GEN_DRIVER_G2H_IN          (DDRG  &=~ (1 << 2))
#define _GEN_DRIVER_G2H_PULLUP      (PORTG |=  (1 << 2))
#define _GEN_DRIVER_G2H_HIZ         (PORTG &=~ (1 << 2))
#define _GEN_ON_G2H                 (PORTG |=  (1 << 2))
#define _GEN_OFF_G2H                (PORTG &=~ (1 << 2))
#define _GEN_ACTIVE_G2H             (PING  &   (1 << 2))
#define _GEN_LATCH_G2H              (PORTG &   (1 << 2))
#define _GEN_TOGGLE_G2H             (PING   =  (1 << 2))
#define _GEN_DDR_G2H                 DDRG
#define _GEN_PIN_G2H                 PING
#define _GEN_PORT_G2H                PORTG
#define _GEN_LEVEL_G2H               true
#define _GEN_BITNUM_G2L                              2
#define _GEN_BITMASK_G2L                       (1 << 2)
#define _GEN_DRIVER_G2L_OUT         (DDRG  |=  (1 << 2))
#define _GEN_DRIVER_G2L_IN          (DDRG  &=~ (1 << 2))
#define _GEN_DRIVER_G2L_PULLUP      (PORTG |=  (1 << 2))
#define _GEN_DRIVER_G2L_HIZ         (PORTG &=~ (1 << 2))
#define _GEN_ON_G2L                 (PORTG &=~ (1 << 2))
#define _GEN_OFF_G2L                (PORTG |=  (1 << 2))
#define _GEN_ACTIVE_G2L            !(PING  &   (1 << 2))
#define _GEN_LATCH_G2L             !(PORTG &   (1 << 2))
#define _GEN_TOGGLE_G2L             (PING   =  (1 << 2))
#define _GEN_DDR_G2L                 DDRG
#define _GEN_PIN_G2L                 PING
#define _GEN_PORT_G2L                PORTG
#define _GEN_LEVEL_G2L               false

#define _GEN_BITNUM_G3H                              3
#define _GEN_BITMASK_G3H                       (1 << 3)
#define _GEN_DRIVER_G3H_OUT         (DDRG  |=  (1 << 3))
#define _GEN_DRIVER_G3H_IN          (DDRG  &=~ (1 << 3))
#define _GEN_DRIVER_G3H_PULLUP      (PORTG |=  (1 << 3))
#define _GEN_DRIVER_G3H_HIZ         (PORTG &=~ (1 << 3))
#define _GEN_ON_G3H                 (PORTG |=  (1 << 3))
#define _GEN_OFF_G3H                (PORTG &=~ (1 << 3))
#define _GEN_ACTIVE_G3H             (PING  &   (1 << 3))
#define _GEN_LATCH_G3H              (PORTG &   (1 << 3))
#define _GEN_TOGGLE_G3H             (PING   =  (1 << 3))
#define _GEN_DDR_G3H                 DDRG
#define _GEN_PIN_G3H                 PING
#define _GEN_PORT_G3H                PORTG
#define _GEN_LEVEL_G3H               true
#define _GEN_BITNUM_G3L                              3
#define _GEN_BITMASK_G3L                       (1 << 3)
#define _GEN_DRIVER_G3L_OUT         (DDRG  |=  (1 << 3))
#define _GEN_DRIVER_G3L_IN          (DDRG  &=~ (1 << 3))
#define _GEN_DRIVER_G3L_PULLUP      (PORTG |=  (1 << 3))
#define _GEN_DRIVER_G3L_HIZ         (PORTG &=~ (1 << 3))
#define _GEN_ON_G3L                 (PORTG &=~ (1 << 3))
#define _GEN_OFF_G3L                (PORTG |=  (1 << 3))
#define _GEN_ACTIVE_G3L            !(PING  &   (1 << 3))
#define _GEN_LATCH_G3L             !(PORTG &   (1 << 3))
#define _GEN_TOGGLE_G3L             (PING   =  (1 << 3))
#define _GEN_DDR_G3L                 DDRG
#define _GEN_PIN_G3L                 PING
#define _GEN_PORT_G3L                PORTG
#define _GEN_LEVEL_G3L               false

#define _GEN_BITNUM_G4H                              4
#define _GEN_BITMASK_G4H                       (1 << 4)
#define _GEN_DRIVER_G4H_OUT         (DDRG  |=  (1 << 4))
#define _GEN_DRIVER_G4H_IN          (DDRG  &=~ (1 << 4))
#define _GEN_DRIVER_G4H_PULLUP      (PORTG |=  (1 << 4))
#define _GEN_DRIVER_G4H_HIZ         (PORTG &=~ (1 << 4))
#define _GEN_ON_G4H                 (PORTG |=  (1 << 4))
#define _GEN_OFF_G4H                (PORTG &=~ (1 << 4))
#define _GEN_ACTIVE_G4H             (PING  &   (1 << 4))
#define _GEN_LATCH_G4H              (PORTG &   (1 << 4))
#define _GEN_TOGGLE_G4H             (PING   =  (1 << 4))
#define _GEN_DDR_G4H                 DDRG
#define _GEN_PIN_G4H                 PING
#define _GEN_PORT_G4H                PORTG
#define _GEN_LEVEL_G4H               true
#define _GEN_BITNUM_G4L                              4
#define _GEN_BITMASK_G4L                       (1 << 4)
#define _GEN_DRIVER_G4L_OUT         (DDRG  |=  (1 << 4))
#define _GEN_DRIVER_G4L_IN          (DDRG  &=~ (1 << 4))
#define _GEN_DRIVER_G4L_PULLUP      (PORTG |=  (1 << 4))
#define _GEN_DRIVER_G4L_HIZ         (PORTG &=~ (1 << 4))
#define _GEN_ON_G4L                 (PORTG &=~ (1 << 4))
#define _GEN_OFF_G4L                (PORTG |=  (1 << 4))
#define _GEN_ACTIVE_G4L            !(PING  &   (1 << 4))
#define _GEN_LATCH_G4L             !(PORTG &   (1 << 4))
#define _GEN_TOGGLE_G4L             (PING   =  (1 << 4))
#define _GEN_DDR_G4L                 DDRG
#define _GEN_PIN_G4L                 PING
#define _GEN_PORT_G4L                PORTG
#define _GEN_LEVEL_G4L               false

#define _GEN_BITNUM_G5H                              5
#define _GEN_BITMASK_G5H                       (1 << 5)
#define _GEN_DRIVER_G5H_OUT         (DDRG  |=  (1 << 5))
#define _GEN_DRIVER_G5H_IN          (DDRG  &=~ (1 << 5))
#define _GEN_DRIVER_G5H_PULLUP      (PORTG |=  (1 << 5))
#define _GEN_DRIVER_G5H_HIZ         (PORTG &=~ (1 << 5))
#define _GEN_ON_G5H                 (PORTG |=  (1 << 5))
#define _GEN_OFF_G5H                (PORTG &=~ (1 << 5))
#define _GEN_ACTIVE_G5H             (PING  &   (1 << 5))
#define _GEN_LATCH_G5H              (PORTG &   (1 << 5))
#define _GEN_TOGGLE_G5H             (PING   =  (1 << 5))
#define _GEN_DDR_G5H                 DDRG
#define _GEN_PIN_G5H                 PING
#define _GEN_PORT_G5H                PORTG
#define _GEN_LEVEL_G5H               true
#define _GEN_BITNUM_G5L                              5
#define _GEN_BITMASK_G5L                       (1 << 5)
#define _GEN_DRIVER_G5L_OUT         (DDRG  |=  (1 << 5))
#define _GEN_DRIVER_G5L_IN          (DDRG  &=~ (1 << 5))
#define _GEN_DRIVER_G5L_PULLUP      (PORTG |=  (1 << 5))
#define _GEN_DRIVER_G5L_HIZ         (PORTG &=~ (1 << 5))
#define _GEN_ON_G5L                 (PORTG &=~ (1 << 5))
#define _GEN_OFF_G5L                (PORTG |=  (1 << 5))
#define _GEN_ACTIVE_G5L            !(PING  &   (1 << 5))
#define _GEN_LATCH_G5L             !(PORTG &   (1 << 5))
#define _GEN_TOGGLE_G5L             (PING   =  (1 << 5))
#define _GEN_DDR_G5L                 DDRG
#define _GEN_PIN_G5L                 PING
#define _GEN_PORT_G5L                PORTG
#define _GEN_LEVEL_G5L               false

#define _GEN_BITNUM_G6H                              6
#define _GEN_BITMASK_G6H                       (1 << 6)
#define _GEN_DRIVER_G6H_OUT         (DDRG  |=  (1 << 6))
#define _GEN_DRIVER_G6H_IN          (DDRG  &=~ (1 << 6))
#define _GEN_DRIVER_G6H_PULLUP      (PORTG |=  (1 << 6))
#define _GEN_DRIVER_G6H_HIZ         (PORTG &=~ (1 << 6))
#define _GEN_ON_G6H                 (PORTG |=  (1 << 6))
#define _GEN_OFF_G6H                (PORTG &=~ (1 << 6))
#define _GEN_ACTIVE_G6H             (PING  &   (1 << 6))
#define _GEN_LATCH_G6H              (PORTG &   (1 << 6))
#define _GEN_TOGGLE_G6H             (PING   =  (1 << 6))
#define _GEN_DDR_G6H                 DDRG
#define _GEN_PIN_G6H                 PING
#define _GEN_PORT_G6H                PORTG
#define _GEN_LEVEL_G6H               true
#define _GEN_BITNUM_G6L                              6
#define _GEN_BITMASK_G6L                       (1 << 6)
#define _GEN_DRIVER_G6L_OUT         (DDRG  |=  (1 << 6))
#define _GEN_DRIVER_G6L_IN          (DDRG  &=~ (1 << 6))
#define _GEN_DRIVER_G6L_PULLUP      (PORTG |=  (1 << 6))
#define _GEN_DRIVER_G6L_HIZ         (PORTG &=~ (1 << 6))
#define _GEN_ON_G6L                 (PORTG &=~ (1 << 6))
#define _GEN_OFF_G6L                (PORTG |=  (1 << 6))
#define _GEN_ACTIVE_G6L            !(PING  &   (1 << 6))
#define _GEN_LATCH_G6L             !(PORTG &   (1 << 6))
#define _GEN_TOGGLE_G6L             (PING   =  (1 << 6))
#define _GEN_DDR_G6L                 DDRG
#define _GEN_PIN_G6L                 PING
#define _GEN_PORT_G6L                PORTG
#define _GEN_LEVEL_G6L               false

#define _GEN_BITNUM_G7H                              7
#define _GEN_BITMASK_G7H                       (1 << 7)
#define _GEN_DRIVER_G7H_OUT         (DDRG  |=  (1 << 7))
#define _GEN_DRIVER_G7H_IN          (DDRG  &=~ (1 << 7))
#define _GEN_DRIVER_G7H_PULLUP      (PORTG |=  (1 << 7))
#define _GEN_DRIVER_G7H_HIZ         (PORTG &=~ (1 << 7))
#define _GEN_ON_G7H                 (PORTG |=  (1 << 7))
#define _GEN_OFF_G7H                (PORTG &=~ (1 << 7))
#define _GEN_ACTIVE_G7H             (PING  &   (1 << 7))
#define _GEN_LATCH_G7H              (PORTG &   (1 << 7))
#define _GEN_TOGGLE_G7H             (PING   =  (1 << 7))
#define _GEN_DDR_G7H                 DDRG
#define _GEN_PIN_G7H                 PING
#define _GEN_PORT_G7H                PORTG
#define _GEN_LEVEL_G7H               true
#define _GEN_BITNUM_G7L                              7
#define _GEN_BITMASK_G7L                       (1 << 7)
#define _GEN_DRIVER_G7L_OUT         (DDRG  |=  (1 << 7))
#define _GEN_DRIVER_G7L_IN          (DDRG  &=~ (1 << 7))
#define _GEN_DRIVER_G7L_PULLUP      (PORTG |=  (1 << 7))
#define _GEN_DRIVER_G7L_HIZ         (PORTG &=~ (1 << 7))
#define _GEN_ON_G7L                 (PORTG &=~ (1 << 7))
#define _GEN_OFF_G7L                (PORTG |=  (1 << 7))
#define _GEN_ACTIVE_G7L            !(PING  &   (1 << 7))
#define _GEN_LATCH_G7L             !(PORTG &   (1 << 7))
#define _GEN_TOGGLE_G7L             (PING   =  (1 << 7))
#define _GEN_DDR_G7L                 DDRG
#define _GEN_PIN_G7L                 PING
#define _GEN_PORT_G7L                PORTG
#define _GEN_LEVEL_G7L               false

#define _GEN_BITNUM_H0H                              0
#define _GEN_BITMASK_H0H                       (1 << 0)
#define _GEN_DRIVER_H0H_OUT         (DDRH  |=  (1 << 0))
#define _GEN_DRIVER_H0H_IN          (DDRH  &=~ (1 << 0))
#define _GEN_DRIVER_H0H_PULLUP      (PORTH |=  (1 << 0))
#define _GEN_DRIVER_H0H_HIZ         (PORTH &=~ (1 << 0))
#define _GEN_ON_H0H                 (PORTH |=  (1 << 0))
#define _GEN_OFF_H0H                (PORTH &=~ (1 << 0))
#define _GEN_ACTIVE_H0H             (PINH  &   (1 << 0))
#define _GEN_LATCH_H0H              (PORTH &   (1 << 0))
#define _GEN_TOGGLE_H0H             (PINH   =  (1 << 0))
#define _GEN_DDR_H0H                 DDRH
#define _GEN_PIN_H0H                 PINH
#define _GEN_PORT_H0H                PORTH
#define _GEN_LEVEL_H0H               true
#define _GEN_BITNUM_H0L                              0
#define _GEN_BITMASK_H0L                       (1 << 0)
#define _GEN_DRIVER_H0L_OUT         (DDRH  |=  (1 << 0))
#define _GEN_DRIVER_H0L_IN          (DDRH  &=~ (1 << 0))
#define _GEN_DRIVER_H0L_PULLUP      (PORTH |=  (1 << 0))
#define _GEN_DRIVER_H0L_HIZ         (PORTH &=~ (1 << 0))
#define _GEN_ON_H0L                 (PORTH &=~ (1 << 0))
#define _GEN_OFF_H0L                (PORTH |=  (1 << 0))
#define _GEN_ACTIVE_H0L            !(PINH  &   (1 << 0))
#define _GEN_LATCH_H0L             !(PORTH &   (1 << 0))
#define _GEN_TOGGLE_H0L             (PINH   =  (1 << 0))
#define _GEN_DDR_H0L                 DDRH
#define _GEN_PIN_H0L                 PINH
#define _GEN_PORT_H0L                PORTH
#define _GEN_LEVEL_H0L               false

#define _GEN_BITNUM_H1H                              1
#define _GEN_BITMASK_H1H                       (1 << 1)
#define _GEN_DRIVER_H1H_OUT         (DDRH  |=  (1 << 1))
#define _GEN_DRIVER_H1H_IN          (DDRH  &=~ (1 << 1))
#define _GEN_DRIVER_H1H_PULLUP      (PORTH |=  (1 << 1))
#define _GEN_DRIVER_H1H_HIZ         (PORTH &=~ (1 << 1))
#define _GEN_ON_H1H                 (PORTH |=  (1 << 1))
#define _GEN_OFF_H1H                (PORTH &=~ (1 << 1))
#define _GEN_ACTIVE_H1H             (PINH  &   (1 << 1))
#define _GEN_LATCH_H1H              (PORTH &   (1 << 1))
#define _GEN_TOGGLE_H1H             (PINH   =  (1 << 1))
#define _GEN_DDR_H1H                 DDRH
#define _GEN_PIN_H1H                 PINH
#define _GEN_PORT_H1H                PORTH
#define _GEN_LEVEL_H1H               true
#define _GEN_BITNUM_H1L                              1
#define _GEN_BITMASK_H1L                       (1 << 1)
#define _GEN_DRIVER_H1L_OUT         (DDRH  |=  (1 << 1))
#define _GEN_DRIVER_H1L_IN          (DDRH  &=~ (1 << 1))
#define _GEN_DRIVER_H1L_PULLUP      (PORTH |=  (1 << 1))
#define _GEN_DRIVER_H1L_HIZ         (PORTH &=~ (1 << 1))
#define _GEN_ON_H1L                 (PORTH &=~ (1 << 1))
#define _GEN_OFF_H1L                (PORTH |=  (1 << 1))
#define _GEN_ACTIVE_H1L            !(PINH  &   (1 << 1))
#define _GEN_LATCH_H1L             !(PORTH &   (1 << 1))
#define _GEN_TOGGLE_H1L             (PINH   =  (1 << 1))
#define _GEN_DDR_H1L                 DDRH
#define _GEN_PIN_H1L                 PINH
#define _GEN_PORT_H1L                PORTH
#define _GEN_LEVEL_H1L               false

#define _GEN_BITNUM_H2H                              2
#define _GEN_BITMASK_H2H                       (1 << 2)
#define _GEN_DRIVER_H2H_OUT         (DDRH  |=  (1 << 2))
#define _GEN_DRIVER_H2H_IN          (DDRH  &=~ (1 << 2))
#define _GEN_DRIVER_H2H_PULLUP      (PORTH |=  (1 << 2))
#define _GEN_DRIVER_H2H_HIZ         (PORTH &=~ (1 << 2))
#define _GEN_ON_H2H                 (PORTH |=  (1 << 2))
#define _GEN_OFF_H2H                (PORTH &=~ (1 << 2))
#define _GEN_ACTIVE_H2H             (PINH  &   (1 << 2))
#define _GEN_LATCH_H2H              (PORTH &   (1 << 2))
#define _GEN_TOGGLE_H2H             (PINH   =  (1 << 2))
#define _GEN_DDR_H2H                 DDRH
#define _GEN_PIN_H2H                 PINH
#define _GEN_PORT_H2H                PORTH
#define _GEN_LEVEL_H2H               true
#define _GEN_BITNUM_H2L                              2
#define _GEN_BITMASK_H2L                       (1 << 2)
#define _GEN_DRIVER_H2L_OUT         (DDRH  |=  (1 << 2))
#define _GEN_DRIVER_H2L_IN          (DDRH  &=~ (1 << 2))
#define _GEN_DRIVER_H2L_PULLUP      (PORTH |=  (1 << 2))
#define _GEN_DRIVER_H2L_HIZ         (PORTH &=~ (1 << 2))
#define _GEN_ON_H2L                 (PORTH &=~ (1 << 2))
#define _GEN_OFF_H2L                (PORTH |=  (1 << 2))
#define _GEN_ACTIVE_H2L            !(PINH  &   (1 << 2))
#define _GEN_LATCH_H2L             !(PORTH &   (1 << 2))
#define _GEN_TOGGLE_H2L             (PINH   =  (1 << 2))
#define _GEN_DDR_H2L                 DDRH
#define _GEN_PIN_H2L                 PINH
#define _GEN_PORT_H2L                PORTH
#define _GEN_LEVEL_H2L               false

#define _GEN_BITNUM_H3H                              3
#define _GEN_BITMASK_H3H                       (1 << 3)
#define _GEN_DRIVER_H3H_OUT         (DDRH  |=  (1 << 3))
#define _GEN_DRIVER_H3H_IN          (DDRH  &=~ (1 << 3))
#define _GEN_DRIVER_H3H_PULLUP      (PORTH |=  (1 << 3))
#define _GEN_DRIVER_H3H_HIZ         (PORTH &=~ (1 << 3))
#define _GEN_ON_H3H                 (PORTH |=  (1 << 3))
#define _GEN_OFF_H3H                (PORTH &=~ (1 << 3))
#define _GEN_ACTIVE_H3H             (PINH  &   (1 << 3))
#define _GEN_LATCH_H3H              (PORTH &   (1 << 3))
#define _GEN_TOGGLE_H3H             (PINH   =  (1 << 3))
#define _GEN_DDR_H3H                 DDRH
#define _GEN_PIN_H3H                 PINH
#define _GEN_PORT_H3H                PORTH
#define _GEN_LEVEL_H3H               true
#define _GEN_BITNUM_H3L                              3
#define _GEN_BITMASK_H3L                       (1 << 3)
#define _GEN_DRIVER_H3L_OUT         (DDRH  |=  (1 << 3))
#define _GEN_DRIVER_H3L_IN          (DDRH  &=~ (1 << 3))
#define _GEN_DRIVER_H3L_PULLUP      (PORTH |=  (1 << 3))
#define _GEN_DRIVER_H3L_HIZ         (PORTH &=~ (1 << 3))
#define _GEN_ON_H3L                 (PORTH &=~ (1 << 3))
#define _GEN_OFF_H3L                (PORTH |=  (1 << 3))
#define _GEN_ACTIVE_H3L            !(PINH  &   (1 << 3))
#define _GEN_LATCH_H3L             !(PORTH &   (1 << 3))
#define _GEN_TOGGLE_H3L             (PINH   =  (1 << 3))
#define _GEN_DDR_H3L                 DDRH
#define _GEN_PIN_H3L                 PINH
#define _GEN_PORT_H3L                PORTH
#define _GEN_LEVEL_H3L               false

#define _GEN_BITNUM_H4H                              4
#define _GEN_BITMASK_H4H                       (1 << 4)
#define _GEN_DRIVER_H4H_OUT         (DDRH  |=  (1 << 4))
#define _GEN_DRIVER_H4H_IN          (DDRH  &=~ (1 << 4))
#define _GEN_DRIVER_H4H_PULLUP      (PORTH |=  (1 << 4))
#define _GEN_DRIVER_H4H_HIZ         (PORTH &=~ (1 << 4))
#define _GEN_ON_H4H                 (PORTH |=  (1 << 4))
#define _GEN_OFF_H4H                (PORTH &=~ (1 << 4))
#define _GEN_ACTIVE_H4H             (PINH  &   (1 << 4))
#define _GEN_LATCH_H4H              (PORTH &   (1 << 4))
#define _GEN_TOGGLE_H4H             (PINH   =  (1 << 4))
#define _GEN_DDR_H4H                 DDRH
#define _GEN_PIN_H4H                 PINH
#define _GEN_PORT_H4H                PORTH
#define _GEN_LEVEL_H4H               true
#define _GEN_BITNUM_H4L                              4
#define _GEN_BITMASK_H4L                       (1 << 4)
#define _GEN_DRIVER_H4L_OUT         (DDRH  |=  (1 << 4))
#define _GEN_DRIVER_H4L_IN          (DDRH  &=~ (1 << 4))
#define _GEN_DRIVER_H4L_PULLUP      (PORTH |=  (1 << 4))
#define _GEN_DRIVER_H4L_HIZ         (PORTH &=~ (1 << 4))
#define _GEN_ON_H4L                 (PORTH &=~ (1 << 4))
#define _GEN_OFF_H4L                (PORTH |=  (1 << 4))
#define _GEN_ACTIVE_H4L            !(PINH  &   (1 << 4))
#define _GEN_LATCH_H4L             !(PORTH &   (1 << 4))
#define _GEN_TOGGLE_H4L             (PINH   =  (1 << 4))
#define _GEN_DDR_H4L                 DDRH
#define _GEN_PIN_H4L                 PINH
#define _GEN_PORT_H4L                PORTH
#define _GEN_LEVEL_H4L               false

#define _GEN_BITNUM_H5H                              5
#define _GEN_BITMASK_H5H                       (1 << 5)
#define _GEN_DRIVER_H5H_OUT         (DDRH  |=  (1 << 5))
#define _GEN_DRIVER_H5H_IN          (DDRH  &=~ (1 << 5))
#define _GEN_DRIVER_H5H_PULLUP      (PORTH |=  (1 << 5))
#define _GEN_DRIVER_H5H_HIZ         (PORTH &=~ (1 << 5))
#define _GEN_ON_H5H                 (PORTH |=  (1 << 5))
#define _GEN_OFF_H5H                (PORTH &=~ (1 << 5))
#define _GEN_ACTIVE_H5H             (PINH  &   (1 << 5))
#define _GEN_LATCH_H5H              (PORTH &   (1 << 5))
#define _GEN_TOGGLE_H5H             (PINH   =  (1 << 5))
#define _GEN_DDR_H5H                 DDRH
#define _GEN_PIN_H5H                 PINH
#define _GEN_PORT_H5H                PORTH
#define _GEN_LEVEL_H5H               true
#define _GEN_BITNUM_H5L                              5
#define _GEN_BITMASK_H5L                       (1 << 5)
#define _GEN_DRIVER_H5L_OUT         (DDRH  |=  (1 << 5))
#define _GEN_DRIVER_H5L_IN          (DDRH  &=~ (1 << 5))
#define _GEN_DRIVER_H5L_PULLUP      (PORTH |=  (1 << 5))
#define _GEN_DRIVER_H5L_HIZ         (PORTH &=~ (1 << 5))
#define _GEN_ON_H5L                 (PORTH &=~ (1 << 5))
#define _GEN_OFF_H5L                (PORTH |=  (1 << 5))
#define _GEN_ACTIVE_H5L            !(PINH  &   (1 << 5))
#define _GEN_LATCH_H5L             !(PORTH &   (1 << 5))
#define _GEN_TOGGLE_H5L             (PINH   =  (1 << 5))
#define _GEN_DDR_H5L                 DDRH
#define _GEN_PIN_H5L                 PINH
#define _GEN_PORT_H5L                PORTH
#define _GEN_LEVEL_H5L               false

#define _GEN_BITNUM_H6H                              6
#define _GEN_BITMASK_H6H                       (1 << 6)
#define _GEN_DRIVER_H6H_OUT         (DDRH  |=  (1 << 6))
#define _GEN_DRIVER_H6H_IN          (DDRH  &=~ (1 << 6))
#define _GEN_DRIVER_H6H_PULLUP      (PORTH |=  (1 << 6))
#define _GEN_DRIVER_H6H_HIZ         (PORTH &=~ (1 << 6))
#define _GEN_ON_H6H                 (PORTH |=  (1 << 6))
#define _GEN_OFF_H6H                (PORTH &=~ (1 << 6))
#define _GEN_ACTIVE_H6H             (PINH  &   (1 << 6))
#define _GEN_LATCH_H6H              (PORTH &   (1 << 6))
#define _GEN_TOGGLE_H6H             (PINH   =  (1 << 6))
#define _GEN_DDR_H6H                 DDRH
#define _GEN_PIN_H6H                 PINH
#define _GEN_PORT_H6H                PORTH
#define _GEN_LEVEL_H6H               true
#define _GEN_BITNUM_H6L                              6
#define _GEN_BITMASK_H6L                       (1 << 6)
#define _GEN_DRIVER_H6L_OUT         (DDRH  |=  (1 << 6))
#define _GEN_DRIVER_H6L_IN          (DDRH  &=~ (1 << 6))
#define _GEN_DRIVER_H6L_PULLUP      (PORTH |=  (1 << 6))
#define _GEN_DRIVER_H6L_HIZ         (PORTH &=~ (1 << 6))
#define _GEN_ON_H6L                 (PORTH &=~ (1 << 6))
#define _GEN_OFF_H6L                (PORTH |=  (1 << 6))
#define _GEN_ACTIVE_H6L            !(PINH  &   (1 << 6))
#define _GEN_LATCH_H6L             !(PORTH &   (1 << 6))
#define _GEN_TOGGLE_H6L             (PINH   =  (1 << 6))
#define _GEN_DDR_H6L                 DDRH
#define _GEN_PIN_H6L                 PINH
#define _GEN_PORT_H6L                PORTH
#define _GEN_LEVEL_H6L               false

#define _GEN_BITNUM_H7H                              7
#define _GEN_BITMASK_H7H                       (1 << 7)
#define _GEN_DRIVER_H7H_OUT         (DDRH  |=  (1 << 7))
#define _GEN_DRIVER_H7H_IN          (DDRH  &=~ (1 << 7))
#define _GEN_DRIVER_H7H_PULLUP      (PORTH |=  (1 << 7))
#define _GEN_DRIVER_H7H_HIZ         (PORTH &=~ (1 << 7))
#define _GEN_ON_H7H                 (PORTH |=  (1 << 7))
#define _GEN_OFF_H7H                (PORTH &=~ (1 << 7))
#define _GEN_ACTIVE_H7H             (PINH  &   (1 << 7))
#define _GEN_LATCH_H7H              (PORTH &   (1 << 7))
#define _GEN_TOGGLE_H7H             (PINH   =  (1 << 7))
#define _GEN_DDR_H7H                 DDRH
#define _GEN_PIN_H7H                 PINH
#define _GEN_PORT_H7H                PORTH
#define _GEN_LEVEL_H7H               true
#define _GEN_BITNUM_H7L                              7
#define _GEN_BITMASK_H7L                       (1 << 7)
#define _GEN_DRIVER_H7L_OUT         (DDRH  |=  (1 << 7))
#define _GEN_DRIVER_H7L_IN          (DDRH  &=~ (1 << 7))
#define _GEN_DRIVER_H7L_PULLUP      (PORTH |=  (1 << 7))
#define _GEN_DRIVER_H7L_HIZ         (PORTH &=~ (1 << 7))
#define _GEN_ON_H7L                 (PORTH &=~ (1 << 7))
#define _GEN_OFF_H7L                (PORTH |=  (1 << 7))
#define _GEN_ACTIVE_H7L            !(PINH  &   (1 << 7))
#define _GEN_LATCH_H7L             !(PORTH &   (1 << 7))
#define _GEN_TOGGLE_H7L             (PINH   =  (1 << 7))
#define _GEN_DDR_H7L                 DDRH
#define _GEN_PIN_H7L                 PINH
#define _GEN_PORT_H7L                PORTH
#define _GEN_LEVEL_H7L               false

#define _GEN_BITNUM_I0H                              0
#define _GEN_BITMASK_I0H                       (1 << 0)
#define _GEN_DRIVER_I0H_OUT         (DDRI  |=  (1 << 0))
#define _GEN_DRIVER_I0H_IN          (DDRI  &=~ (1 << 0))
#define _GEN_DRIVER_I0H_PULLUP      (PORTI |=  (1 << 0))
#define _GEN_DRIVER_I0H_HIZ         (PORTI &=~ (1 << 0))
#define _GEN_ON_I0H                 (PORTI |=  (1 << 0))
#define _GEN_OFF_I0H                (PORTI &=~ (1 << 0))
#define _GEN_ACTIVE_I0H             (PINI  &   (1 << 0))
#define _GEN_LATCH_I0H              (PORTI &   (1 << 0))
#define _GEN_TOGGLE_I0H             (PINI   =  (1 << 0))
#define _GEN_DDR_I0H                 DDRI
#define _GEN_PIN_I0H                 PINI
#define _GEN_PORT_I0H                PORTI
#define _GEN_LEVEL_I0H               true
#define _GEN_BITNUM_I0L                              0
#define _GEN_BITMASK_I0L                       (1 << 0)
#define _GEN_DRIVER_I0L_OUT         (DDRI  |=  (1 << 0))
#define _GEN_DRIVER_I0L_IN          (DDRI  &=~ (1 << 0))
#define _GEN_DRIVER_I0L_PULLUP      (PORTI |=  (1 << 0))
#define _GEN_DRIVER_I0L_HIZ         (PORTI &=~ (1 << 0))
#define _GEN_ON_I0L                 (PORTI &=~ (1 << 0))
#define _GEN_OFF_I0L                (PORTI |=  (1 << 0))
#define _GEN_ACTIVE_I0L            !(PINI  &   (1 << 0))
#define _GEN_LATCH_I0L             !(PORTI &   (1 << 0))
#define _GEN_TOGGLE_I0L             (PINI   =  (1 << 0))
#define _GEN_DDR_I0L                 DDRI
#define _GEN_PIN_I0L                 PINI
#define _GEN_PORT_I0L                PORTI
#define _GEN_LEVEL_I0L               false

#define _GEN_BITNUM_I1H                              1
#define _GEN_BITMASK_I1H                       (1 << 1)
#define _GEN_DRIVER_I1H_OUT         (DDRI  |=  (1 << 1))
#define _GEN_DRIVER_I1H_IN          (DDRI  &=~ (1 << 1))
#define _GEN_DRIVER_I1H_PULLUP      (PORTI |=  (1 << 1))
#define _GEN_DRIVER_I1H_HIZ         (PORTI &=~ (1 << 1))
#define _GEN_ON_I1H                 (PORTI |=  (1 << 1))
#define _GEN_OFF_I1H                (PORTI &=~ (1 << 1))
#define _GEN_ACTIVE_I1H             (PINI  &   (1 << 1))
#define _GEN_LATCH_I1H              (PORTI &   (1 << 1))
#define _GEN_TOGGLE_I1H             (PINI   =  (1 << 1))
#define _GEN_DDR_I1H                 DDRI
#define _GEN_PIN_I1H                 PINI
#define _GEN_PORT_I1H                PORTI
#define _GEN_LEVEL_I1H               true
#define _GEN_BITNUM_I1L                              1
#define _GEN_BITMASK_I1L                       (1 << 1)
#define _GEN_DRIVER_I1L_OUT         (DDRI  |=  (1 << 1))
#define _GEN_DRIVER_I1L_IN          (DDRI  &=~ (1 << 1))
#define _GEN_DRIVER_I1L_PULLUP      (PORTI |=  (1 << 1))
#define _GEN_DRIVER_I1L_HIZ         (PORTI &=~ (1 << 1))
#define _GEN_ON_I1L                 (PORTI &=~ (1 << 1))
#define _GEN_OFF_I1L                (PORTI |=  (1 << 1))
#define _GEN_ACTIVE_I1L            !(PINI  &   (1 << 1))
#define _GEN_LATCH_I1L             !(PORTI &   (1 << 1))
#define _GEN_TOGGLE_I1L             (PINI   =  (1 << 1))
#define _GEN_DDR_I1L                 DDRI
#define _GEN_PIN_I1L                 PINI
#define _GEN_PORT_I1L                PORTI
#define _GEN_LEVEL_I1L               false

#define _GEN_BITNUM_I2H                              2
#define _GEN_BITMASK_I2H                       (1 << 2)
#define _GEN_DRIVER_I2H_OUT         (DDRI  |=  (1 << 2))
#define _GEN_DRIVER_I2H_IN          (DDRI  &=~ (1 << 2))
#define _GEN_DRIVER_I2H_PULLUP      (PORTI |=  (1 << 2))
#define _GEN_DRIVER_I2H_HIZ         (PORTI &=~ (1 << 2))
#define _GEN_ON_I2H                 (PORTI |=  (1 << 2))
#define _GEN_OFF_I2H                (PORTI &=~ (1 << 2))
#define _GEN_ACTIVE_I2H             (PINI  &   (1 << 2))
#define _GEN_LATCH_I2H              (PORTI &   (1 << 2))
#define _GEN_TOGGLE_I2H             (PINI   =  (1 << 2))
#define _GEN_DDR_I2H                 DDRI
#define _GEN_PIN_I2H                 PINI
#define _GEN_PORT_I2H                PORTI
#define _GEN_LEVEL_I2H               true
#define _GEN_BITNUM_I2L                              2
#define _GEN_BITMASK_I2L                       (1 << 2)
#define _GEN_DRIVER_I2L_OUT         (DDRI  |=  (1 << 2))
#define _GEN_DRIVER_I2L_IN          (DDRI  &=~ (1 << 2))
#define _GEN_DRIVER_I2L_PULLUP      (PORTI |=  (1 << 2))
#define _GEN_DRIVER_I2L_HIZ         (PORTI &=~ (1 << 2))
#define _GEN_ON_I2L                 (PORTI &=~ (1 << 2))
#define _GEN_OFF_I2L                (PORTI |=  (1 << 2))
#define _GEN_ACTIVE_I2L            !(PINI  &   (1 << 2))
#define _GEN_LATCH_I2L             !(PORTI &   (1 << 2))
#define _GEN_TOGGLE_I2L             (PINI   =  (1 << 2))
#define _GEN_DDR_I2L                 DDRI
#define _GEN_PIN_I2L                 PINI
#define _GEN_PORT_I2L                PORTI
#define _GEN_LEVEL_I2L               false

#define _GEN_BITNUM_I3H                              3
#define _GEN_BITMASK_I3H                       (1 << 3)
#define _GEN_DRIVER_I3H_OUT         (DDRI  |=  (1 << 3))
#define _GEN_DRIVER_I3H_IN          (DDRI  &=~ (1 << 3))
#define _GEN_DRIVER_I3H_PULLUP      (PORTI |=  (1 << 3))
#define _GEN_DRIVER_I3H_HIZ         (PORTI &=~ (1 << 3))
#define _GEN_ON_I3H                 (PORTI |=  (1 << 3))
#define _GEN_OFF_I3H                (PORTI &=~ (1 << 3))
#define _GEN_ACTIVE_I3H             (PINI  &   (1 << 3))
#define _GEN_LATCH_I3H              (PORTI &   (1 << 3))
#define _GEN_TOGGLE_I3H             (PINI   =  (1 << 3))
#define _GEN_DDR_I3H                 DDRI
#define _GEN_PIN_I3H                 PINI
#define _GEN_PORT_I3H                PORTI
#define _GEN_LEVEL_I3H               true
#define _GEN_BITNUM_I3L                              3
#define _GEN_BITMASK_I3L                       (1 << 3)
#define _GEN_DRIVER_I3L_OUT         (DDRI  |=  (1 << 3))
#define _GEN_DRIVER_I3L_IN          (DDRI  &=~ (1 << 3))
#define _GEN_DRIVER_I3L_PULLUP      (PORTI |=  (1 << 3))
#define _GEN_DRIVER_I3L_HIZ         (PORTI &=~ (1 << 3))
#define _GEN_ON_I3L                 (PORTI &=~ (1 << 3))
#define _GEN_OFF_I3L                (PORTI |=  (1 << 3))
#define _GEN_ACTIVE_I3L            !(PINI  &   (1 << 3))
#define _GEN_LATCH_I3L             !(PORTI &   (1 << 3))
#define _GEN_TOGGLE_I3L             (PINI   =  (1 << 3))
#define _GEN_DDR_I3L                 DDRI
#define _GEN_PIN_I3L                 PINI
#define _GEN_PORT_I3L                PORTI
#define _GEN_LEVEL_I3L               false

#define _GEN_BITNUM_I4H                              4
#define _GEN_BITMASK_I4H                       (1 << 4)
#define _GEN_DRIVER_I4H_OUT         (DDRI  |=  (1 << 4))
#define _GEN_DRIVER_I4H_IN          (DDRI  &=~ (1 << 4))
#define _GEN_DRIVER_I4H_PULLUP      (PORTI |=  (1 << 4))
#define _GEN_DRIVER_I4H_HIZ         (PORTI &=~ (1 << 4))
#define _GEN_ON_I4H                 (PORTI |=  (1 << 4))
#define _GEN_OFF_I4H                (PORTI &=~ (1 << 4))
#define _GEN_ACTIVE_I4H             (PINI  &   (1 << 4))
#define _GEN_LATCH_I4H              (PORTI &   (1 << 4))
#define _GEN_TOGGLE_I4H             (PINI   =  (1 << 4))
#define _GEN_DDR_I4H                 DDRI
#define _GEN_PIN_I4H                 PINI
#define _GEN_PORT_I4H                PORTI
#define _GEN_LEVEL_I4H               true
#define _GEN_BITNUM_I4L                              4
#define _GEN_BITMASK_I4L                       (1 << 4)
#define _GEN_DRIVER_I4L_OUT         (DDRI  |=  (1 << 4))
#define _GEN_DRIVER_I4L_IN          (DDRI  &=~ (1 << 4))
#define _GEN_DRIVER_I4L_PULLUP      (PORTI |=  (1 << 4))
#define _GEN_DRIVER_I4L_HIZ         (PORTI &=~ (1 << 4))
#define _GEN_ON_I4L                 (PORTI &=~ (1 << 4))
#define _GEN_OFF_I4L                (PORTI |=  (1 << 4))
#define _GEN_ACTIVE_I4L            !(PINI  &   (1 << 4))
#define _GEN_LATCH_I4L             !(PORTI &   (1 << 4))
#define _GEN_TOGGLE_I4L             (PINI   =  (1 << 4))
#define _GEN_DDR_I4L                 DDRI
#define _GEN_PIN_I4L                 PINI
#define _GEN_PORT_I4L                PORTI
#define _GEN_LEVEL_I4L               false

#define _GEN_BITNUM_I5H                              5
#define _GEN_BITMASK_I5H                       (1 << 5)
#define _GEN_DRIVER_I5H_OUT         (DDRI  |=  (1 << 5))
#define _GEN_DRIVER_I5H_IN          (DDRI  &=~ (1 << 5))
#define _GEN_DRIVER_I5H_PULLUP      (PORTI |=  (1 << 5))
#define _GEN_DRIVER_I5H_HIZ         (PORTI &=~ (1 << 5))
#define _GEN_ON_I5H                 (PORTI |=  (1 << 5))
#define _GEN_OFF_I5H                (PORTI &=~ (1 << 5))
#define _GEN_ACTIVE_I5H             (PINI  &   (1 << 5))
#define _GEN_LATCH_I5H              (PORTI &   (1 << 5))
#define _GEN_TOGGLE_I5H             (PINI   =  (1 << 5))
#define _GEN_DDR_I5H                 DDRI
#define _GEN_PIN_I5H                 PINI
#define _GEN_PORT_I5H                PORTI
#define _GEN_LEVEL_I5H               true
#define _GEN_BITNUM_I5L                              5
#define _GEN_BITMASK_I5L                       (1 << 5)
#define _GEN_DRIVER_I5L_OUT         (DDRI  |=  (1 << 5))
#define _GEN_DRIVER_I5L_IN          (DDRI  &=~ (1 << 5))
#define _GEN_DRIVER_I5L_PULLUP      (PORTI |=  (1 << 5))
#define _GEN_DRIVER_I5L_HIZ         (PORTI &=~ (1 << 5))
#define _GEN_ON_I5L                 (PORTI &=~ (1 << 5))
#define _GEN_OFF_I5L                (PORTI |=  (1 << 5))
#define _GEN_ACTIVE_I5L            !(PINI  &   (1 << 5))
#define _GEN_LATCH_I5L             !(PORTI &   (1 << 5))
#define _GEN_TOGGLE_I5L             (PINI   =  (1 << 5))
#define _GEN_DDR_I5L                 DDRI
#define _GEN_PIN_I5L                 PINI
#define _GEN_PORT_I5L                PORTI
#define _GEN_LEVEL_I5L               false

#define _GEN_BITNUM_I6H                              6
#define _GEN_BITMASK_I6H                       (1 << 6)
#define _GEN_DRIVER_I6H_OUT         (DDRI  |=  (1 << 6))
#define _GEN_DRIVER_I6H_IN          (DDRI  &=~ (1 << 6))
#define _GEN_DRIVER_I6H_PULLUP      (PORTI |=  (1 << 6))
#define _GEN_DRIVER_I6H_HIZ         (PORTI &=~ (1 << 6))
#define _GEN_ON_I6H                 (PORTI |=  (1 << 6))
#define _GEN_OFF_I6H                (PORTI &=~ (1 << 6))
#define _GEN_ACTIVE_I6H             (PINI  &   (1 << 6))
#define _GEN_LATCH_I6H              (PORTI &   (1 << 6))
#define _GEN_TOGGLE_I6H             (PINI   =  (1 << 6))
#define _GEN_DDR_I6H                 DDRI
#define _GEN_PIN_I6H                 PINI
#define _GEN_PORT_I6H                PORTI
#define _GEN_LEVEL_I6H               true
#define _GEN_BITNUM_I6L                              6
#define _GEN_BITMASK_I6L                       (1 << 6)
#define _GEN_DRIVER_I6L_OUT         (DDRI  |=  (1 << 6))
#define _GEN_DRIVER_I6L_IN          (DDRI  &=~ (1 << 6))
#define _GEN_DRIVER_I6L_PULLUP      (PORTI |=  (1 << 6))
#define _GEN_DRIVER_I6L_HIZ         (PORTI &=~ (1 << 6))
#define _GEN_ON_I6L                 (PORTI &=~ (1 << 6))
#define _GEN_OFF_I6L                (PORTI |=  (1 << 6))
#define _GEN_ACTIVE_I6L            !(PINI  &   (1 << 6))
#define _GEN_LATCH_I6L             !(PORTI &   (1 << 6))
#define _GEN_TOGGLE_I6L             (PINI   =  (1 << 6))
#define _GEN_DDR_I6L                 DDRI
#define _GEN_PIN_I6L                 PINI
#define _GEN_PORT_I6L                PORTI
#define _GEN_LEVEL_I6L               false

#define _GEN_BITNUM_I7H                              7
#define _GEN_BITMASK_I7H                       (1 << 7)
#define _GEN_DRIVER_I7H_OUT         (DDRI  |=  (1 << 7))
#define _GEN_DRIVER_I7H_IN          (DDRI  &=~ (1 << 7))
#define _GEN_DRIVER_I7H_PULLUP      (PORTI |=  (1 << 7))
#define _GEN_DRIVER_I7H_HIZ         (PORTI &=~ (1 << 7))
#define _GEN_ON_I7H                 (PORTI |=  (1 << 7))
#define _GEN_OFF_I7H                (PORTI &=~ (1 << 7))
#define _GEN_ACTIVE_I7H             (PINI  &   (1 << 7))
#define _GEN_LATCH_I7H              (PORTI &   (1 << 7))
#define _GEN_TOGGLE_I7H             (PINI   =  (1 << 7))
#define _GEN_DDR_I7H                 DDRI
#define _GEN_PIN_I7H                 PINI
#define _GEN_PORT_I7H                PORTI
#define _GEN_LEVEL_I7H               true
#define _GEN_BITNUM_I7L                              7
#define _GEN_BITMASK_I7L                       (1 << 7)
#define _GEN_DRIVER_I7L_OUT         (DDRI  |=  (1 << 7))
#define _GEN_DRIVER_I7L_IN          (DDRI  &=~ (1 << 7))
#define _GEN_DRIVER_I7L_PULLUP      (PORTI |=  (1 << 7))
#define _GEN_DRIVER_I7L_HIZ         (PORTI &=~ (1 << 7))
#define _GEN_ON_I7L                 (PORTI &=~ (1 << 7))
#define _GEN_OFF_I7L                (PORTI |=  (1 << 7))
#define _GEN_ACTIVE_I7L            !(PINI  &   (1 << 7))
#define _GEN_LATCH_I7L             !(PORTI &   (1 << 7))
#define _GEN_TOGGLE_I7L             (PINI   =  (1 << 7))
#define _GEN_DDR_I7L                 DDRI
#define _GEN_PIN_I7L                 PINI
#define _GEN_PORT_I7L                PORTI
#define _GEN_LEVEL_I7L               false

#define _GEN_BITNUM_J0H                              0
#define _GEN_BITMASK_J0H                       (1 << 0)
#define _GEN_DRIVER_J0H_OUT         (DDRJ  |=  (1 << 0))
#define _GEN_DRIVER_J0H_IN          (DDRJ  &=~ (1 << 0))
#define _GEN_DRIVER_J0H_PULLUP      (PORTJ |=  (1 << 0))
#define _GEN_DRIVER_J0H_HIZ         (PORTJ &=~ (1 << 0))
#define _GEN_ON_J0H                 (PORTJ |=  (1 << 0))
#define _GEN_OFF_J0H                (PORTJ &=~ (1 << 0))
#define _GEN_ACTIVE_J0H             (PINJ  &   (1 << 0))
#define _GEN_LATCH_J0H              (PORTJ &   (1 << 0))
#define _GEN_TOGGLE_J0H             (PINJ   =  (1 << 0))
#define _GEN_DDR_J0H                 DDRJ
#define _GEN_PIN_J0H                 PINJ
#define _GEN_PORT_J0H                PORTJ
#define _GEN_LEVEL_J0H               true
#define _GEN_BITNUM_J0L                              0
#define _GEN_BITMASK_J0L                       (1 << 0)
#define _GEN_DRIVER_J0L_OUT         (DDRJ  |=  (1 << 0))
#define _GEN_DRIVER_J0L_IN          (DDRJ  &=~ (1 << 0))
#define _GEN_DRIVER_J0L_PULLUP      (PORTJ |=  (1 << 0))
#define _GEN_DRIVER_J0L_HIZ         (PORTJ &=~ (1 << 0))
#define _GEN_ON_J0L                 (PORTJ &=~ (1 << 0))
#define _GEN_OFF_J0L                (PORTJ |=  (1 << 0))
#define _GEN_ACTIVE_J0L            !(PINJ  &   (1 << 0))
#define _GEN_LATCH_J0L             !(PORTJ &   (1 << 0))
#define _GEN_TOGGLE_J0L             (PINJ   =  (1 << 0))
#define _GEN_DDR_J0L                 DDRJ
#define _GEN_PIN_J0L                 PINJ
#define _GEN_PORT_J0L                PORTJ
#define _GEN_LEVEL_J0L               false

#define _GEN_BITNUM_J1H                              1
#define _GEN_BITMASK_J1H                       (1 << 1)
#define _GEN_DRIVER_J1H_OUT         (DDRJ  |=  (1 << 1))
#define _GEN_DRIVER_J1H_IN          (DDRJ  &=~ (1 << 1))
#define _GEN_DRIVER_J1H_PULLUP      (PORTJ |=  (1 << 1))
#define _GEN_DRIVER_J1H_HIZ         (PORTJ &=~ (1 << 1))
#define _GEN_ON_J1H                 (PORTJ |=  (1 << 1))
#define _GEN_OFF_J1H                (PORTJ &=~ (1 << 1))
#define _GEN_ACTIVE_J1H             (PINJ  &   (1 << 1))
#define _GEN_LATCH_J1H              (PORTJ &   (1 << 1))
#define _GEN_TOGGLE_J1H             (PINJ   =  (1 << 1))
#define _GEN_DDR_J1H                 DDRJ
#define _GEN_PIN_J1H                 PINJ
#define _GEN_PORT_J1H                PORTJ
#define _GEN_LEVEL_J1H               true
#define _GEN_BITNUM_J1L                              1
#define _GEN_BITMASK_J1L                       (1 << 1)
#define _GEN_DRIVER_J1L_OUT         (DDRJ  |=  (1 << 1))
#define _GEN_DRIVER_J1L_IN          (DDRJ  &=~ (1 << 1))
#define _GEN_DRIVER_J1L_PULLUP      (PORTJ |=  (1 << 1))
#define _GEN_DRIVER_J1L_HIZ         (PORTJ &=~ (1 << 1))
#define _GEN_ON_J1L                 (PORTJ &=~ (1 << 1))
#define _GEN_OFF_J1L                (PORTJ |=  (1 << 1))
#define _GEN_ACTIVE_J1L            !(PINJ  &   (1 << 1))
#define _GEN_LATCH_J1L             !(PORTJ &   (1 << 1))
#define _GEN_TOGGLE_J1L             (PINJ   =  (1 << 1))
#define _GEN_DDR_J1L                 DDRJ
#define _GEN_PIN_J1L                 PINJ
#define _GEN_PORT_J1L                PORTJ
#define _GEN_LEVEL_J1L               false

#define _GEN_BITNUM_J2H                              2
#define _GEN_BITMASK_J2H                       (1 << 2)
#define _GEN_DRIVER_J2H_OUT         (DDRJ  |=  (1 << 2))
#define _GEN_DRIVER_J2H_IN          (DDRJ  &=~ (1 << 2))
#define _GEN_DRIVER_J2H_PULLUP      (PORTJ |=  (1 << 2))
#define _GEN_DRIVER_J2H_HIZ         (PORTJ &=~ (1 << 2))
#define _GEN_ON_J2H                 (PORTJ |=  (1 << 2))
#define _GEN_OFF_J2H                (PORTJ &=~ (1 << 2))
#define _GEN_ACTIVE_J2H             (PINJ  &   (1 << 2))
#define _GEN_LATCH_J2H              (PORTJ &   (1 << 2))
#define _GEN_TOGGLE_J2H             (PINJ   =  (1 << 2))
#define _GEN_DDR_J2H                 DDRJ
#define _GEN_PIN_J2H                 PINJ
#define _GEN_PORT_J2H                PORTJ
#define _GEN_LEVEL_J2H               true
#define _GEN_BITNUM_J2L                              2
#define _GEN_BITMASK_J2L                       (1 << 2)
#define _GEN_DRIVER_J2L_OUT         (DDRJ  |=  (1 << 2))
#define _GEN_DRIVER_J2L_IN          (DDRJ  &=~ (1 << 2))
#define _GEN_DRIVER_J2L_PULLUP      (PORTJ |=  (1 << 2))
#define _GEN_DRIVER_J2L_HIZ         (PORTJ &=~ (1 << 2))
#define _GEN_ON_J2L                 (PORTJ &=~ (1 << 2))
#define _GEN_OFF_J2L                (PORTJ |=  (1 << 2))
#define _GEN_ACTIVE_J2L            !(PINJ  &   (1 << 2))
#define _GEN_LATCH_J2L             !(PORTJ &   (1 << 2))
#define _GEN_TOGGLE_J2L             (PINJ   =  (1 << 2))
#define _GEN_DDR_J2L                 DDRJ
#define _GEN_PIN_J2L                 PINJ
#define _GEN_PORT_J2L                PORTJ
#define _GEN_LEVEL_J2L               false

#define _GEN_BITNUM_J3H                              3
#define _GEN_BITMASK_J3H                       (1 << 3)
#define _GEN_DRIVER_J3H_OUT         (DDRJ  |=  (1 << 3))
#define _GEN_DRIVER_J3H_IN          (DDRJ  &=~ (1 << 3))
#define _GEN_DRIVER_J3H_PULLUP      (PORTJ |=  (1 << 3))
#define _GEN_DRIVER_J3H_HIZ         (PORTJ &=~ (1 << 3))
#define _GEN_ON_J3H                 (PORTJ |=  (1 << 3))
#define _GEN_OFF_J3H                (PORTJ &=~ (1 << 3))
#define _GEN_ACTIVE_J3H             (PINJ  &   (1 << 3))
#define _GEN_LATCH_J3H              (PORTJ &   (1 << 3))
#define _GEN_TOGGLE_J3H             (PINJ   =  (1 << 3))
#define _GEN_DDR_J3H                 DDRJ
#define _GEN_PIN_J3H                 PINJ
#define _GEN_PORT_J3H                PORTJ
#define _GEN_LEVEL_J3H               true
#define _GEN_BITNUM_J3L                              3
#define _GEN_BITMASK_J3L                       (1 << 3)
#define _GEN_DRIVER_J3L_OUT         (DDRJ  |=  (1 << 3))
#define _GEN_DRIVER_J3L_IN          (DDRJ  &=~ (1 << 3))
#define _GEN_DRIVER_J3L_PULLUP      (PORTJ |=  (1 << 3))
#define _GEN_DRIVER_J3L_HIZ         (PORTJ &=~ (1 << 3))
#define _GEN_ON_J3L                 (PORTJ &=~ (1 << 3))
#define _GEN_OFF_J3L                (PORTJ |=  (1 << 3))
#define _GEN_ACTIVE_J3L            !(PINJ  &   (1 << 3))
#define _GEN_LATCH_J3L             !(PORTJ &   (1 << 3))
#define _GEN_TOGGLE_J3L             (PINJ   =  (1 << 3))
#define _GEN_DDR_J3L                 DDRJ
#define _GEN_PIN_J3L                 PINJ
#define _GEN_PORT_J3L                PORTJ
#define _GEN_LEVEL_J3L               false

#define _GEN_BITNUM_J4H                              4
#define _GEN_BITMASK_J4H                       (1 << 4)
#define _GEN_DRIVER_J4H_OUT         (DDRJ  |=  (1 << 4))
#define _GEN_DRIVER_J4H_IN          (DDRJ  &=~ (1 << 4))
#define _GEN_DRIVER_J4H_PULLUP      (PORTJ |=  (1 << 4))
#define _GEN_DRIVER_J4H_HIZ         (PORTJ &=~ (1 << 4))
#define _GEN_ON_J4H                 (PORTJ |=  (1 << 4))
#define _GEN_OFF_J4H                (PORTJ &=~ (1 << 4))
#define _GEN_ACTIVE_J4H             (PINJ  &   (1 << 4))
#define _GEN_LATCH_J4H              (PORTJ &   (1 << 4))
#define _GEN_TOGGLE_J4H             (PINJ   =  (1 << 4))
#define _GEN_DDR_J4H                 DDRJ
#define _GEN_PIN_J4H                 PINJ
#define _GEN_PORT_J4H                PORTJ
#define _GEN_LEVEL_J4H               true
#define _GEN_BITNUM_J4L                              4
#define _GEN_BITMASK_J4L                       (1 << 4)
#define _GEN_DRIVER_J4L_OUT         (DDRJ  |=  (1 << 4))
#define _GEN_DRIVER_J4L_IN          (DDRJ  &=~ (1 << 4))
#define _GEN_DRIVER_J4L_PULLUP      (PORTJ |=  (1 << 4))
#define _GEN_DRIVER_J4L_HIZ         (PORTJ &=~ (1 << 4))
#define _GEN_ON_J4L                 (PORTJ &=~ (1 << 4))
#define _GEN_OFF_J4L                (PORTJ |=  (1 << 4))
#define _GEN_ACTIVE_J4L            !(PINJ  &   (1 << 4))
#define _GEN_LATCH_J4L             !(PORTJ &   (1 << 4))
#define _GEN_TOGGLE_J4L             (PINJ   =  (1 << 4))
#define _GEN_DDR_J4L                 DDRJ
#define _GEN_PIN_J4L                 PINJ
#define _GEN_PORT_J4L                PORTJ
#define _GEN_LEVEL_J4L               false

#define _GEN_BITNUM_J5H                              5
#define _GEN_BITMASK_J5H                       (1 << 5)
#define _GEN_DRIVER_J5H_OUT         (DDRJ  |=  (1 << 5))
#define _GEN_DRIVER_J5H_IN          (DDRJ  &=~ (1 << 5))
#define _GEN_DRIVER_J5H_PULLUP      (PORTJ |=  (1 << 5))
#define _GEN_DRIVER_J5H_HIZ         (PORTJ &=~ (1 << 5))
#define _GEN_ON_J5H                 (PORTJ |=  (1 << 5))
#define _GEN_OFF_J5H                (PORTJ &=~ (1 << 5))
#define _GEN_ACTIVE_J5H             (PINJ  &   (1 << 5))
#define _GEN_LATCH_J5H              (PORTJ &   (1 << 5))
#define _GEN_TOGGLE_J5H             (PINJ   =  (1 << 5))
#define _GEN_DDR_J5H                 DDRJ
#define _GEN_PIN_J5H                 PINJ
#define _GEN_PORT_J5H                PORTJ
#define _GEN_LEVEL_J5H               true
#define _GEN_BITNUM_J5L                              5
#define _GEN_BITMASK_J5L                       (1 << 5)
#define _GEN_DRIVER_J5L_OUT         (DDRJ  |=  (1 << 5))
#define _GEN_DRIVER_J5L_IN          (DDRJ  &=~ (1 << 5))
#define _GEN_DRIVER_J5L_PULLUP      (PORTJ |=  (1 << 5))
#define _GEN_DRIVER_J5L_HIZ         (PORTJ &=~ (1 << 5))
#define _GEN_ON_J5L                 (PORTJ &=~ (1 << 5))
#define _GEN_OFF_J5L                (PORTJ |=  (1 << 5))
#define _GEN_ACTIVE_J5L            !(PINJ  &   (1 << 5))
#define _GEN_LATCH_J5L             !(PORTJ &   (1 << 5))
#define _GEN_TOGGLE_J5L             (PINJ   =  (1 << 5))
#define _GEN_DDR_J5L                 DDRJ
#define _GEN_PIN_J5L                 PINJ
#define _GEN_PORT_J5L                PORTJ
#define _GEN_LEVEL_J5L               false

#define _GEN_BITNUM_J6H                              6
#define _GEN_BITMASK_J6H                       (1 << 6)
#define _GEN_DRIVER_J6H_OUT         (DDRJ  |=  (1 << 6))
#define _GEN_DRIVER_J6H_IN          (DDRJ  &=~ (1 << 6))
#define _GEN_DRIVER_J6H_PULLUP      (PORTJ |=  (1 << 6))
#define _GEN_DRIVER_J6H_HIZ         (PORTJ &=~ (1 << 6))
#define _GEN_ON_J6H                 (PORTJ |=  (1 << 6))
#define _GEN_OFF_J6H                (PORTJ &=~ (1 << 6))
#define _GEN_ACTIVE_J6H             (PINJ  &   (1 << 6))
#define _GEN_LATCH_J6H              (PORTJ &   (1 << 6))
#define _GEN_TOGGLE_J6H             (PINJ   =  (1 << 6))
#define _GEN_DDR_J6H                 DDRJ
#define _GEN_PIN_J6H                 PINJ
#define _GEN_PORT_J6H                PORTJ
#define _GEN_LEVEL_J6H               true
#define _GEN_BITNUM_J6L                              6
#define _GEN_BITMASK_J6L                       (1 << 6)
#define _GEN_DRIVER_J6L_OUT         (DDRJ  |=  (1 << 6))
#define _GEN_DRIVER_J6L_IN          (DDRJ  &=~ (1 << 6))
#define _GEN_DRIVER_J6L_PULLUP      (PORTJ |=  (1 << 6))
#define _GEN_DRIVER_J6L_HIZ         (PORTJ &=~ (1 << 6))
#define _GEN_ON_J6L                 (PORTJ &=~ (1 << 6))
#define _GEN_OFF_J6L                (PORTJ |=  (1 << 6))
#define _GEN_ACTIVE_J6L            !(PINJ  &   (1 << 6))
#define _GEN_LATCH_J6L             !(PORTJ &   (1 << 6))
#define _GEN_TOGGLE_J6L             (PINJ   =  (1 << 6))
#define _GEN_DDR_J6L                 DDRJ
#define _GEN_PIN_J6L                 PINJ
#define _GEN_PORT_J6L                PORTJ
#define _GEN_LEVEL_J6L               false

#define _GEN_BITNUM_J7H                              7
#define _GEN_BITMASK_J7H                       (1 << 7)
#define _GEN_DRIVER_J7H_OUT         (DDRJ  |=  (1 << 7))
#define _GEN_DRIVER_J7H_IN          (DDRJ  &=~ (1 << 7))
#define _GEN_DRIVER_J7H_PULLUP      (PORTJ |=  (1 << 7))
#define _GEN_DRIVER_J7H_HIZ         (PORTJ &=~ (1 << 7))
#define _GEN_ON_J7H                 (PORTJ |=  (1 << 7))
#define _GEN_OFF_J7H                (PORTJ &=~ (1 << 7))
#define _GEN_ACTIVE_J7H             (PINJ  &   (1 << 7))
#define _GEN_LATCH_J7H              (PORTJ &   (1 << 7))
#define _GEN_TOGGLE_J7H             (PINJ   =  (1 << 7))
#define _GEN_DDR_J7H                 DDRJ
#define _GEN_PIN_J7H                 PINJ
#define _GEN_PORT_J7H                PORTJ
#define _GEN_LEVEL_J7H               true
#define _GEN_BITNUM_J7L                              7
#define _GEN_BITMASK_J7L                       (1 << 7)
#define _GEN_DRIVER_J7L_OUT         (DDRJ  |=  (1 << 7))
#define _GEN_DRIVER_J7L_IN          (DDRJ  &=~ (1 << 7))
#define _GEN_DRIVER_J7L_PULLUP      (PORTJ |=  (1 << 7))
#define _GEN_DRIVER_J7L_HIZ         (PORTJ &=~ (1 << 7))
#define _GEN_ON_J7L                 (PORTJ &=~ (1 << 7))
#define _GEN_OFF_J7L                (PORTJ |=  (1 << 7))
#define _GEN_ACTIVE_J7L            !(PINJ  &   (1 << 7))
#define _GEN_LATCH_J7L             !(PORTJ &   (1 << 7))
#define _GEN_TOGGLE_J7L             (PINJ   =  (1 << 7))
#define _GEN_DDR_J7L                 DDRJ
#define _GEN_PIN_J7L                 PINJ
#define _GEN_PORT_J7L                PORTJ
#define _GEN_LEVEL_J7L               false

#define _GEN_BITNUM_K0H                              0
#define _GEN_BITMASK_K0H                       (1 << 0)
#define _GEN_DRIVER_K0H_OUT         (DDRK  |=  (1 << 0))
#define _GEN_DRIVER_K0H_IN          (DDRK  &=~ (1 << 0))
#define _GEN_DRIVER_K0H_PULLUP      (PORTK |=  (1 << 0))
#define _GEN_DRIVER_K0H_HIZ         (PORTK &=~ (1 << 0))
#define _GEN_ON_K0H                 (PORTK |=  (1 << 0))
#define _GEN_OFF_K0H                (PORTK &=~ (1 << 0))
#define _GEN_ACTIVE_K0H             (PINK  &   (1 << 0))
#define _GEN_LATCH_K0H              (PORTK &   (1 << 0))
#define _GEN_TOGGLE_K0H             (PINK   =  (1 << 0))
#define _GEN_DDR_K0H                 DDRK
#define _GEN_PIN_K0H                 PINK
#define _GEN_PORT_K0H                PORTK
#define _GEN_LEVEL_K0H               true
#define _GEN_BITNUM_K0L                              0
#define _GEN_BITMASK_K0L                       (1 << 0)
#define _GEN_DRIVER_K0L_OUT         (DDRK  |=  (1 << 0))
#define _GEN_DRIVER_K0L_IN          (DDRK  &=~ (1 << 0))
#define _GEN_DRIVER_K0L_PULLUP      (PORTK |=  (1 << 0))
#define _GEN_DRIVER_K0L_HIZ         (PORTK &=~ (1 << 0))
#define _GEN_ON_K0L                 (PORTK &=~ (1 << 0))
#define _GEN_OFF_K0L                (PORTK |=  (1 << 0))
#define _GEN_ACTIVE_K0L            !(PINK  &   (1 << 0))
#define _GEN_LATCH_K0L             !(PORTK &   (1 << 0))
#define _GEN_TOGGLE_K0L             (PINK   =  (1 << 0))
#define _GEN_DDR_K0L                 DDRK
#define _GEN_PIN_K0L                 PINK
#define _GEN_PORT_K0L                PORTK
#define _GEN_LEVEL_K0L               false

#define _GEN_BITNUM_K1H                              1
#define _GEN_BITMASK_K1H                       (1 << 1)
#define _GEN_DRIVER_K1H_OUT         (DDRK  |=  (1 << 1))
#define _GEN_DRIVER_K1H_IN          (DDRK  &=~ (1 << 1))
#define _GEN_DRIVER_K1H_PULLUP      (PORTK |=  (1 << 1))
#define _GEN_DRIVER_K1H_HIZ         (PORTK &=~ (1 << 1))
#define _GEN_ON_K1H                 (PORTK |=  (1 << 1))
#define _GEN_OFF_K1H                (PORTK &=~ (1 << 1))
#define _GEN_ACTIVE_K1H             (PINK  &   (1 << 1))
#define _GEN_LATCH_K1H              (PORTK &   (1 << 1))
#define _GEN_TOGGLE_K1H             (PINK   =  (1 << 1))
#define _GEN_DDR_K1H                 DDRK
#define _GEN_PIN_K1H                 PINK
#define _GEN_PORT_K1H                PORTK
#define _GEN_LEVEL_K1H               true
#define _GEN_BITNUM_K1L                              1
#define _GEN_BITMASK_K1L                       (1 << 1)
#define _GEN_DRIVER_K1L_OUT         (DDRK  |=  (1 << 1))
#define _GEN_DRIVER_K1L_IN          (DDRK  &=~ (1 << 1))
#define _GEN_DRIVER_K1L_PULLUP      (PORTK |=  (1 << 1))
#define _GEN_DRIVER_K1L_HIZ         (PORTK &=~ (1 << 1))
#define _GEN_ON_K1L                 (PORTK &=~ (1 << 1))
#define _GEN_OFF_K1L                (PORTK |=  (1 << 1))
#define _GEN_ACTIVE_K1L            !(PINK  &   (1 << 1))
#define _GEN_LATCH_K1L             !(PORTK &   (1 << 1))
#define _GEN_TOGGLE_K1L             (PINK   =  (1 << 1))
#define _GEN_DDR_K1L                 DDRK
#define _GEN_PIN_K1L                 PINK
#define _GEN_PORT_K1L                PORTK
#define _GEN_LEVEL_K1L               false

#define _GEN_BITNUM_K2H                              2
#define _GEN_BITMASK_K2H                       (1 << 2)
#define _GEN_DRIVER_K2H_OUT         (DDRK  |=  (1 << 2))
#define _GEN_DRIVER_K2H_IN          (DDRK  &=~ (1 << 2))
#define _GEN_DRIVER_K2H_PULLUP      (PORTK |=  (1 << 2))
#define _GEN_DRIVER_K2H_HIZ         (PORTK &=~ (1 << 2))
#define _GEN_ON_K2H                 (PORTK |=  (1 << 2))
#define _GEN_OFF_K2H                (PORTK &=~ (1 << 2))
#define _GEN_ACTIVE_K2H             (PINK  &   (1 << 2))
#define _GEN_LATCH_K2H              (PORTK &   (1 << 2))
#define _GEN_TOGGLE_K2H             (PINK   =  (1 << 2))
#define _GEN_DDR_K2H                 DDRK
#define _GEN_PIN_K2H                 PINK
#define _GEN_PORT_K2H                PORTK
#define _GEN_LEVEL_K2H               true
#define _GEN_BITNUM_K2L                              2
#define _GEN_BITMASK_K2L                       (1 << 2)
#define _GEN_DRIVER_K2L_OUT         (DDRK  |=  (1 << 2))
#define _GEN_DRIVER_K2L_IN          (DDRK  &=~ (1 << 2))
#define _GEN_DRIVER_K2L_PULLUP      (PORTK |=  (1 << 2))
#define _GEN_DRIVER_K2L_HIZ         (PORTK &=~ (1 << 2))
#define _GEN_ON_K2L                 (PORTK &=~ (1 << 2))
#define _GEN_OFF_K2L                (PORTK |=  (1 << 2))
#define _GEN_ACTIVE_K2L            !(PINK  &   (1 << 2))
#define _GEN_LATCH_K2L             !(PORTK &   (1 << 2))
#define _GEN_TOGGLE_K2L             (PINK   =  (1 << 2))
#define _GEN_DDR_K2L                 DDRK
#define _GEN_PIN_K2L                 PINK
#define _GEN_PORT_K2L                PORTK
#define _GEN_LEVEL_K2L               false

#define _GEN_BITNUM_K3H                              3
#define _GEN_BITMASK_K3H                       (1 << 3)
#define _GEN_DRIVER_K3H_OUT         (DDRK  |=  (1 << 3))
#define _GEN_DRIVER_K3H_IN          (DDRK  &=~ (1 << 3))
#define _GEN_DRIVER_K3H_PULLUP      (PORTK |=  (1 << 3))
#define _GEN_DRIVER_K3H_HIZ         (PORTK &=~ (1 << 3))
#define _GEN_ON_K3H                 (PORTK |=  (1 << 3))
#define _GEN_OFF_K3H                (PORTK &=~ (1 << 3))
#define _GEN_ACTIVE_K3H             (PINK  &   (1 << 3))
#define _GEN_LATCH_K3H              (PORTK &   (1 << 3))
#define _GEN_TOGGLE_K3H             (PINK   =  (1 << 3))
#define _GEN_DDR_K3H                 DDRK
#define _GEN_PIN_K3H                 PINK
#define _GEN_PORT_K3H                PORTK
#define _GEN_LEVEL_K3H               true
#define _GEN_BITNUM_K3L                              3
#define _GEN_BITMASK_K3L                       (1 << 3)
#define _GEN_DRIVER_K3L_OUT         (DDRK  |=  (1 << 3))
#define _GEN_DRIVER_K3L_IN          (DDRK  &=~ (1 << 3))
#define _GEN_DRIVER_K3L_PULLUP      (PORTK |=  (1 << 3))
#define _GEN_DRIVER_K3L_HIZ         (PORTK &=~ (1 << 3))
#define _GEN_ON_K3L                 (PORTK &=~ (1 << 3))
#define _GEN_OFF_K3L                (PORTK |=  (1 << 3))
#define _GEN_ACTIVE_K3L            !(PINK  &   (1 << 3))
#define _GEN_LATCH_K3L             !(PORTK &   (1 << 3))
#define _GEN_TOGGLE_K3L             (PINK   =  (1 << 3))
#define _GEN_DDR_K3L                 DDRK
#define _GEN_PIN_K3L                 PINK
#define _GEN_PORT_K3L                PORTK
#define _GEN_LEVEL_K3L               false

#define _GEN_BITNUM_K4H                              4
#define _GEN_BITMASK_K4H                       (1 << 4)
#define _GEN_DRIVER_K4H_OUT         (DDRK  |=  (1 << 4))
#define _GEN_DRIVER_K4H_IN          (DDRK  &=~ (1 << 4))
#define _GEN_DRIVER_K4H_PULLUP      (PORTK |=  (1 << 4))
#define _GEN_DRIVER_K4H_HIZ         (PORTK &=~ (1 << 4))
#define _GEN_ON_K4H                 (PORTK |=  (1 << 4))
#define _GEN_OFF_K4H                (PORTK &=~ (1 << 4))
#define _GEN_ACTIVE_K4H             (PINK  &   (1 << 4))
#define _GEN_LATCH_K4H              (PORTK &   (1 << 4))
#define _GEN_TOGGLE_K4H             (PINK   =  (1 << 4))
#define _GEN_DDR_K4H                 DDRK
#define _GEN_PIN_K4H                 PINK
#define _GEN_PORT_K4H                PORTK
#define _GEN_LEVEL_K4H               true
#define _GEN_BITNUM_K4L                              4
#define _GEN_BITMASK_K4L                       (1 << 4)
#define _GEN_DRIVER_K4L_OUT         (DDRK  |=  (1 << 4))
#define _GEN_DRIVER_K4L_IN          (DDRK  &=~ (1 << 4))
#define _GEN_DRIVER_K4L_PULLUP      (PORTK |=  (1 << 4))
#define _GEN_DRIVER_K4L_HIZ         (PORTK &=~ (1 << 4))
#define _GEN_ON_K4L                 (PORTK &=~ (1 << 4))
#define _GEN_OFF_K4L                (PORTK |=  (1 << 4))
#define _GEN_ACTIVE_K4L            !(PINK  &   (1 << 4))
#define _GEN_LATCH_K4L             !(PORTK &   (1 << 4))
#define _GEN_TOGGLE_K4L             (PINK   =  (1 << 4))
#define _GEN_DDR_K4L                 DDRK
#define _GEN_PIN_K4L                 PINK
#define _GEN_PORT_K4L                PORTK
#define _GEN_LEVEL_K4L               false

#define _GEN_BITNUM_K5H                              5
#define _GEN_BITMASK_K5H                       (1 << 5)
#define _GEN_DRIVER_K5H_OUT         (DDRK  |=  (1 << 5))
#define _GEN_DRIVER_K5H_IN          (DDRK  &=~ (1 << 5))
#define _GEN_DRIVER_K5H_PULLUP      (PORTK |=  (1 << 5))
#define _GEN_DRIVER_K5H_HIZ         (PORTK &=~ (1 << 5))
#define _GEN_ON_K5H                 (PORTK |=  (1 << 5))
#define _GEN_OFF_K5H                (PORTK &=~ (1 << 5))
#define _GEN_ACTIVE_K5H             (PINK  &   (1 << 5))
#define _GEN_LATCH_K5H              (PORTK &   (1 << 5))
#define _GEN_TOGGLE_K5H             (PINK   =  (1 << 5))
#define _GEN_DDR_K5H                 DDRK
#define _GEN_PIN_K5H                 PINK
#define _GEN_PORT_K5H                PORTK
#define _GEN_LEVEL_K5H               true
#define _GEN_BITNUM_K5L                              5
#define _GEN_BITMASK_K5L                       (1 << 5)
#define _GEN_DRIVER_K5L_OUT         (DDRK  |=  (1 << 5))
#define _GEN_DRIVER_K5L_IN          (DDRK  &=~ (1 << 5))
#define _GEN_DRIVER_K5L_PULLUP      (PORTK |=  (1 << 5))
#define _GEN_DRIVER_K5L_HIZ         (PORTK &=~ (1 << 5))
#define _GEN_ON_K5L                 (PORTK &=~ (1 << 5))
#define _GEN_OFF_K5L                (PORTK |=  (1 << 5))
#define _GEN_ACTIVE_K5L            !(PINK  &   (1 << 5))
#define _GEN_LATCH_K5L             !(PORTK &   (1 << 5))
#define _GEN_TOGGLE_K5L             (PINK   =  (1 << 5))
#define _GEN_DDR_K5L                 DDRK
#define _GEN_PIN_K5L                 PINK
#define _GEN_PORT_K5L                PORTK
#define _GEN_LEVEL_K5L               false

#define _GEN_BITNUM_K6H                              6
#define _GEN_BITMASK_K6H                       (1 << 6)
#define _GEN_DRIVER_K6H_OUT         (DDRK  |=  (1 << 6))
#define _GEN_DRIVER_K6H_IN          (DDRK  &=~ (1 << 6))
#define _GEN_DRIVER_K6H_PULLUP      (PORTK |=  (1 << 6))
#define _GEN_DRIVER_K6H_HIZ         (PORTK &=~ (1 << 6))
#define _GEN_ON_K6H                 (PORTK |=  (1 << 6))
#define _GEN_OFF_K6H                (PORTK &=~ (1 << 6))
#define _GEN_ACTIVE_K6H             (PINK  &   (1 << 6))
#define _GEN_LATCH_K6H              (PORTK &   (1 << 6))
#define _GEN_TOGGLE_K6H             (PINK   =  (1 << 6))
#define _GEN_DDR_K6H                 DDRK
#define _GEN_PIN_K6H                 PINK
#define _GEN_PORT_K6H                PORTK
#define _GEN_LEVEL_K6H               true
#define _GEN_BITNUM_K6L                              6
#define _GEN_BITMASK_K6L                       (1 << 6)
#define _GEN_DRIVER_K6L_OUT         (DDRK  |=  (1 << 6))
#define _GEN_DRIVER_K6L_IN          (DDRK  &=~ (1 << 6))
#define _GEN_DRIVER_K6L_PULLUP      (PORTK |=  (1 << 6))
#define _GEN_DRIVER_K6L_HIZ         (PORTK &=~ (1 << 6))
#define _GEN_ON_K6L                 (PORTK &=~ (1 << 6))
#define _GEN_OFF_K6L                (PORTK |=  (1 << 6))
#define _GEN_ACTIVE_K6L            !(PINK  &   (1 << 6))
#define _GEN_LATCH_K6L             !(PORTK &   (1 << 6))
#define _GEN_TOGGLE_K6L             (PINK   =  (1 << 6))
#define _GEN_DDR_K6L                 DDRK
#define _GEN_PIN_K6L                 PINK
#define _GEN_PORT_K6L                PORTK
#define _GEN_LEVEL_K6L               false

#define _GEN_BITNUM_K7H                              7
#define _GEN_BITMASK_K7H                       (1 << 7)
#define _GEN_DRIVER_K7H_OUT         (DDRK  |=  (1 << 7))
#define _GEN_DRIVER_K7H_IN          (DDRK  &=~ (1 << 7))
#define _GEN_DRIVER_K7H_PULLUP      (PORTK |=  (1 << 7))
#define _GEN_DRIVER_K7H_HIZ         (PORTK &=~ (1 << 7))
#define _GEN_ON_K7H                 (PORTK |=  (1 << 7))
#define _GEN_OFF_K7H                (PORTK &=~ (1 << 7))
#define _GEN_ACTIVE_K7H             (PINK  &   (1 << 7))
#define _GEN_LATCH_K7H              (PORTK &   (1 << 7))
#define _GEN_TOGGLE_K7H             (PINK   =  (1 << 7))
#define _GEN_DDR_K7H                 DDRK
#define _GEN_PIN_K7H                 PINK
#define _GEN_PORT_K7H                PORTK
#define _GEN_LEVEL_K7H               true
#define _GEN_BITNUM_K7L                              7
#define _GEN_BITMASK_K7L                       (1 << 7)
#define _GEN_DRIVER_K7L_OUT         (DDRK  |=  (1 << 7))
#define _GEN_DRIVER_K7L_IN          (DDRK  &=~ (1 << 7))
#define _GEN_DRIVER_K7L_PULLUP      (PORTK |=  (1 << 7))
#define _GEN_DRIVER_K7L_HIZ         (PORTK &=~ (1 << 7))
#define _GEN_ON_K7L                 (PORTK &=~ (1 << 7))
#define _GEN_OFF_K7L                (PORTK |=  (1 << 7))
#define _GEN_ACTIVE_K7L            !(PINK  &   (1 << 7))
#define _GEN_LATCH_K7L             !(PORTK &   (1 << 7))
#define _GEN_TOGGLE_K7L             (PINK   =  (1 << 7))
#define _GEN_DDR_K7L                 DDRK
#define _GEN_PIN_K7L                 PINK
#define _GEN_PORT_K7L                PORTK
#define _GEN_LEVEL_K7L               false

#define _GEN_BITNUM_L0H                              0
#define _GEN_BITMASK_L0H                       (1 << 0)
#define _GEN_DRIVER_L0H_OUT         (DDRL  |=  (1 << 0))
#define _GEN_DRIVER_L0H_IN          (DDRL  &=~ (1 << 0))
#define _GEN_DRIVER_L0H_PULLUP      (PORTL |=  (1 << 0))
#define _GEN_DRIVER_L0H_HIZ         (PORTL &=~ (1 << 0))
#define _GEN_ON_L0H                 (PORTL |=  (1 << 0))
#define _GEN_OFF_L0H                (PORTL &=~ (1 << 0))
#define _GEN_ACTIVE_L0H             (PINL  &   (1 << 0))
#define _GEN_LATCH_L0H              (PORTL &   (1 << 0))
#define _GEN_TOGGLE_L0H             (PINL   =  (1 << 0))
#define _GEN_DDR_L0H                 DDRL
#define _GEN_PIN_L0H                 PINL
#define _GEN_PORT_L0H                PORTL
#define _GEN_LEVEL_L0H               true
#define _GEN_BITNUM_L0L                              0
#define _GEN_BITMASK_L0L                       (1 << 0)
#define _GEN_DRIVER_L0L_OUT         (DDRL  |=  (1 << 0))
#define _GEN_DRIVER_L0L_IN          (DDRL  &=~ (1 << 0))
#define _GEN_DRIVER_L0L_PULLUP      (PORTL |=  (1 << 0))
#define _GEN_DRIVER_L0L_HIZ         (PORTL &=~ (1 << 0))
#define _GEN_ON_L0L                 (PORTL &=~ (1 << 0))
#define _GEN_OFF_L0L                (PORTL |=  (1 << 0))
#define _GEN_ACTIVE_L0L            !(PINL  &   (1 << 0))
#define _GEN_LATCH_L0L             !(PORTL &   (1 << 0))
#define _GEN_TOGGLE_L0L             (PINL   =  (1 << 0))
#define _GEN_DDR_L0L                 DDRL
#define _GEN_PIN_L0L                 PINL
#define _GEN_PORT_L0L                PORTL
#define _GEN_LEVEL_L0L               false

#define _GEN_BITNUM_L1H                              1
#define _GEN_BITMASK_L1H                       (1 << 1)
#define _GEN_DRIVER_L1H_OUT         (DDRL  |=  (1 << 1))
#define _GEN_DRIVER_L1H_IN          (DDRL  &=~ (1 << 1))
#define _GEN_DRIVER_L1H_PULLUP      (PORTL |=  (1 << 1))
#define _GEN_DRIVER_L1H_HIZ         (PORTL &=~ (1 << 1))
#define _GEN_ON_L1H                 (PORTL |=  (1 << 1))
#define _GEN_OFF_L1H                (PORTL &=~ (1 << 1))
#define _GEN_ACTIVE_L1H             (PINL  &   (1 << 1))
#define _GEN_LATCH_L1H              (PORTL &   (1 << 1))
#define _GEN_TOGGLE_L1H             (PINL   =  (1 << 1))
#define _GEN_DDR_L1H                 DDRL
#define _GEN_PIN_L1H                 PINL
#define _GEN_PORT_L1H                PORTL
#define _GEN_LEVEL_L1H               true
#define _GEN_BITNUM_L1L                              1
#define _GEN_BITMASK_L1L                       (1 << 1)
#define _GEN_DRIVER_L1L_OUT         (DDRL  |=  (1 << 1))
#define _GEN_DRIVER_L1L_IN          (DDRL  &=~ (1 << 1))
#define _GEN_DRIVER_L1L_PULLUP      (PORTL |=  (1 << 1))
#define _GEN_DRIVER_L1L_HIZ         (PORTL &=~ (1 << 1))
#define _GEN_ON_L1L                 (PORTL &=~ (1 << 1))
#define _GEN_OFF_L1L                (PORTL |=  (1 << 1))
#define _GEN_ACTIVE_L1L            !(PINL  &   (1 << 1))
#define _GEN_LATCH_L1L             !(PORTL &   (1 << 1))
#define _GEN_TOGGLE_L1L             (PINL   =  (1 << 1))
#define _GEN_DDR_L1L                 DDRL
#define _GEN_PIN_L1L                 PINL
#define _GEN_PORT_L1L                PORTL
#define _GEN_LEVEL_L1L               false

#define _GEN_BITNUM_L2H                              2
#define _GEN_BITMASK_L2H                       (1 << 2)
#define _GEN_DRIVER_L2H_OUT         (DDRL  |=  (1 << 2))
#define _GEN_DRIVER_L2H_IN          (DDRL  &=~ (1 << 2))
#define _GEN_DRIVER_L2H_PULLUP      (PORTL |=  (1 << 2))
#define _GEN_DRIVER_L2H_HIZ         (PORTL &=~ (1 << 2))
#define _GEN_ON_L2H                 (PORTL |=  (1 << 2))
#define _GEN_OFF_L2H                (PORTL &=~ (1 << 2))
#define _GEN_ACTIVE_L2H             (PINL  &   (1 << 2))
#define _GEN_LATCH_L2H              (PORTL &   (1 << 2))
#define _GEN_TOGGLE_L2H             (PINL   =  (1 << 2))
#define _GEN_DDR_L2H                 DDRL
#define _GEN_PIN_L2H                 PINL
#define _GEN_PORT_L2H                PORTL
#define _GEN_LEVEL_L2H               true
#define _GEN_BITNUM_L2L                              2
#define _GEN_BITMASK_L2L                       (1 << 2)
#define _GEN_DRIVER_L2L_OUT         (DDRL  |=  (1 << 2))
#define _GEN_DRIVER_L2L_IN          (DDRL  &=~ (1 << 2))
#define _GEN_DRIVER_L2L_PULLUP      (PORTL |=  (1 << 2))
#define _GEN_DRIVER_L2L_HIZ         (PORTL &=~ (1 << 2))
#define _GEN_ON_L2L                 (PORTL &=~ (1 << 2))
#define _GEN_OFF_L2L                (PORTL |=  (1 << 2))
#define _GEN_ACTIVE_L2L            !(PINL  &   (1 << 2))
#define _GEN_LATCH_L2L             !(PORTL &   (1 << 2))
#define _GEN_TOGGLE_L2L             (PINL   =  (1 << 2))
#define _GEN_DDR_L2L                 DDRL
#define _GEN_PIN_L2L                 PINL
#define _GEN_PORT_L2L                PORTL
#define _GEN_LEVEL_L2L               false

#define _GEN_BITNUM_L3H                              3
#define _GEN_BITMASK_L3H                       (1 << 3)
#define _GEN_DRIVER_L3H_OUT         (DDRL  |=  (1 << 3))
#define _GEN_DRIVER_L3H_IN          (DDRL  &=~ (1 << 3))
#define _GEN_DRIVER_L3H_PULLUP      (PORTL |=  (1 << 3))
#define _GEN_DRIVER_L3H_HIZ         (PORTL &=~ (1 << 3))
#define _GEN_ON_L3H                 (PORTL |=  (1 << 3))
#define _GEN_OFF_L3H                (PORTL &=~ (1 << 3))
#define _GEN_ACTIVE_L3H             (PINL  &   (1 << 3))
#define _GEN_LATCH_L3H              (PORTL &   (1 << 3))
#define _GEN_TOGGLE_L3H             (PINL   =  (1 << 3))
#define _GEN_DDR_L3H                 DDRL
#define _GEN_PIN_L3H                 PINL
#define _GEN_PORT_L3H                PORTL
#define _GEN_LEVEL_L3H               true
#define _GEN_BITNUM_L3L                              3
#define _GEN_BITMASK_L3L                       (1 << 3)
#define _GEN_DRIVER_L3L_OUT         (DDRL  |=  (1 << 3))
#define _GEN_DRIVER_L3L_IN          (DDRL  &=~ (1 << 3))
#define _GEN_DRIVER_L3L_PULLUP      (PORTL |=  (1 << 3))
#define _GEN_DRIVER_L3L_HIZ         (PORTL &=~ (1 << 3))
#define _GEN_ON_L3L                 (PORTL &=~ (1 << 3))
#define _GEN_OFF_L3L                (PORTL |=  (1 << 3))
#define _GEN_ACTIVE_L3L            !(PINL  &   (1 << 3))
#define _GEN_LATCH_L3L             !(PORTL &   (1 << 3))
#define _GEN_TOGGLE_L3L             (PINL   =  (1 << 3))
#define _GEN_DDR_L3L                 DDRL
#define _GEN_PIN_L3L                 PINL
#define _GEN_PORT_L3L                PORTL
#define _GEN_LEVEL_L3L               false

#define _GEN_BITNUM_L4H                              4
#define _GEN_BITMASK_L4H                       (1 << 4)
#define _GEN_DRIVER_L4H_OUT         (DDRL  |=  (1 << 4))
#define _GEN_DRIVER_L4H_IN          (DDRL  &=~ (1 << 4))
#define _GEN_DRIVER_L4H_PULLUP      (PORTL |=  (1 << 4))
#define _GEN_DRIVER_L4H_HIZ         (PORTL &=~ (1 << 4))
#define _GEN_ON_L4H                 (PORTL |=  (1 << 4))
#define _GEN_OFF_L4H                (PORTL &=~ (1 << 4))
#define _GEN_ACTIVE_L4H             (PINL  &   (1 << 4))
#define _GEN_LATCH_L4H              (PORTL &   (1 << 4))
#define _GEN_TOGGLE_L4H             (PINL   =  (1 << 4))
#define _GEN_DDR_L4H                 DDRL
#define _GEN_PIN_L4H                 PINL
#define _GEN_PORT_L4H                PORTL
#define _GEN_LEVEL_L4H               true
#define _GEN_BITNUM_L4L                              4
#define _GEN_BITMASK_L4L                       (1 << 4)
#define _GEN_DRIVER_L4L_OUT         (DDRL  |=  (1 << 4))
#define _GEN_DRIVER_L4L_IN          (DDRL  &=~ (1 << 4))
#define _GEN_DRIVER_L4L_PULLUP      (PORTL |=  (1 << 4))
#define _GEN_DRIVER_L4L_HIZ         (PORTL &=~ (1 << 4))
#define _GEN_ON_L4L                 (PORTL &=~ (1 << 4))
#define _GEN_OFF_L4L                (PORTL |=  (1 << 4))
#define _GEN_ACTIVE_L4L            !(PINL  &   (1 << 4))
#define _GEN_LATCH_L4L             !(PORTL &   (1 << 4))
#define _GEN_TOGGLE_L4L             (PINL   =  (1 << 4))
#define _GEN_DDR_L4L                 DDRL
#define _GEN_PIN_L4L                 PINL
#define _GEN_PORT_L4L                PORTL
#define _GEN_LEVEL_L4L               false

#define _GEN_BITNUM_L5H                              5
#define _GEN_BITMASK_L5H                       (1 << 5)
#define _GEN_DRIVER_L5H_OUT         (DDRL  |=  (1 << 5))
#define _GEN_DRIVER_L5H_IN          (DDRL  &=~ (1 << 5))
#define _GEN_DRIVER_L5H_PULLUP      (PORTL |=  (1 << 5))
#define _GEN_DRIVER_L5H_HIZ         (PORTL &=~ (1 << 5))
#define _GEN_ON_L5H                 (PORTL |=  (1 << 5))
#define _GEN_OFF_L5H                (PORTL &=~ (1 << 5))
#define _GEN_ACTIVE_L5H             (PINL  &   (1 << 5))
#define _GEN_LATCH_L5H              (PORTL &   (1 << 5))
#define _GEN_TOGGLE_L5H             (PINL   =  (1 << 5))
#define _GEN_DDR_L5H                 DDRL
#define _GEN_PIN_L5H                 PINL
#define _GEN_PORT_L5H                PORTL
#define _GEN_LEVEL_L5H               true
#define _GEN_BITNUM_L5L                              5
#define _GEN_BITMASK_L5L                       (1 << 5)
#define _GEN_DRIVER_L5L_OUT         (DDRL  |=  (1 << 5))
#define _GEN_DRIVER_L5L_IN          (DDRL  &=~ (1 << 5))
#define _GEN_DRIVER_L5L_PULLUP      (PORTL |=  (1 << 5))
#define _GEN_DRIVER_L5L_HIZ         (PORTL &=~ (1 << 5))
#define _GEN_ON_L5L                 (PORTL &=~ (1 << 5))
#define _GEN_OFF_L5L                (PORTL |=  (1 << 5))
#define _GEN_ACTIVE_L5L            !(PINL  &   (1 << 5))
#define _GEN_LATCH_L5L             !(PORTL &   (1 << 5))
#define _GEN_TOGGLE_L5L             (PINL   =  (1 << 5))
#define _GEN_DDR_L5L                 DDRL
#define _GEN_PIN_L5L                 PINL
#define _GEN_PORT_L5L                PORTL
#define _GEN_LEVEL_L5L               false

#define _GEN_BITNUM_L6H                              6
#define _GEN_BITMASK_L6H                       (1 << 6)
#define _GEN_DRIVER_L6H_OUT         (DDRL  |=  (1 << 6))
#define _GEN_DRIVER_L6H_IN          (DDRL  &=~ (1 << 6))
#define _GEN_DRIVER_L6H_PULLUP      (PORTL |=  (1 << 6))
#define _GEN_DRIVER_L6H_HIZ         (PORTL &=~ (1 << 6))
#define _GEN_ON_L6H                 (PORTL |=  (1 << 6))
#define _GEN_OFF_L6H                (PORTL &=~ (1 << 6))
#define _GEN_ACTIVE_L6H             (PINL  &   (1 << 6))
#define _GEN_LATCH_L6H              (PORTL &   (1 << 6))
#define _GEN_TOGGLE_L6H             (PINL   =  (1 << 6))
#define _GEN_DDR_L6H                 DDRL
#define _GEN_PIN_L6H                 PINL
#define _GEN_PORT_L6H                PORTL
#define _GEN_LEVEL_L6H               true
#define _GEN_BITNUM_L6L                              6
#define _GEN_BITMASK_L6L                       (1 << 6)
#define _GEN_DRIVER_L6L_OUT         (DDRL  |=  (1 << 6))
#define _GEN_DRIVER_L6L_IN          (DDRL  &=~ (1 << 6))
#define _GEN_DRIVER_L6L_PULLUP      (PORTL |=  (1 << 6))
#define _GEN_DRIVER_L6L_HIZ         (PORTL &=~ (1 << 6))
#define _GEN_ON_L6L                 (PORTL &=~ (1 << 6))
#define _GEN_OFF_L6L                (PORTL |=  (1 << 6))
#define _GEN_ACTIVE_L6L            !(PINL  &   (1 << 6))
#define _GEN_LATCH_L6L             !(PORTL &   (1 << 6))
#define _GEN_TOGGLE_L6L             (PINL   =  (1 << 6))
#define _GEN_DDR_L6L                 DDRL
#define _GEN_PIN_L6L                 PINL
#define _GEN_PORT_L6L                PORTL
#define _GEN_LEVEL_L6L               false

#define _GEN_BITNUM_L7H                              7
#define _GEN_BITMASK_L7H                       (1 << 7)
#define _GEN_DRIVER_L7H_OUT         (DDRL  |=  (1 << 7))
#define _GEN_DRIVER_L7H_IN          (DDRL  &=~ (1 << 7))
#define _GEN_DRIVER_L7H_PULLUP      (PORTL |=  (1 << 7))
#define _GEN_DRIVER_L7H_HIZ         (PORTL &=~ (1 << 7))
#define _GEN_ON_L7H                 (PORTL |=  (1 << 7))
#define _GEN_OFF_L7H                (PORTL &=~ (1 << 7))
#define _GEN_ACTIVE_L7H             (PINL  &   (1 << 7))
#define _GEN_LATCH_L7H              (PORTL &   (1 << 7))
#define _GEN_TOGGLE_L7H             (PINL   =  (1 << 7))
#define _GEN_DDR_L7H                 DDRL
#define _GEN_PIN_L7H                 PINL
#define _GEN_PORT_L7H                PORTL
#define _GEN_LEVEL_L7H               true
#define _GEN_BITNUM_L7L                              7
#define _GEN_BITMASK_L7L                       (1 << 7)
#define _GEN_DRIVER_L7L_OUT         (DDRL  |=  (1 << 7))
#define _GEN_DRIVER_L7L_IN          (DDRL  &=~ (1 << 7))
#define _GEN_DRIVER_L7L_PULLUP      (PORTL |=  (1 << 7))
#define _GEN_DRIVER_L7L_HIZ         (PORTL &=~ (1 << 7))
#define _GEN_ON_L7L                 (PORTL &=~ (1 << 7))
#define _GEN_OFF_L7L                (PORTL |=  (1 << 7))
#define _GEN_ACTIVE_L7L            !(PINL  &   (1 << 7))
#define _GEN_LATCH_L7L             !(PORTL &   (1 << 7))
#define _GEN_TOGGLE_L7L             (PINL   =  (1 << 7))
#define _GEN_DDR_L7L                 DDRL
#define _GEN_PIN_L7L                 PINL
#define _GEN_PORT_L7L                PORTL
#define _GEN_LEVEL_L7L               false

#ifndef __ASSEMBLER__
	
	typedef struct
	{
		uint8_t mask;
		bool level;
		volatile uint8_t *ddr;
		volatile uint8_t *pin;
		volatile uint8_t *port;
	}
	pinref_t;
	
	#define PINREF(_name, _pin)      pinref_t _name = PINREF_INIT(_pin)
	#define PINREF_INIT(_pin)        { .mask = BITMASK(_pin), .level = LEVEL(_pin), .ddr = &DDR(_pin), .pin = &PIN(_pin), .port = &PORT(_pin) }
	#define PINREF_BIND(_ref, _pin)  _ref = (pinref_t)PINREF_INIT(_pin)
	
	#define PINREF_SET(_ref, _reg)  ((*(_ref._reg)) |=  (_ref.mask))
	#define PINREF_CLR(_ref, _reg)  ((*(_ref._reg)) &= ~(_ref.mask))
	#define PINREF_GET(_ref, _reg)  ((*(_ref._reg)) &   (_ref.mask))
	#define PINREF_PUT(_ref, _reg)  ((*(_ref._reg))  =  (_ref.mask))
	
	#define driver_in(_ref)              PINREF_CLR(_ref, ddr)
	#define driver_in_hiz(_ref)     do { PINREF_CLR(_ref, port); PINREF_CLR(_ref, ddr);  } while( 0 )
	#define driver_in_pullup(_ref)  do { PINREF_CLR(_ref, ddr);  PINREF_SET(_ref, port); } while( 0 )
	#define driver_out(_ref)             PINREF_SET(_ref, ddr)
	#define driver_out_off(_ref)    do { off(_ref); PINREF_SET(_ref, ddr); } while( 0 )
	#define driver_out_on(_ref)     do { PINREF_SET(_ref, ddr);  on(_ref); } while( 0 )
	
	#define on(_ref)           (_ref.level ? PINREF_SET(_ref, port) : PINREF_CLR(_ref, port))
	#define off(_ref)          (_ref.level ? PINREF_CLR(_ref, port) : PINREF_SET(_ref, port))
	#define set(_ref, _level)  ((_level) == _ref.level ? PINREF_SET(_ref, port) : PINREF_CLR(_ref, port))
	#define active(_ref)       (PINREF_GET(_ref, pin) == _ref.level)
	#define latch(_ref)        (PINREF_GET(_ref, port) == _ref.level)
	#define toggle(_ref)        PINREF_PUT(_ref, pin)
	
#endif // __ASSEMBLER__

#endif // __TOS_LIB_PIN_H__

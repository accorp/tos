#ifndef __TOS_LIB_TWI_H__
#define __TOS_LIB_TWI_H__

#if ! defined(TWI_TWBR) \
 || ! defined(TWI_TWPS) \
 || ! defined(TWI_PULLUP) \
 || ! defined(TWI_YIELD) \
 || ! defined(TWI_SYNC) \
 || ! defined(TWI_STATUS)
	#error "not configured"
#endif

#if ! defined(PIN_SCL)
	#error "#include <tos/def_[mcu].h>"
#endif

#if TWI_SYNC == 1 && ! defined(__TOS_EXT_SYNC_H__)
	#error "#include <tos/ext_sync.h>"
#endif

#ifndef __ASSEMBLER__
	
	#if TWI_STATUS != 0
		#define TWIMS_START       0x08
		#define TWIMS_RESTART     0x10
		#define TWIMS_ADDRWACK    0x18
		#define TWIMS_ADDRWNACK   0x20
		#define TWIMS_DATAWACK    0x28
		#define TWIMS_DATAWNACK   0x30
		#define TWIMS_ARBLOST     0x38
		#define TWIMS_ADDRRACK    0x40
		#define TWIMS_ADDRRNACK   0x48
		#define TWIMS_DATARACK    0x50
		#define TWIMS_DATARNACK   0x58
	#endif
	
	#if TWI_SYNC == 1
		MUTEX(twi_mutex);
	#endif
	
	void twi_init_master()
	{
		DRIVER(PIN_SDA(H), IN);
		DRIVER(PIN_SCL(H), IN);
		#if TWI_PULLUP == 1
			DRIVER(PIN_SDA(H), PULLUP);
			DRIVER(PIN_SCL(H), PULLUP);
		#endif
		
		TWBR = TWI_TWBR;      // TWI Bit Rate Register
		TWSR                  // TWI Status Register
			= ( 0 << TWS3  )  // 7-3 TWI Status Bit
			| ( TWI_TWPS << TWPS0 );  // 1-0 TWI Prescaler Bit
		TWCR                  // TWI Control Register
			= ( 0 << TWINT )  // 7 TWI Interrupt Flag
			| ( 0 << TWEA  )  // 6 TWI Enable Acknowledge Bit
			| ( 0 << TWSTA )  // 5 TWI START Condition Bit
			| ( 0 << TWSTO )  // 4 TWI STOP Condition Bit
			| ( 0 << TWWC  )  // 3 TWI Write Collision Flag
			| ( 1 << TWEN  )  // 2 TWI Enable Bit
			| ( 0 << TWIE  ); // 0 TWI Interrupt Enable
		TWAR                  // TWI (Slave) Address Register
			= ( 0 << TWA0  )  // 7-1 TWI (Slave) Address Register
			| ( 0 << TWGCE ); // 0 TWI General Call Recognition Enable Bit
		TWAMR                 // TWI (Slave) Address Mask Register
			= ( 0 << TWAM0 ); // 7-1 TWI Address Mask
	}
	
	#define twi_disable()  TWCR = 0x00
	
	#if TWI_STATUS == 1
		#define twi_status(_status)  ( (TWSR & 0b11111000) == _status )
	#elif TWI_STATUS == 2
		__attribute__((noinline))
		bool twi_status(uint8_t status)
		{
			return( (TWSR & 0b11111000) == status );
		}
	#endif
	
	#if TWI_YIELD == 1
		#define twi_wait()  while( (TWCR & (1 << TWINT)) == 0 ) { yield(); }
	#else
		#define twi_wait()  while( (TWCR & (1 << TWINT)) == 0 ) {}
	#endif
	
	typedef enum
	{
		TWI_WRITE = 0,
		TWI_READ  = 1,
	}
	PACKED twi_mode_t;
	
	void twi_do_write(uint8_t byte)
	{
		TWDR = byte;
		TWCR
			= ( 1 << TWINT )
			| ( 1 << TWEN  );
		twi_wait();
	}
	
	uint8_t twi_do_read(bool last)
	{
		TWCR
			= ( 1            << TWINT )
			| ( last ? 0 : 1 << TWEA  )
			| ( 1            << TWEN  );
		twi_wait();
		return TWDR;
	}
	
	bool twi_do_start(bool restart, uint8_t address, twi_mode_t mode)
	{
		TWCR
			= ( 1 << TWINT )
			| ( 1 << TWSTA )
			| ( 1 << TWEN  );
		twi_wait();
		#if TWI_STATUS == 0
			(void)restart;
		#else
			if( !twi_status(restart ? TWIMS_RESTART : TWIMS_START) )
			{
				return false;
			}
		#endif
		twi_do_write((address << 1) | mode);
		#if TWI_STATUS == 0
			return true;
		#else
			return twi_status(mode == TWI_WRITE ? TWIMS_ADDRWACK : TWIMS_ADDRRACK);
		#endif
	}
	
	void twi_stop()
	{
		TWCR
			= ( 1 << TWINT )
			| ( 1 << TWSTO )
			| ( 1 << TWEN  );
		#if TWI_SYNC == 1
			mutex_unlock(twi_mutex);
		#endif
	}
	
	bool twi_start(uint8_t address, twi_mode_t mode)
	{
		#if TWI_SYNC == 1
			mutex_lock(twi_mutex);
		#endif
		if( !twi_do_start(false, address, mode) )
		{
			twi_stop();
			return false;
		}
		return true;
	}
	
	bool twi_restart(uint8_t address, twi_mode_t mode)
	{
		if( !twi_do_start(true, address, mode) )
		{
			twi_stop();
			return false;
		}
		return true;
	}
	
	bool twi_write(uint8_t byte)
	{
		twi_do_write(byte);
		#if TWI_STATUS != 0
			if( !twi_status(TWIMS_DATAWACK) )
			{
				twi_stop();
				return false;
			}
		#endif
		return true;
	}
	
	bool twi_write_buffer(void* buffer, uint8_t buffer_len)
	{
		uint8_t* ptr = buffer;
		while( buffer_len -- )
		{
			if( !twi_write(*ptr ++) )
			{
				return false;
			}
		}
		return true;
	}
	
	bool twi_read(bool last, uint8_t* byte)
	{
		*byte = twi_do_read(last);
		#if TWI_STATUS != 0
			if( !twi_status(last ? TWIMS_DATARNACK : TWIMS_DATARACK) )
			{
				twi_stop();
				return false;
			}
		#endif
		return true;
	}
	
	bool twi_read_buffer(bool last, void* buffer, uint8_t buffer_len)
	{
		uint8_t* ptr = buffer;
		while( buffer_len -- )
		{
			if( !twi_read(last && (buffer_len == 0), ptr ++) )
			{
				return false;
			}
		}
		return true;
	}
	
#endif // __ASSEMBLER__

#endif // __TOS_LIB_TWI_H__

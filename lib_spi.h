#ifndef __TOS_LIB_SPI_H__
#define __TOS_LIB_SPI_H__

#if ! defined(SPI_SPR) \
 || ! defined(SPI_2X) \
 || ! defined(SPI_YIELD) \
 || ! defined(SPI_SYNC)
	#error "not configured"
#endif

#if ! defined(PIN_SCK)
	#error "#include <tos/def_[mcu].h>"
#endif

#if SPI_SYNC == 1 && ! defined(__TOS_EXT_SYNC_H__)
	#error "#include <tos/ext_sync.h>"
#endif

#ifndef __ASSEMBLER__
	
	#if SPI_SYNC == 1
		MUTEX(spi_mutex);
	#endif
	
	void spi_init_master()
	{
		DRIVER(PIN_SCK(H), OUT);
		DRIVER(PIN_MOSI(H), OUT);
		DRIVER(PIN_MISO(H), IN);
		DRIVER(PIN_SS(H), OUT);
		
		SPSR
			= ( 0 << SPIF   )  // 7 SPI Interrupt Flag
			| ( 0 << WCOL   )  // 6 Write COLlision Flag
			| ( SPI_2X << SPI2X ); // 0 Double SPI Speed Bit
		
		SPCR
			= ( 0 << SPIE   )  // 7 SPI Interrupt Enable
			| ( 1 << SPE    )  // 6 SPI Enable
			| ( 0 << DORD   )  // 5 Data Order
			| ( 1 << MSTR   )  // 4 Master/Slave Select
			| ( 0 << CPOL   )  // 3 Clock Polarity
			| ( 0 << CPHA   )  // 2 Clock Phase
			| ( SPI_SPR << SPR0  ); // 1-0 SPI Clock Rate Select
	}
	
	#if SPI_SYNC == 1
		#define spi_start(_pin)  do { mutex_lock(spi_mutex); ON(_pin); } while( 0 )
		#define spi_stop(_pin)   do { OFF(_pin); mutex_unlock(spi_mutex); } while( 0 )
	#else
		#define spi_start(_pin)  ON(_pin)
		#define spi_stop(_pin)   OFF(_pin)
	#endif
	
	#if SPI_YIELD == 1
		#define spi_wait()  while( (SPSR & (1 << SPIF)) == 0 ) { yield(); }
	#else
		#define spi_wait()  while( (SPSR & (1 << SPIF)) == 0 ) {}
	#endif
	
	#define spi_write(_b)  SPDR = (_b)
	#define spi_read()     SPDR
	
	void spi_byte(uint8_t byte)
	{
		spi_write(byte);
		spi_wait();
	}
	
	void spi_buffer(void* buffer, uint16_t buffer_len)
	{
		uint8_t* ptr = buffer;
		while( buffer_len -- )
		{
			spi_write(*ptr ++);
			spi_wait();
		}
	}
	
	void spi_exchange(void* buffer, uint16_t buffer_len)
	{
		uint8_t* ptr = buffer;
		while( buffer_len -- )
		{
			spi_write(*ptr);
			spi_wait();
			*ptr ++ = spi_read();
		}
	}
	
#endif // __ASSEMBLER__

#endif // __TOS_LIB_SPI_H__

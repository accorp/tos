#ifndef __TOS_TASK_DISPLAY_H__
#define __TOS_TASK_DISPLAY_H__

#if ! defined(DISPLAY_HEIGHT) \
 || ! defined(DISPLAY_WIDTH) \
 || ! defined(DISPLAY_WIDTH_EXTRA) \
 || ! defined(DISPLAY_COMMIT) \
 || ! defined(DISPLAY_SYNC)
	#error "not configured"
#endif

#if ! defined(__TOS_LIB_LCD_H__)
	#error "#include <tos/lib_lcd.h>"
#endif

#if ! defined(__TOS_EXT_SYNC_H__)
	#error "#include <tos/ext_sync.h>"
#endif

#ifndef __ASSEMBLER__
	
	#define DISPLAY_WIDTH_EX  ( DISPLAY_WIDTH + DISPLAY_WIDTH_EXTRA )
	
	NOINIT lcd_t display_lcd;
	
	#define display_command(cmd, arg) lcd_command(&display_lcd, (cmd), (arg))
	#define display_custom(code, map) lcd_custom(&display_lcd, (code), (map))
	#define display_on()              lcd_backlight(&display_lcd, true)
	#define display_off()             lcd_backlight(&display_lcd, false)
	
	NOINIT char display_buffer[DISPLAY_HEIGHT][DISPLAY_WIDTH_EX];
	#define display_fill(chr)         memset(&display_buffer[0][0], (chr), DISPLAY_HEIGHT * DISPLAY_WIDTH_EX)
	#define display_clear()           display_fill(' ')
	#define display_clear_line(y)     memset(&display_buffer[y][0], ' ', DISPLAY_WIDTH)
	#define display_set(y, x, chr)    display_buffer[y][x] = (chr)
	#define display_print(y, x, str)  strcpy(&display_buffer[y][x], (str))
	#define display_copy(y, x, b, l)  memcpy(&display_buffer[y][x], (b), (l));
	
	SWITCH(display);
	#define display_wait()            switch_waiton(display)
	#define display_update()          switch_off(display)
	#define display_update_wait()     switch_off_waiton(display)
	
	#if DISPLAY_SYNC == 1
		
		MUTEX(display_mutex);
		#define display_lock()            do { display_wait(); mutex_lock(display_mutex); } while( 0 )
		#define display_unlock()          do { mutex_unlock(display_mutex); display_update(); } while( 0 )
		
	#endif
	
	void display_commit(lcd_t *lcd)
	{
		DISPLAY_COMMIT(lcd);
	}
	
	THREAD(display, 0x38)
	{
		display_clear();
		lcd_init(&display_lcd, DISPLAY_HEIGHT, DISPLAY_WIDTH, display_commit);
		
		display_command(LCD_CMD_MODE, LCD_POS_INC | LCD_DISP_KEEP);
		display_command(LCD_CMD_CTRL, LCD_DISP_ON | LCD_CURS_OFF | LCD_BLINK_OFF);
		display_command(LCD_CMD_FUNC, LCD_DATA_4BIT | LCD_LINES_2 | LCD_FONT_5x8);
		
		display_custom(0x00, 0x0000000000000000);
		display_on();
		
		while( 1 )
		{
			bool seq;
			int y;
			for( y = 0; y < DISPLAY_HEIGHT; y ++ )
			{
				seq = false;
				int x;
				for( x = 0; x < DISPLAY_WIDTH; x ++ )
				{
					if( display_buffer[y][x] == 0xFF )
					{
						seq = false;
					}
					else
					{
						if( !seq )
						{
							lcd_goto(&display_lcd, y, x);
							seq = true;
						}
						lcd_write(&display_lcd, display_buffer[y][x]);
						display_buffer[y][x] = 0xFF;
					}
				}
			}
			switch_on_waitoff(display);
		}
	}
	
#endif // __ASSEMBLER__

#endif // __TOS_TASK_DISPLAY_H__

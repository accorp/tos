#ifndef __TOS_LIB_CRC_H__
#define __TOS_LIB_CRC_H__

#ifndef __ASSEMBLER__
	
	#define CRC8_CCITT_POLYNOMIAL  0x07
	#define CRC8_CCITT_INIT        0x00
	#define CRC8_CCITT_XOROUT      0x00
	
	#define CRC8_CDMA2000_POLYNOMIAL  0x9B
	#define CRC8_CDMA2000_INIT        0xFF
	#define CRC8_CDMA2000_XOROUT      0x00
	
	#define CRC8_DVBS2_POLYNOMIAL  0xD5
	#define CRC8_DVBS2_INIT        0x00
	#define CRC8_DVBS2_XOROUT      0x00
	
	#define CRC8_ICODE_POLYNOMIAL  0x1D
	#define CRC8_ICODE_INIT        0xFD
	#define CRC8_ICODE_XOROUT      0x00
	
	#define CRC8_ITU_POLYNOMIAL  0x07
	#define CRC8_ITU_INIT        0x00
	#define CRC8_ITU_XOROUT      0x55
	
	#define CRC16_AUGCCITT_POLYNOMIAL  0x1021
	#define CRC16_AUGCCITT_INIT        0x1D0F
	#define CRC16_AUGCCITT_XOROUT      0x0000
	
	#define CRC16_CCITT_POLYNOMIAL  0x1021
	#define CRC16_CCITT_INIT        0xFFFF
	#define CRC16_CCITT_XOROUT      0x0000
	
	#define CRC16_GENIBUS_POLYNOMIAL  0x1021
	#define CRC16_GENIBUS_INIT        0xFFFF
	#define CRC16_GENIBUS_XOROUT      0xFFFF
	
	#define CRC16_XMODEM_POLYNOMIAL  0x1021
	#define CRC16_XMODEM_INIT        0x0000
	#define CRC16_XMODEM_XOROUT      0x0000
	
	#define CRC16_BUYPASS_POLYNOMIAL  0x8005
	#define CRC16_BUYPASS_INIT        0x0000
	#define CRC16_BUYPASS_XOROUT      0x0000
	
	#define CRC16_DDS110_POLYNOMIAL  0x8005
	#define CRC16_DDS110_INIT        0x800D
	#define CRC16_DDS110_XOROUT      0x0000
	
	#define CRC16_CDMA2000_POLYNOMIAL  0xC867
	#define CRC16_CDMA2000_INIT        0xFFFF
	#define CRC16_CDMA2000_XOROUT      0x0000
	
	#define CRC16_DECTR_POLYNOMIAL  0x0589
	#define CRC16_DECTR_INIT        0x0000
	#define CRC16_DECTR_XOROUT      0x0001
	
	#define CRC16_DECTX_POLYNOMIAL  0x0589
	#define CRC16_DECTX_INIT        0x0000
	#define CRC16_DECTX_XOROUT      0x0000
	
	#define CRC16_EN13757_POLYNOMIAL  0x3D65
	#define CRC16_EN13757_INIT        0x0000
	#define CRC16_EN13757_XOROUT      0xFFFF
	
	#define CRC16_T10DIF_POLYNOMIAL  0x8BB7
	#define CRC16_T10DIF_INIT        0x0000
	#define CRC16_T10DIF_XOROUT      0x0000
	
	#define CRC16_TELEDISK_POLYNOMIAL  0xA097
	#define CRC16_TELEDISK_INIT        0x0000
	#define CRC16_TELEDISK_XOROUT      0x0000
	
	#define crc8_init(_name)                 (__JOIN3(CRC8_, _name, _INIT))
	#define crc8_update(_name, _crc, _byte)  crc8_do_update(__JOIN3(CRC8_, _name, _POLYNOMIAL), _crc, _byte)
	#define crc8_get(_name, _crc)            ((_crc) ^ __JOIN3(CRC8_, _name, _XOROUT))
	
	#define crc8_calc(_name, _buffer, _length) \
		(crc8_do_calc( \
			__JOIN3(CRC8_, _name, _POLYNOMIAL), \
			__JOIN3(CRC8_, _name, _INIT), \
			_buffer, _length) \
		^ __JOIN3(CRC8_, _name, _XOROUT))
	
	#define crc16_init(_name)                 (__JOIN3(CRC16_, _name, _INIT))
	#define crc16_update(_name, _crc, _byte)  crc16_do_update(__JOIN3(CRC16_, _name, _POLYNOMIAL), _crc, _byte)
	#define crc16_get(_name, _crc)            ((_crc) ^ __JOIN3(CRC16_, _name, _XOROUT))
	
	#define crc16_calc(_name, _buffer, _length) \
		(crc16_do_calc( \
			__JOIN3(CRC16_, _name, _POLYNOMIAL), \
			__JOIN3(CRC16_, _name, _INIT), \
			_buffer, _length) \
		^ __JOIN3(CRC16_, _name, _XOROUT))
	
	uint8_t crc8_do_update(uint8_t polynomial, uint8_t crc, uint8_t byte)
	{
		crc = crc ^ byte;
		uint8_t i;
		for( i = 0; i < 8; i ++ )
		{
			if( crc & 0x80 )
			{
				crc = (crc << 1) ^ polynomial;
			}
			else
			{
				crc = (crc << 1);
			}
		}
		return crc;
	}
	
	uint8_t crc8_do_calc(uint8_t polynomial, uint8_t crc, uint8_t* buffer, uint16_t length)
	{
		while( length -- )
		{
			crc = crc8_do_update(polynomial, crc, *buffer ++);
		}
		return crc;
	}
	
	uint16_t crc16_do_update(uint16_t polynomial, uint16_t crc, uint8_t byte)
	{
		crc = crc ^ (byte << 8);
		uint8_t i;
		for( i = 0; i < 8; i ++ )
		{
			if( crc & 0x8000 )
			{
				crc = (crc << 1) ^ polynomial;
			}
			else
			{
				crc = (crc << 1);
			}
		}
		return crc;
	}
	
	uint16_t crc16_do_calc(uint16_t polynomial, uint16_t crc, uint8_t* buffer, uint16_t length)
	{
		while( length -- )
		{
			crc = crc16_do_update(polynomial, crc, *buffer ++);
		}
		return crc;
	}
	
#endif // __ASSEMBLER__

#endif // __TOS_LIB_CRC_H__

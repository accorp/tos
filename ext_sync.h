#ifndef __TOS_EXT_SYNC_H__
#define __TOS_EXT_SYNC_H__

#if THREAD_EXT_SIZE < 2
	#error "#define THREAD_EXT_SIZE  2"
#endif

#ifndef __ASSEMBLER__
	
	typedef volatile uint8_t sync_t;
	
	void sync_zero(sync_t *sync);
	void sync_nonzero(sync_t *sync);
	
	#define sync_waitzero(_name)       if(  (_name) ) {    sync_zero(&(_name)); } else {}
	#define sync_waitnonzero(_name)    if( !(_name) ) { sync_nonzero(&(_name)); } else {}
	
	// event
	#define event_t                    sync_t
	#define EVENT(_name)               event_t _name = 0x00
	#define event_reset(_name)         (_name) = 0x00
	#define event_fire(_name)          (_name) = 0xFF
	#define event_set(_name, _value)   (_name) = (_value)
	#define event_isset(_name)         (_name)
	#define event_wait(_name)          sync_waitnonzero(_name)
	#define event_wait_reset(_name)    do { sync_waitnonzero(_name); (_name) = 0x00; } while( 0 )
	
	// mutex
	#define mutex_t                    sync_t
	#define MUTEX(_name)               mutex_t _name = 0x00
	#define mutex_locked(_name)        (_name)
	#define mutex_lock(_name)          do { sync_waitzero(_name); (_name) = 0xFF; } while( 0 )
	#define mutex_unlock(_name)        (_name) = 0x00
	#define mutex_wait(_name)          sync_waitzero(_name)
	
	// semaphore
	#define semaphore_t                sync_t
	#define SEMAPHORE(_name, _init)    semaphore_t _name = _init
	#define semaphore_ready(_name)     (_name)
	#define semaphore_enter(_name)     do { sync_waitnonzero(_name); (_name) --; } while( 0 )
	#define semaphore_leave(_name)     (_name) ++
	#define semaphore_wait(_name)      sync_waitnonzero(_name)
	
	// multiple lock
	#define multi_lock_t               sync_t
	#define MULTI_LOCK(_name)          multi_lock_t _name = 0x00
	#define multi_locked(_name)        (_name)
	#define multi_lock(_name)          (_name) ++
	#define multi_unlock(_name)        (_name) --
	#define multi_wait(_name)          sync_waitzero(_name)
	#define multi_waitlock(_name)      sync_waitnonzero(_name)
	
	// switch
	#define switch_t                   sync_t
	#define SWITCH(_name)              switch_t _name = 0x00
	#define switch_off(_name)          (_name) = 0x00
	#define switch_on(_name)           (_name) = 0xFF
	#define switch_set(_name, _value)  (_name) = (_value)
	#define switch_test(_name)         (_name)
	#define switch_waiton(_name)       sync_waitnonzero(_name)
	#define switch_waitoff(_name)      sync_waitzero(_name)
	#define switch_off_waiton(_name)   do { (_name) = 0x00; sync_nonzero(&(_name)); } while( 0 )
	#define switch_on_waitoff(_name)   do { (_name) = 0xFF;    sync_zero(&(_name)); } while( 0 )
	
#else // __ASSEMBLER__
	
	#define OFFSET_SYNC  (OFFSET_EXT + 0)
	
	
	.section TOS_SECTION
	
	
	.global sync_zero // void (sync_t* 24:25)
	sync_zero:
		ldi   r18, pm_lo8(sync_zero_restore)
		ldi   r19, pm_hi8(sync_zero_restore)
		ldi   r30, pm_lo8(sync_save)
		ldi   r31, pm_hi8(sync_save)
		rjmp  tos_save
	
	
	.global sync_nonzero // void (sync_t* 24:25)
	sync_nonzero:
		ldi   r18, pm_lo8(sync_nonzero_restore)
		ldi   r19, pm_hi8(sync_nonzero_restore)
		ldi   r30, pm_lo8(sync_save)
		ldi   r31, pm_hi8(sync_save)
		rjmp  tos_save
	
	
	sync_save:
		std   Y+OFFSET_SYNC+0, r24
		std   Y+OFFSET_SYNC+1, r25
		rjmp  tos_next
	
	
	sync_zero_restore:
		ldd   r26, Y+OFFSET_SYNC+0
		ldd   r27, Y+OFFSET_SYNC+1
		ld    r18, X
		tst   r18
		breq  sync_restore
		rjmp  tos_next
	
	
	sync_nonzero_restore:
		ldd   r26, Y+OFFSET_SYNC+0
		ldd   r27, Y+OFFSET_SYNC+1
		ld    r18, X
		tst   r18
		brne  sync_restore
		rjmp  tos_next
	
	
	sync_restore:
		rjmp  tos_restore
	
	
#endif // __ASSEMBLER__

#endif // __TOS_EXT_SYNC_H__

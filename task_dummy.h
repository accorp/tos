#ifndef __TOS_TASK_DUMMY_H__
#define __TOS_TASK_DUMMY_H__

#ifndef __ASSEMBLER__
	
	KTASK(dummy);
	
	uint32_t dummy_switch_count = 0;
	
#else // __ASSEMBLER__
	
	
	.section TOS_SECTION
	
	
	.global TASK_NAME(dummy) // void ()
	TASK_NAME(dummy):
		lds   r24, dummy_switch_count+0
		lds   r25, dummy_switch_count+1
		lds   r26, dummy_switch_count+2
		lds   r27, dummy_switch_count+3
		adiw  r24, 0x01
		adc   r26, r1
		adc   r27, r1
		sts   dummy_switch_count+0, r24
		sts   dummy_switch_count+1, r25
		sts   dummy_switch_count+2, r26
		sts   dummy_switch_count+3, r27
		rjmp  tos_next
	
	
#endif // __ASSEMBLER__

#endif // __TOS_TASK_DUMMY_H__

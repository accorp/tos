#ifndef __TOS_LIB_LCD_H__
#define __TOS_LIB_LCD_H__

#ifndef __ASSEMBLER__
	
	/*
		Instruction               RS  R/W DB7 DB6 DB5 DB4 DB3 DB2 DB1 DB0  Time
		Clear display             0   0   0   0   0   0   0   0   0   1        
		Return home               0   0   0   0   0   0   0   0   1   -    26us
		Entry mode set            0   0   0   0   0   0   0   1   ID  S   624ns
		Display on/off control    0   0   0   0   0   0   1   D   C   B   624ns
		Cursor or display shift   0   0   0   0   0   1   SC  RL  -   -   624ns
		Function set              0   0   0   0   1   DL  N   F   -   -   624ns
		Set CGRAM address         0   0   0   1   ACG ACG ACG ACG ACG ACG 624ns
		Set DDRAM address         0   0   1   ADD ADD ADD ADD ADD ADD ADD 624ns
		Read busy flag & address  0   1   BF  AC  AC  AC  AC  AC  AC  AC  0    
		Write data                1   0   WD  WD  WD  WD  WD  WD  WD  WD  624ns
		Read data                 1   1   RD  RD  RD  RD  RD  RD  RD  RD  624ns
	*/
	
	// Clears entire display and sets DDRAM address 0 in address counter.
	#define LCD_CMD_CLEAR   (1 << 0)
	
	// Sets DDRAM address 0 in address counter. Also returns display from being shifted to original position. DDRAM contents remain unchanged.
	#define LCD_CMD_HOME    (1 << 1)
	
	// Sets cursor move direction and specifies display shift. These operations are performed during data write and read.
	#define LCD_CMD_MODE    (1 << 2)
	#define LCD_POS_DEC     (0 << 1)  // Decrement position
	#define LCD_POS_INC     (1 << 1)  // Increment position
	#define LCD_DISP_KEEP   (0 << 0)  // No display shift
	#define LCD_DISP_SHIFT  (1 << 0)  // Accompanies display shift
	
	// Sets entire display on/off, cursor on/off, and blinking of cursor position character.
	#define LCD_CMD_CTRL    (1 << 3)
	#define LCD_DISP_OFF    (0 << 2)  // Display off
	#define LCD_DISP_ON     (1 << 2)  // Display on
	#define LCD_CURS_OFF    (0 << 1)  // Cursor off
	#define LCD_CURS_ON     (1 << 1)  // Cursor on
	#define LCD_BLINK_OFF   (0 << 0)  // Blink cursor off
	#define LCD_BLINK_ON    (1 << 0)  // Blink cursor on
	
	// Moves cursor and shifts display without changing DDRAM contents.
	#define LCD_CMD_SHIFT   (1 << 4)
	#define LCD_SHIFT_CURS  (0 << 3)  // Cursor move
	#define LCD_SHIFT_DISP  (1 << 3)  // Display shift
	#define LCD_SHIFT_LEFT  (0 << 2)  // Shift to the left
	#define LCD_SHIFT_RIGHT (1 << 2)  // Shift to the right
	
	// Sets interface data length, number of display lines, and character font.
	#define LCD_CMD_FUNC    (1 << 5)
	#define LCD_DATA_4BIT   (0 << 4)  // Data length 4 bits
	#define LCD_DATA_8BIT   (1 << 4)  // Data length 8 bits
	#define LCD_LINES_1     (0 << 3)  // Display 1 line
	#define LCD_LINES_2     (1 << 3)  // Display 2 lines
	#define LCD_FONT_5x8    (0 << 2)  // Font 5 x 8 dots
	#define LCD_FONT_5x10   (1 << 2)  // Font 5 x 10 dots
	
	// Sets CGRAM address. CGRAM data is sent and received after this setting.
	#define LCD_CMD_CGRAM   (1 << 6)
	
	// Sets DDRAM address. DDRAM data is sent and received after this setting.
	#define LCD_CMD_DDRAM   (1 << 7)
	
	// Busy flag
	#define LCD_FLAG_READY  (0 << 7)  // BF=0: Instructions acceptable
	#define LCD_FLAG_BUSY   (1 << 7)  // BF=1: Internally operating
	
	uint8_t lcd_line_start[2][4] =
	{
		{0x00, 0x40, 0x10, 0x50},
		{0x00, 0x40, 0x14, 0x54},
	};
	
	#define LCD_BIT_RS      0
	#define LCD_BIT_RW      1
	#define LCD_BIT_EN      2
	#define LCD_BIT_BL      3
	#define LCD_BIT_D4      4
	#define LCD_BIT_D5      5
	#define LCD_BIT_D6      6
	#define LCD_BIT_D7      7
	
	#define lcd_command(lcd, cmd, arg)  lcd_dispatch((lcd), true, (cmd) | (arg))
	#define lcd_write(lcd, chr)         lcd_dispatch((lcd), false, (chr))
	#define lcd_clear(lcd)              lcd_command((lcd), LCD_CMD_CLEAR, 0)
	#define lcd_home(lcd)               lcd_command((lcd), LCD_CMD_HOME, 0)
	#define lcd_goto(lcd, y, x)         lcd_command((lcd), LCD_CMD_DDRAM, (lcd)->start[y] + (x))
	#define lcd_print(lcd, y, x, str)   lcd_buffer((lcd), (y), (x), (uint8_t*)(str), strlen(str))
	
	typedef struct lcd_struct_t lcd_t;
	typedef void (*lcd_commit_t)(lcd_t*);
	
	struct lcd_struct_t
	{
		uint8_t height;
		uint8_t width;
		uint8_t *start;
		uint8_t port;
		lcd_commit_t commit;
	};
	
	typedef union
	{
		uint8_t  u8[8];
		uint16_t u16[4];
		uint32_t u32[2];
		uint64_t u64;
	}
	lcd_charmap_t;
	
	void lcd_pulse(lcd_t *lcd)
	{
		lcd->port |= ( 1 << LCD_BIT_EN );
		lcd->commit(lcd);
		sleep(USEC(450));
		lcd->port &= ~( 1 << LCD_BIT_EN );
		lcd->commit(lcd);
	}
	
	void lcd_init(lcd_t *lcd, uint8_t height, uint8_t width, lcd_commit_t commit)
	{
		lcd->height = height;
		lcd->width = width;
		lcd->commit = commit;
		lcd->start = lcd_line_start[width / 20];
		
		sleep_to_time(MSEC(50));
		
		lcd->port = ( 0x03 << 4 );
		lcd_pulse(lcd);
		lcd_pulse(lcd);
		lcd_pulse(lcd);
		lcd->port = ( 0x02 << 4 );
		lcd_pulse(lcd);
	}
	
	void lcd_dispatch(lcd_t *lcd, bool command, uint8_t value)
	{
		if( command )
		{
			lcd->port &= ~( 1 << LCD_BIT_RS );
		}
		else
		{
			lcd->port |= ( 1 << LCD_BIT_RS );
		}
		
		lcd->port = ( lcd->port & 0x0F ) | ( value & 0xF0 );
		lcd_pulse(lcd);
		
		lcd->port = ( lcd->port & 0x0F ) | ( value << 4 );
		lcd_pulse(lcd);
		
		if( command )
		{
			sleep(MSEC(3));
		}
	}
	
	void lcd_custom(lcd_t *lcd, uint8_t code, uint64_t map)
	{
		lcd_command(lcd, LCD_CMD_CGRAM, (code << 3));
		int i;
		for( i = 0; i < 8; i ++ )
		{
			lcd_write(lcd, ((lcd_charmap_t)map).u8[i]);
		}
	}
	
	void lcd_buffer(lcd_t *lcd, uint8_t y, uint8_t x, uint8_t *buf, uint8_t len)
	{
		lcd_goto(lcd, y, x);
		while( len -- )
		{
			lcd_write(lcd, *buf ++);
		}
	}
	
	void lcd_fill(lcd_t *lcd, uint8_t chr)
	{
		uint8_t y = lcd->height;
		while( y -- )
		{
			lcd_goto(lcd, y, 0);
			uint8_t x = lcd->width;
			while( x -- )
			{
				lcd_write(lcd, chr);
			}
		}
	}
	
	void lcd_backlight(lcd_t *lcd, bool state)
	{
		if( state )
		{
			lcd->port |= ( 1 << LCD_BIT_BL );
		}
		else
		{
			lcd->port &= ~( 1 << LCD_BIT_BL );
		}
		lcd->commit(lcd);
	}
	
#endif // __ASSEMBLER__

#endif // __TOS_LIB_LCD_H__

--
-- lib_pin.h
--

local lib_name = "lib_pin"
local port_min = ("A"):byte()
local port_max = ("L"):byte()

local file, msg = io.open(("../%s.h"):format(lib_name), "wb")
if not file then
	print(("io error: %s"):format(msg))
	return
end

local indent

local function put(s, ...)
	file:write(indent, s:format(...), "\n")
end

local function def(s, ...)
	put("#define " .. s, ...)
end

indent = ""
put("#ifndef __TOS_%s_H__", lib_name:upper())
put("#define __TOS_%s_H__", lib_name:upper())
put ""
put "// GENERATED FILE. DO NOT EDIT."
put ""
def "BITNUM(_pin)                 BITNUM_IMP(_pin)"
def "BITMASK(_pin)                BITMASK_IMP(_pin)"
def "DRIVER(_pin, _mode)          DRIVER_IMP(_pin, _mode)"
def "ON(_pin)                     ON_IMP(_pin)"
def "OFF(_pin)                    OFF_IMP(_pin)"
def "ACTIVE(_pin)                 ACTIVE_IMP(_pin)"
def "LATCH(_pin)                  LATCH_IMP(_pin)"
def "TOGGLE(_pin)                 TOGGLE_IMP(_pin)"
def "DDR(_pin)                    DDR_IMP(_pin)"
def "PIN(_pin)                    PIN_IMP(_pin)"
def "PORT(_pin)                   PORT_IMP(_pin)"
def "LEVEL(_pin)                  LEVEL_IMP(_pin)"
put ""
def "BITNUM_IMP(_pin)            _GEN_BITNUM_  ## _pin"
def "BITMASK_IMP(_pin)           _GEN_BITMASK_ ## _pin"
def "DRIVER_IMP(_pin, _mode)     _GEN_DRIVER_  ## _pin ## _ ## _mode"
def "ON_IMP(_pin)                _GEN_ON_      ## _pin"
def "OFF_IMP(_pin)               _GEN_OFF_     ## _pin"
def "ACTIVE_IMP(_pin)            _GEN_ACTIVE_  ## _pin"
def "LATCH_IMP(_pin)             _GEN_LATCH_   ## _pin"
def "TOGGLE_IMP(_pin)            _GEN_TOGGLE_  ## _pin"
def "DDR_IMP(_pin)               _GEN_DDR_     ## _pin"
def "PIN_IMP(_pin)               _GEN_PIN_     ## _pin"
def "PORT_IMP(_pin)              _GEN_PORT_    ## _pin"
def "LEVEL_IMP(_pin)             _GEN_LEVEL_   ## _pin"
for port = port_min, port_max do
	for bit = 0, 7 do
		put ""
		for _, level in ipairs({"H", "L"}) do
			local function pdf(s)
				s = s:gsub("[@#$%%]", {
					['@'] = string.char(port),
					['#'] = bit,
					['$'] = level,
					['%'] = (level == "H" and "true" or "false"),
				})
				return def(s)
			end
			pdf "_GEN_BITNUM_@#$                              #"
			pdf "_GEN_BITMASK_@#$                       (1 << #)"
			pdf "_GEN_DRIVER_@#$_OUT         (DDR@  |=  (1 << #))"
			pdf "_GEN_DRIVER_@#$_IN          (DDR@  &=~ (1 << #))"
			pdf "_GEN_DRIVER_@#$_PULLUP      (PORT@ |=  (1 << #))"
			pdf "_GEN_DRIVER_@#$_HIZ         (PORT@ &=~ (1 << #))"
		if level == "H" then
			pdf "_GEN_ON_@#$                 (PORT@ |=  (1 << #))"
			pdf "_GEN_OFF_@#$                (PORT@ &=~ (1 << #))"
			pdf "_GEN_ACTIVE_@#$             (PIN@  &   (1 << #))"
			pdf "_GEN_LATCH_@#$              (PORT@ &   (1 << #))"
		else
			pdf "_GEN_ON_@#$                 (PORT@ &=~ (1 << #))"
			pdf "_GEN_OFF_@#$                (PORT@ |=  (1 << #))"
			pdf "_GEN_ACTIVE_@#$            !(PIN@  &   (1 << #))"
			pdf "_GEN_LATCH_@#$             !(PORT@ &   (1 << #))"
		end
			pdf "_GEN_TOGGLE_@#$             (PIN@   =  (1 << #))"
			pdf "_GEN_DDR_@#$                 DDR@"
			pdf "_GEN_PIN_@#$                 PIN@"
			pdf "_GEN_PORT_@#$                PORT@"
			pdf "_GEN_LEVEL_@#$               %"
		end
	end
end
put ""
put "#ifndef __ASSEMBLER__"
indent = "\t"
put ""
put "typedef struct"
put "{"
put "	uint8_t mask;"
put "	bool level;"
put "	volatile uint8_t *ddr;"
put "	volatile uint8_t *pin;"
put "	volatile uint8_t *port;"
put "}"
put "pinref_t;"
put ""
def "PINREF(_name, _pin)      pinref_t _name = PINREF_INIT(_pin)"
def "PINREF_INIT(_pin)        { .mask = BITMASK(_pin), .level = LEVEL(_pin), .ddr = &DDR(_pin), .pin = &PIN(_pin), .port = &PORT(_pin) }"
def "PINREF_BIND(_ref, _pin)  _ref = (pinref_t)PINREF_INIT(_pin)"
put ""
def "PINREF_SET(_ref, _reg)  ((*(_ref._reg)) |=  (_ref.mask))"
def "PINREF_CLR(_ref, _reg)  ((*(_ref._reg)) &= ~(_ref.mask))"
def "PINREF_GET(_ref, _reg)  ((*(_ref._reg)) &   (_ref.mask))"
def "PINREF_PUT(_ref, _reg)  ((*(_ref._reg))  =  (_ref.mask))"
put ""
def "driver_in(_ref)              PINREF_CLR(_ref, ddr)"
def "driver_in_hiz(_ref)     do { PINREF_CLR(_ref, port); PINREF_CLR(_ref, ddr);  } while( 0 )"
def "driver_in_pullup(_ref)  do { PINREF_CLR(_ref, ddr);  PINREF_SET(_ref, port); } while( 0 )"
def "driver_out(_ref)             PINREF_SET(_ref, ddr)"
def "driver_out_off(_ref)    do { off(_ref); PINREF_SET(_ref, ddr); } while( 0 )"
def "driver_out_on(_ref)     do { PINREF_SET(_ref, ddr);  on(_ref); } while( 0 )"
put ""
def "on(_ref)           (_ref.level ? PINREF_SET(_ref, port) : PINREF_CLR(_ref, port))"
def "off(_ref)          (_ref.level ? PINREF_CLR(_ref, port) : PINREF_SET(_ref, port))"
def "set(_ref, _level)  ((_level) == _ref.level ? PINREF_SET(_ref, port) : PINREF_CLR(_ref, port))"
def "active(_ref)       (PINREF_GET(_ref, pin) == _ref.level)"
def "latch(_ref)        (PINREF_GET(_ref, port) == _ref.level)"
def "toggle(_ref)        PINREF_PUT(_ref, pin)"
put ""
indent = ""
put "#endif // __ASSEMBLER__"
put ""
put("#endif // __TOS_%s_H__", lib_name:upper())

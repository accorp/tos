#ifndef __TOS_DEF_ARDUINO_PRO_MINI_328P_H__
#define __TOS_DEF_ARDUINO_PRO_MINI_328P_H__

#define PIN_0(_A)   __JOIN2(D0, _A)
#define PIN_1(_A)   __JOIN2(D1, _A)
#define PIN_2(_A)   __JOIN2(D2, _A)
#define PIN_3(_A)   __JOIN2(D3, _A)
#define PIN_4(_A)   __JOIN2(D4, _A)
#define PIN_5(_A)   __JOIN2(D5, _A)
#define PIN_6(_A)   __JOIN2(D6, _A)
#define PIN_7(_A)   __JOIN2(D7, _A)
#define PIN_8(_A)   __JOIN2(B0, _A)
#define PIN_9(_A)   __JOIN2(B1, _A)
#define PIN_10(_A)  __JOIN2(B2, _A)
#define PIN_11(_A)  __JOIN2(B3, _A)
#define PIN_12(_A)  __JOIN2(B4, _A)
#define PIN_13(_A)  __JOIN2(B5, _A)

#define PIN_A0(_A)   __JOIN2(C0, _A)
#define PIN_A1(_A)   __JOIN2(C1, _A)
#define PIN_A2(_A)   __JOIN2(C2, _A)
#define PIN_A3(_A)   __JOIN2(C3, _A)
#define PIN_A4(_A)   __JOIN2(C4, _A)
#define PIN_A5(_A)   __JOIN2(C5, _A)

#define PIN_TXO(_A)  PIN_1(_A)
#define PIN_RXI(_A)  PIN_0(_A)

#define PIN_LED(_A)  PIN_13(_A)

#endif // __TOS_DEF_ARDUINO_PRO_MINI_328P_H__

#ifndef __TOS_LIB_TIMER_H__
#define __TOS_LIB_TIMER_H__

#define PIN_OCA(_n, _L)  __JOIN3(PIN_OC, _n, A)(_L)
#define PIN_OCB(_n, _L)  __JOIN3(PIN_OC, _n, B)(_L)
#define PIN_OCC(_n, _L)  __JOIN3(PIN_OC, _n, C)(_L)
#define PIN_ICP(_n, _L)  __JOIN2(PIN_ICP, _n)(_L)

#define TCNT(_n)     __JOIN2(TCNT, _n)       // Timer/Counter
#define TCNTH(_n)    __JOIN3(TCNT, _n, H)    // Timer/Counter HIGH
#define TCNTL(_n)    __JOIN3(TCNT, _n, L)    // Timer/Counter LOW

#define OCRA(_n)     __JOIN3(OCR, _n, A)     // Output Compare Register A
#define OCRAH(_n)    __JOIN3(OCR, _n, AH)    // Output Compare Register A HIGH
#define OCRAL(_n)    __JOIN3(OCR, _n, AL)    // Output Compare Register A LOW

#define OCRB(_n)     __JOIN3(OCR, _n, B)     // Output Compare Register B
#define OCRBH(_n)    __JOIN3(OCR, _n, BH)    // Output Compare Register B HIGH
#define OCRBL(_n)    __JOIN3(OCR, _n, BL)    // Output Compare Register B LOW

#define OCRC(_n)     __JOIN3(OCR, _n, C)     // Output Compare Register C
#define OCRCH(_n)    __JOIN3(OCR, _n, CH)    // Output Compare Register C HIGH
#define OCRCL(_n)    __JOIN3(OCR, _n, CL)    // Output Compare Register C LOW

#define ICR(_n)      __JOIN2(ICR, _n)        // Input Capture Register
#define ICRH(_n)     __JOIN3(ICR, _n, H)     // Input Capture Register HIGH
#define ICRL(_n)     __JOIN3(ICR, _n, L)     // Input Capture Register LOW

#define TCCRA(_n)    __JOIN3(TCCR, _n, A)    // Timer/Counter Control Register A
#define TCCRB(_n)    __JOIN3(TCCR, _n, B)    // Timer/Counter Control Register B
#define TCCRC(_n)    __JOIN3(TCCR, _n, C)    // Timer/Counter Control Register C
#define TIMSK(_n)    __JOIN2(TIMSK, _n)      // Timer/Counter Interrupt Mask Register
#define TIFR(_n)     __JOIN2(TIFR, _n)       // Timer/Counter Interrupt Flag Register

// TCCRA
#define COMA1(_n)    __JOIN3(COM, _n, A1)    // 7 Compare Output Mode for Channel A Bit 1
#define COMA0(_n)    __JOIN3(COM, _n, A0)    // 6 Compare Output Mode for Channel A Bit 0
#define COMB1(_n)    __JOIN3(COM, _n, B1)    // 5 Compare Output Mode for Channel B Bit 1
#define COMB0(_n)    __JOIN3(COM, _n, B0)    // 4 Compare Output Mode for Channel B Bit 0
#define COMC1(_n)    __JOIN3(COM, _n, C1)    // 3 Compare Output Mode for Channel C Bit 1
#define COMC0(_n)    __JOIN3(COM, _n, C0)    // 2 Compare Output Mode for Channel C Bit 0
#define WGM1(_n)     __JOIN3(WGM, _n, 1)     // 1 Waveform Generation Mode Bit 1
#define WGM0(_n)     __JOIN3(WGM, _n, 0)     // 0 Waveform Generation Mode Bit 0

// TCCRB
#define ICNC(_n)     __JOIN2(ICNC, _n)       // 7 Input Capture Noise Canceler
#define ICES(_n)     __JOIN2(ICES, _n)       // 6 Input Capture Edge Select
#define WGM3(_n)     __JOIN3(WGM, _n, 3)     // 4 Waveform Generation Mode Bit 3
#define WGM2(_n)     __JOIN3(WGM, _n, 2)     // 3 Waveform Generation Mode Bit 2
#define CS2(_n)      __JOIN3(CS, _n, 2)      // 2 Clock Select Bit 2
#define CS1(_n)      __JOIN3(CS, _n, 1)      // 1 Clock Select Bit 1
#define CS0(_n)      __JOIN3(CS, _n, 0)      // 0 Clock Select Bit 0

// TCCRC
#define FOCA(_n)     __JOIN3(FOC, _n, A)     // 7 Force Output Compare for Channel A
#define FOCB(_n)     __JOIN3(FOC, _n, B)     // 6 Force Output Compare for Channel B
#define FOCC(_n)     __JOIN3(FOC, _n, C)     // 5 Force Output Compare for Channel C

// TIMSK
#define ICIE(_n)     __JOIN2(ICIE, _n)       // 5 Timer/Counter Input Capture Interrupt Enable
#define OCIEC(_n)    __JOIN3(OCIE, _n, C)    // 3 Timer/Counter Output Compare C Match Interrupt Enable
#define OCIEB(_n)    __JOIN3(OCIE, _n, B)    // 2 Timer/Counter Output Compare B Match Interrupt Enable
#define OCIEA(_n)    __JOIN3(OCIE, _n, A)    // 1 Timer/Counter Output Compare A Match Interrupt Enable
#define TOIE(_n)     __JOIN2(TOIE, _n)       // 0 Timer/Counter Overflow Interrupt Enable

// TIFR
#define ICF(_n)     __JOIN2(ICF, _n)         // 5 Timer/Counter Input Capture Flag
#define OCFC(_n)    __JOIN3(OCF, _n, C)      // 3 Timer/Counter Output Compare C Match Flag
#define OCFB(_n)    __JOIN3(OCF, _n, B)      // 2 Timer/Counter Output Compare B Match Flag
#define OCFA(_n)    __JOIN3(OCF, _n, A)      // 1 Timer/Counter Output Compare A Match Flag
#define TOV(_n)     __JOIN2(TOV, _n)         // 0 Timer/Counter Overflow Flag

#define TIMER_CAPT_VECT(_n)   __JOIN3(TIMER, _n, _CAPT_vect)   // Timer/Counter Capture Event
#define TIMER_COMPA_VECT(_n)  __JOIN3(TIMER, _n, _COMPA_vect)  // Timer/Counter Compare Match A
#define TIMER_COMPB_VECT(_n)  __JOIN3(TIMER, _n, _COMPB_vect)  // Timer/Counter Compare Match B
#define TIMER_COMPC_VECT(_n)  __JOIN3(TIMER, _n, _COMPC_vect)  // Timer/Counter Compare Match C
#define TIMER_OVF_VECT(_n)    __JOIN3(TIMER, _n, _OVF_vect)    // Timer/Counter Overflow

#endif // __TOS_LIB_TIMER_H__

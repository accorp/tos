#ifndef __TOS_TASK_SHELL_H__
#define __TOS_TASK_SHELL_H__

#if ! defined(SHELL_SERIAL) \
 || ! defined(SHELL_BAUD) \
 || ! defined(SHELL_BUFFER_SIZE) \
 || ! ( defined(SHELL_COMMAND) || defined(SHELL_CMD_COUNT) ) \
 ||   ( defined(SHELL_COMMAND) && defined(SHELL_CMD_COUNT) )
	#error "not configured"
#endif

#ifndef __TOS_LIB_USART_H__
	#error "#include <tos/lib_usart.h>"
#endif

#ifndef __TOS_EXT_SYNC_TIMER_H__
	#error "#include <tos/ext_sync_timer.h>"
#endif

#ifndef __ASSEMBLER__
	
	MUTEX(shell_tx_mutex);
	NOINIT char* shell_tx_buffer;
	
	#define shell_tx_start(_msg) \
		do { \
			mutex_lock(shell_tx_mutex); \
			shell_tx_buffer = (_msg); \
			UCSRB(SHELL_SERIAL) |= (1 << UDRIE(SHELL_SERIAL)); \
		} while( 0 )
	
	#define shell_tx_stop() \
		do { \
			UCSRB(SHELL_SERIAL) &= ~(1 << UDRIE(SHELL_SERIAL)); \
			mutex_unlock(shell_tx_mutex); \
		} while( 0 )
	
	ISR(USART_UDRE_VECT(SHELL_SERIAL))
	{
		uint8_t tx_byte = *shell_tx_buffer ++;
		if( tx_byte == 0x00 )
		{
			shell_tx_stop();
		}
		else
		{
			UDR(SHELL_SERIAL) = tx_byte;
		}
	}
	
	void shell_print(char* msg)
	{
		if( !msg ) return;
		shell_tx_start(msg);
		mutex_wait(shell_tx_mutex);
	}
	
	void shell_print_eol()
	{
		char str[3];
		str[0] = '\r';
		str[1] = '\n';
		str[2] = '\0';
		shell_print(str);
	}
	
	void shell_print_line(char* msg)
	{
		shell_print(msg);
		shell_print_eol();
	}
	
	void shell_print_bool(bool value)
	{
		char str[6];
		if( value )
		{
			str[0] = 't';
			str[1] = 'r';
			str[2] = 'u';
			str[3] = 'e';
			str[4] = '\0';
		}
		else
		{
			str[0] = 'f';
			str[1] = 'a';
			str[2] = 'l';
			str[3] = 's';
			str[4] = 'e';
			str[5] = '\0';
		}
		shell_print(str);
	}
	
	void shell_print_int(int32_t value)
	{
		char str[12];
		ltoa(value, str, 10);
		shell_print(str);
	}
	
	void shell_print_hex(int32_t value)
	{
		char str[12];
		str[0] = '0';
		str[1] = 'x';
		ltoa(value, &str[2], 16);
		shell_print(str);
	}
	
	typedef union
	{
		uint32_t value;
		uint8_t  byte[4];
		uint8_t  chr;
	}
	shell_rx_code_t;
	
	EVENT(shell_rx_done);
	NOINIT shell_rx_code_t shell_rx_code;
	NOINIT uint8_t shell_rx_len;
	
	#define shell_rx_start() \
		do { \
			shell_rx_len = 0; \
			shell_rx_code.value = 0x00000000; \
			UCSRB(SHELL_SERIAL) |= (1 << RXCIE(SHELL_SERIAL)); \
		} while( 0 )
	
	#define shell_rx_stop() \
		do { \
			UCSRB(SHELL_SERIAL) &= ~(1 << RXCIE(SHELL_SERIAL)); \
		} while( 0 )
	
	ISR(USART_RX_VECT(SHELL_SERIAL))
	{
		uint8_t rx_byte = UDR(SHELL_SERIAL);
		if( shell_rx_len < 4 )
		{
			shell_rx_code.byte[shell_rx_len ++] = rx_byte;
		}
		if( (shell_rx_len == 1 && rx_byte != 0x1B )
		 || (shell_rx_len  > 2 && rx_byte >= 0x40 && rx_byte <= 0x7E) )
		{
			shell_rx_stop();
			event_fire(shell_rx_done);
		}
	}
	
	uint32_t shell_read()
	{
		shell_rx_start();
		event_wait_reset(shell_rx_done);
		return shell_rx_code.value;
	}
	
	bool shell_read_timer(uint32_t* code, time_t* timer)
	{
		shell_rx_start();
		if( event_wait_reset_timer(shell_rx_done, *timer) )
		{
			*code = shell_rx_code.value;
			return true;
		}
		else
		{
			shell_rx_stop();
			event_reset(shell_rx_done);
			return false;
		}
	}
	
	bool shell_read_timeout(uint32_t* code, uint32_t timeout)
	{
		time_t timer = time_add(time(), timeout);
		return shell_read_timer(code, &timer);
	}
	
	void shell_wait(uint32_t code)
	{
		while( shell_read() != code );
	}
	
	bool shell_wait_timer(uint32_t code, time_t* timer)
	{
		while( 1 )
		{
			uint32_t read;
			if( ! shell_read_timer(&read, timer) )
			{
				return false;
			}
			if( read == code )
			{
				return true;
			}
		}
	}
	
	bool shell_wait_timeout(uint32_t code, uint32_t timeout)
	{
		time_t timer = time_add(time(), timeout);
		return shell_wait_timer(code, &timer);
	}
	
	char* shell_next_arg(char** buffer)
	{
		char* ptr = *buffer;
		while( *ptr == ' ' || *ptr == '\t' )
		{
			ptr ++;
		}
		char* arg = ptr;
		while( *ptr != '\0' && *ptr != ' ' && *ptr != '\t' )
		{
			ptr ++;
		}
		*buffer = ptr;
		return arg;
	}
	
	#ifdef SHELL_CMD_COUNT
		
		typedef void (*cmd_exec_t) (uint8_t index, char* buffer);
		typedef struct
		{
			char* name;
			cmd_exec_t exec;
		}
		shell_cmd_t;
		
		extern shell_cmd_t shell_cmd_list[SHELL_CMD_COUNT];
		
		#define SHELL_CMD_LIST(...)  \
			shell_cmd_t shell_cmd_list[SHELL_CMD_COUNT] = { __VA_ARGS__ };
		
		#define SHELL_COMMAND  shell_command
		
		void shell_command(char* buffer)
		{
			char* arg = shell_next_arg(&buffer);
			uint8_t arg_len = ( buffer - arg );
			if( arg_len == 0 )
			{
				return;
			}
			if( arg_len == 4 && memcmp(arg, "help", 4) == 0 )
			{
				int8_t i;
				for( i = 0; i < SHELL_CMD_COUNT; i ++ )
				{
					shell_print("  * ");
					shell_print_line(shell_cmd_list[i].name);
				}
				return;
			}
			int8_t i;
			for( i = 0; i < SHELL_CMD_COUNT; i ++ )
			{
				char* name = shell_cmd_list[i].name;
				uint8_t name_len = strlen(name);
				if( arg_len == name_len && memcmp(arg, name, name_len) == 0 )
				{
					shell_cmd_list[i].exec(i, buffer);
					return;
				}
			}
			shell_print_line("unknown command");
		}
		
	#endif // SHELL_CMD_COUNT
	
	NOINIT char shell_cmd_buffer[SHELL_BUFFER_SIZE];
	
	THREAD(shell, 0x10)
	{
		DRIVER(PIN_RXD(SHELL_SERIAL, H), IN);
		DRIVER(PIN_TXD(SHELL_SERIAL, H), OUT);
		USART_INIT_8N1(SHELL_SERIAL);
		
		#define BAUD  SHELL_BAUD
		#include <util/setbaud.h>
		UBRR(SHELL_SERIAL) = UBRR_VALUE;
		#if USE_2X
			UCSRA(SHELL_SERIAL) |= (1 << U2X(SHELL_SERIAL));
		#endif
		#undef BAUD
		
		UCSRB(SHELL_SERIAL) |= (1 << RXEN(SHELL_SERIAL)) | (1 << TXEN(SHELL_SERIAL));
		
		memset(shell_cmd_buffer, 0x00, SHELL_BUFFER_SIZE);
		shell_print_eol();
		
		while( 1 )
		{
			shell_print("# \x1B[s");
			uint8_t shell_cmd_len = 0;
			while( 1 )
			{
				shell_rx_start();
				event_wait_reset(shell_rx_done);
				if( shell_rx_len > 1 )
				{
					switch( shell_rx_code.value )
					{
						case 0x00415B1B:
						case 0x41315B1B:
							if( shell_cmd_len == 0 )
							{
								shell_cmd_len = strlen(shell_cmd_buffer);
								shell_print(shell_cmd_buffer);
							}
							break;
						case 0x00425B1B:
						case 0x42315B1B:
							if( shell_cmd_len > 0 )
							{
								shell_cmd_len = 0;
								shell_print("\x1B[u\x1B[K");
							}
							break;
					}
					continue;
				}
				uint8_t chr = shell_rx_code.chr;
				if( chr == 0x0D )
				{
					shell_cmd_buffer[shell_cmd_len] = 0x00;
					break;
				}
				if( chr == 0x08 || chr == 0x7F )
				{
					if( shell_cmd_len > 0 )
					{
						shell_cmd_len --;
						shell_print("\x1B[D \x1B[D");
					}
					continue;
				}
				if( shell_cmd_len == SHELL_BUFFER_SIZE - 1 )
				{
					continue;
				}
				if( chr > 0x1F && chr < 0x7F )
				{
					shell_cmd_buffer[shell_cmd_len ++] = chr;
					UDR(SHELL_SERIAL) = chr;
				}
			}
			shell_print_eol();
			if( shell_cmd_len != 0 )
			{
				SHELL_COMMAND(shell_cmd_buffer);
			}
		}
	}
	
#endif // __ASSEMBLER__

#endif // __TOS_TASK_SHELL_H__

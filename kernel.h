#ifndef __TOS_KERNEL_H__
#define __TOS_KERNEL_H__

#ifndef THREAD_EXT_SIZE
	#error "#define THREAD_EXT_SIZE  [bytes]"
#endif

#ifndef STACK_BASE_SIZE
	#error "#define STACK_BASE_SIZE  [bytes]"
#endif

#include <avr/io.h>
#include <avr/interrupt.h>

#define __JOIN2(_a, _b)      _a ## _b
#define __JOIN3(_a, _b, _c)  _a ## _b ## _c

#define TASK_NAME(_name)  tos_task_ ## _name

#ifndef __ASSEMBLER__
	
	#include <stdint.h>
	#include <stdbool.h>
	#include <stdlib.h>
	#include <string.h>
	#include <math.h>
	
	#define NOINIT  __attribute__((section(".noinit")))
	#define PACKED  __attribute__((packed))
	
	typedef void (*thread_t) (void);
	
	typedef struct
	{
		uint8_t  id;
		thread_t ep;
		uint16_t sp;
		uint8_t  ext[THREAD_EXT_SIZE];
	}
	thread_entry_t;
	
	#define THREAD(_name, _size) \
		enum { _name ## _stack_size = _size }; \
		__attribute__((OS_task)) \
		__attribute__((noreturn)) \
		void TASK_NAME(_name)(void)
	
	#define KTASK(_name) \
		__attribute__((naked)) \
		__attribute__((section(".init9"))) \
		void TASK_NAME(_name)(void)
	
	#ifdef __AVR_3_BYTE_PC__
		#define STACK_MIN_SIZE  (3 + 12 + 3)
	#else
		#define STACK_MIN_SIZE  (2 + 12 + 2)
	#endif
	
	#define THREAD_LIST(...) \
		thread_entry_t thread_list[] = { __VA_ARGS__ }; \
		void *thread_ramend = (void*)THREAD_COUNT;
	
	#define THREAD_ENTRY(_name)  THREAD_ENTRY_EX(_name, 0)
	
	#define THREAD_ENTRY_EX(_name, _size) \
		{ \
			.id = 0x00, \
			.ep = &TASK_NAME(_name), \
			.sp = (STACK_MIN_SIZE + STACK_BASE_SIZE + _name ## _stack_size + _size), \
		}
	
	#define KERNEL_TASK(_name) \
		{ \
			.id = 0x01, \
			.ep = &TASK_NAME(_name), \
			.sp = 0x00, \
		}
	
	NOINIT thread_entry_t *active_thread;
	
	#define FIRST_THREAD  ( *(thread_entry_t*)(RAMEND - sizeof(thread_entry_t) + 1) )
	#define THREAD_COUNT  ( sizeof(thread_list) / sizeof(thread_entry_t) )
	#define THREAD_ID     ( active_thread->id )
	#define THREAD_INDEX  ( THREAD_COUNT - 1 - active_thread->id )
	
	void yield();
	void suspend();
	void resume(uint8_t index);
	#define resume_m(_index)  thread_list[_index].ep = &tos_restore
	
	void tos_restore();
	#define restore()  goto *(void*)&tos_restore
	
	void tos_next();
	#define next()  goto *(void*)&tos_next
	
#else // __ASSEMBLER__
	
	#define OFFSET_ID   0
	#define OFFSET_EP   1
	#define OFFSET_SP   3
	#define OFFSET_EXT  5
	
	#define SIZEOF_THREAD_ENTRY  (1 + 2 + 2 + THREAD_EXT_SIZE)
	
	#define TOS_SECTION  .init9
	
	#define sreg  _SFR_IO_ADDR(SREG)
	#define spl   _SFR_IO_ADDR(SPL)
	#define sph   _SFR_IO_ADDR(SPH)
	
	
	.section .init8
	
	
		ldi   r28, lo8(thread_list + SIZEOF_THREAD_ENTRY)
		ldi   r29, hi8(thread_list + SIZEOF_THREAD_ENTRY)
		ldi   r16, SIZEOF_THREAD_ENTRY
	do_save_first:
		ld    r0, -Y
		push  r0
		dec   r16
		brne  do_save_first
		sts   active_thread+0, r28
		sts   active_thread+1, r29
		std   Y+OFFSET_ID, r1
		sei
	
	
	.section .fini1
	
	
		ldi   r28, lo8(thread_list)
		ldi   r29, hi8(thread_list)
		ldi   r16, SIZEOF_THREAD_ENTRY
	do_restore_first:
		pop   r0
		st    Y+, r0
		dec   r16
		brne  do_restore_first
		cli
		lds   r16, thread_ramend+0
	#ifdef __AVR_3_BYTE_PC__
		ldi   r17, 0x00
	#endif
		ldi   r26, pm_lo8(tos_start_restore)
		ldi   r27, pm_hi8(tos_start_restore)
		ldi   r28, lo8(thread_list)
		ldi   r29, hi8(thread_list)
		ldi   r30, lo8(RAMEND)
		ldi   r31, hi8(RAMEND)
	tos_start_thread:
		dec   r16
		brmi  tos_start_exit
		ldd   r18, Y+OFFSET_ID
		tst   r18
		brne  tos_start_next
		ldd   r20, Y+OFFSET_EP+0
		ldd   r21, Y+OFFSET_EP+1
		ldd   r22, Y+OFFSET_SP+0
		ldd   r23, Y+OFFSET_SP+1
		out   spl, r30
		out   sph, r31
		push  r20
		push  r21
	#ifdef __AVR_3_BYTE_PC__
		push  r17
	#endif
		in    r24, spl
		in    r25, sph
		std   Y+OFFSET_EP+0, r26
		std   Y+OFFSET_EP+1, r27
		std   Y+OFFSET_SP+0, r24
		std   Y+OFFSET_SP+1, r25
		sub   r30, r22
		sbc   r31, r23
	tos_start_next:
		std   Y+OFFSET_ID,   r16
		adiw  r28, SIZEOF_THREAD_ENTRY
		rjmp  tos_start_thread
	tos_start_exit:
		sts   thread_ramend+0, r30
		sts   thread_ramend+1, r31
		sei
		jmp   tos_first
	
	
	.section TOS_SECTION
	
	
	tos_start_restore:
		sts   active_thread+0, r28
		sts   active_thread+1, r29
		ldd   r16, Y+OFFSET_SP+0
		ldd   r17, Y+OFFSET_SP+1
		cli
		out   spl, r16
		sei
		out   sph, r17
		ldi   r18, (1 << SREG_I)
		out   sreg, r18
		ret
	
	
	.global yield // void ()
	yield:
		ldi   r18, pm_lo8(tos_restore)
		ldi   r19, pm_hi8(tos_restore)
		ldi   r30, pm_lo8(tos_next)
		ldi   r31, pm_hi8(tos_next)
	// in  r18:19 => restore proc
	// in  r30:31 => return
	// out r28:29 => active thread
	tos_save:
		push  r29
		push  r28
		push  r17
		push  r16
		push  r15
		push  r14
		push  r13
		push  r12
		push  r11
		push  r10
		push  r9
		push  r8
		lds   r28, active_thread+0
		lds   r29, active_thread+1
		in    r16, spl
		in    r17, sph
		std   Y+OFFSET_EP+0, r18
		std   Y+OFFSET_EP+1, r19
		std   Y+OFFSET_SP+0, r16
		std   Y+OFFSET_SP+1, r17
		ijmp
	
	
	.global suspend // void ()
	suspend:
		ldi   r18, pm_lo8(tos_next)
		ldi   r19, pm_hi8(tos_next)
		ldi   r30, pm_lo8(tos_next)
		ldi   r31, pm_hi8(tos_next)
		rjmp  tos_save
	
	
	.global resume // void (uint8_t r24)
	resume:
		ldi   r18, pm_lo8(tos_restore)
		ldi   r19, pm_hi8(tos_restore)
		ldi   r28, lo8(thread_list)
		ldi   r29, hi8(thread_list)
		ldi   r25, SIZEOF_THREAD_ENTRY
		mul   r24, r25
		add   r28, r0
		adc   r29, r1
		clr   r1
		std   Y+OFFSET_EP+0, r18
		std   Y+OFFSET_EP+1, r19
		ret
	
	
	// in  r28:29 => active thread
	.global tos_restore
	tos_restore:
		sts   active_thread+0, r28
		sts   active_thread+1, r29
		ldd   r16, Y+OFFSET_SP+0
		ldd   r17, Y+OFFSET_SP+1
		cli
		out   spl, r16
		sei
		out   sph, r17
		pop   r8
		pop   r9
		pop   r10
		pop   r11
		pop   r12
		pop   r13
		pop   r14
		pop   r15
		pop   r16
		pop   r17
		pop   r28
		pop   r29
		ret
	
	
	// out r28:29 => active thread
	tos_first:
		ldi   r28, lo8(thread_list)
		ldi   r29, hi8(thread_list)
		ldd   r30, Y+OFFSET_EP+0
		ldd   r31, Y+OFFSET_EP+1
		ijmp
	
	
	// in  r28:29 => active thread
	// out r28:29 => active thread
	.global tos_next
	tos_next:
		ldd   r16, Y+OFFSET_ID
		tst   r16
		breq  tos_first
		adiw  r28, SIZEOF_THREAD_ENTRY
		ldd   r30, Y+OFFSET_EP+0
		ldd   r31, Y+OFFSET_EP+1
		ijmp
	
	
#endif // __ASSEMBLER__

#endif // __TOS_KERNEL_H__

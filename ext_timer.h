#ifndef __TOS_EXT_TIMER_H__
#define __TOS_EXT_TIMER_H__

#ifndef TIMER_VECT
	#error "#define TIMER_VECT  [isr_vector]"
#endif

#ifndef TIMER_FREQ
	#error "#define TIMER_FREQ  [Hz]"
#endif

#if THREAD_EXT_SIZE < 5
	#error "#define THREAD_EXT_SIZE  5"
#endif

#ifndef __ASSEMBLER__
	
	#define CEILDIV(_a, _b)  ( ( (_a) + (_b) - 1 ) / (_b) )
	
	#define  SEC(_t)         (TIMER_FREQ * (_t))
	#define MSEC(_t)  CEILDIV(TIMER_FREQ * (_t), 1000L)
	#define USEC(_t)  CEILDIV(TIMER_FREQ * (_t), 1000000L)
	
	typedef union
	{
		uint8_t  u8[5];
		uint16_t u16[2];
		uint32_t u32;
		uint64_t u40 :40;
	}
	time_t;
	
	#define TIME_NULL  { .u8 = { [0 ... 4] = 0 } }
	#define TIMER(_name)  time_t _name = TIME_NULL
	
	register uint8_t timereg_a   asm("r2");
	register uint8_t timereg_b   asm("r3");
	register uint8_t timereg_c   asm("r4");
	register uint8_t timereg_d   asm("r5");
	register uint8_t timereg_e   asm("r6");
	register uint8_t timereg_tmp asm("r7");
	
	time_t time();
	time_t __time_add(uint16_t time_lo, time_t timer, uint16_t time_hi);
	time_t __time_sub(uint16_t time_lo, time_t timer, uint16_t time_hi);
	#define time_add(_timer, _time)  __time_add((uint16_t)(_time), (_timer), (uint16_t)((_time) >> 16))
	#define time_sub(_timer, _time)  __time_sub((uint16_t)(_time), (_timer), (uint16_t)((_time) >> 16))
	
	void sleep(uint32_t time);
	void sleep_ex(uint32_t time, time_t *timer);
	void sleep_to_time(uint32_t time);
	void sleep_to_timer(time_t *timer);
	
#else // __ASSEMBLER__
	
	#define timereg_a    r2
	#define timereg_b    r3
	#define timereg_c    r4
	#define timereg_d    r5
	#define timereg_e    r6
	#define timereg_tmp  r7
	
	#define OFFSET_TIME  (OFFSET_EXT + 0)
	
	
	.section .init7
	
	
		mov   timereg_a, r1
		mov   timereg_b, r1
		mov   timereg_c, r1
		mov   timereg_d, r1
		mov   timereg_e, r1
	
	
	.section .text
	
	
	.global TIMER_VECT // void ()
	TIMER_VECT:
		in    timereg_tmp, sreg
		inc   timereg_a
		brne  timer_vect_ret
		inc   timereg_b
		brne  timer_vect_ret
		inc   timereg_c
		brne  timer_vect_ret
		inc   timereg_d
		brne  timer_vect_ret
		inc   timereg_e
		brne  timer_vect_ret
		jmp   0x0000
	timer_vect_ret:
		out   sreg, timereg_tmp
		reti
	
	
	.global time // time_t ()
	time:
		cli
		movw  r18, timereg_a
		movw  r20, timereg_c
		sei
		mov   r22, timereg_e
		ret
	
	
	.global __time_add // time_t (time_t 18:19:20:21:22, uint32_t 24:25:16:17);
	__time_add:
		add   r18, r24
		adc   r19, r25
		adc   r20, r16
		adc   r21, r17
		adc   r22, r1
		ret
	
	
	.global __time_sub // time_t (time_t 18:19:20:21:22, uint32_t 24:25:16:17);
	__time_sub:
		sub   r18, r24
		sbc   r19, r25
		sbc   r20, r16
		sbc   r21, r17
		sbc   r22, r1
		ret
	
	
	.section TOS_SECTION
	
	
	.global sleep // void (uint32_t 22:23:24:25)
	sleep:
		clr   r26
		cli
		add   r22, timereg_a
		adc   r23, timereg_b
		adc   r24, timereg_c
		adc   r25, timereg_d
		sei
		adc   r26, timereg_e
	sleep_save:
		ldi   r18, pm_lo8(sleep_restore)
		ldi   r19, pm_hi8(sleep_restore)
		ldi   r30, pm_lo8(sleep_save_ext)
		ldi   r31, pm_hi8(sleep_save_ext)
		rjmp  tos_save
	sleep_save_ext:
		std   Y+OFFSET_TIME+0, r22
		std   Y+OFFSET_TIME+1, r23
		std   Y+OFFSET_TIME+2, r24
		std   Y+OFFSET_TIME+3, r25
		std   Y+OFFSET_TIME+4, r26
		rjmp  tos_next
	
	
	.global sleep_ex // void (uint32_t 22:23:24:25, time_t* 20:21)
	sleep_ex:
		clr   r26
		movw  r30, r20
		ldd   r0, Z+0
		add   r22, r0
		std   Z+0, r22
		ldd   r0, Z+1
		adc   r23, r0
		std   Z+1, r23
		ldd   r0, Z+2
		adc   r24, r0
		std   Z+2, r24
		ldd   r0, Z+3
		adc   r25, r0
		std   Z+3, r25
		ldd   r0, Z+4
		adc   r26, r0
		std   Z+4, r26
		rjmp  sleep_save
	
	
	.global sleep_to_time // void (uint32_t 22:23:24:25)
	sleep_to_time:
		ldi   r26, 0x00
		rjmp  sleep_save
	
	
	.global sleep_to_timer // void (time_t* 24:25)
	sleep_to_timer:
		movw  r30, r24
		ldd   r22, Z+0
		ldd   r23, Z+1
		ldd   r24, Z+2
		ldd   r25, Z+3
		ldd   r26, Z+4
		rjmp  sleep_save
	
	
	sleep_restore:
		ldd   r22, Y+OFFSET_TIME+0
		ldd   r23, Y+OFFSET_TIME+1
		ldd   r24, Y+OFFSET_TIME+2
		ldd   r25, Y+OFFSET_TIME+3
		ldd   r26, Y+OFFSET_TIME+4
		cli
		cp    timereg_a, r22
		cpc   timereg_b, r23
		cpc   timereg_c, r24
		cpc   timereg_d, r25
		sei
		cpc   timereg_e, r26
		brcc  sleep_restore_do
		rjmp  tos_next
	sleep_restore_do:
		rjmp  tos_restore
	
	
#endif // __ASSEMBLER__

#endif // __TOS_EXT_TIMER_H__

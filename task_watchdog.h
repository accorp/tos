#ifndef __TOS_TASK_WATCHDOG_H__
#define __TOS_TASK_WATCHDOG_H__

#if ! defined(WATCHDOG_TIME)
	#error "not configured"
#endif

#ifndef __ASSEMBLER__
	
	#include <avr/wdt.h>
	
	NOINIT uint8_t watchdog_mcusr;
	#define is_reset_by_watchdog()  (watchdog_mcusr & (1 << WDRF))
	
	__attribute__((naked))
	__attribute__((section(".init3")))
	void watchdog_init(void)
	{
		register uint8_t r2 asm("r2");
		watchdog_mcusr = r2;
		wdt_disable();
	}
	
	void watchdog_start();
	
	KTASK(watchdog)
	{
		wdt_enable(WATCHDOG_TIME);
		goto *(void*)watchdog_start;
	}
	
#else // __ASSEMBLER__
	
	
	.section TOS_SECTION
	
	
	.global watchdog_start // void ()
	watchdog_start:
		ldi   r18, pm_lo8(watchdog_reset)
		ldi   r19, pm_hi8(watchdog_reset)
		std   Y+OFFSET_EP+0, r18
		std   Y+OFFSET_EP+1, r19
	watchdog_reset:
		wdr
		rjmp  tos_next
	
	
#endif // __ASSEMBLER__

#endif // __TOS_TASK_WATCHDOG_H__

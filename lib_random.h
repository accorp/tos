#ifndef __TOS_LIB_RANDOM_H__
#define __TOS_LIB_RANDOM_H__

#if ! defined(RANDOM_MUX)
	#error "not configured"
#endif

#if ! defined(__TOS_LIB_ADC_H__)
	#error "#include <tos/lib_adc.h>"
#endif

#ifndef __ASSEMBLER__
	
	#include <stdlib.h>
	
	#define random_u8()   ((uint8_t)((uint32_t)random() >> 16))
	#define random_u16()  ((uint16_t)((uint32_t)random() >> 8))
	
	void random_init()
	{
		uint32_t s = 0x00000000;
		uint8_t i;
		for( i = 0; i < 32; i ++ )
		{
			uint16_t adc = adc_read(RANDOM_MUX);
			s = (s << 1) | (adc & 0x0001);
		}
		srandom(s);
	}
	
#endif // __ASSEMBLER__

#endif // __TOS_LIB_RANDOM_H__

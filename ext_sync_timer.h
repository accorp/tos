#ifndef __TOS_EXT_SYNC_TIMER_H__
#define __TOS_EXT_SYNC_TIMER_H__

#include <tos/ext_sync_time.h>

#ifndef __ASSEMBLER__
	
	void sync_zero_timer(time_t *timer, sync_t *sync);
	void sync_nonzero_timer(time_t *timer, sync_t *sync);
	
	#define sync_waitzero_timer(_name, _timer) \
		(__extension__({ \
			if( (_name) ) { sync_zero_timer(&(_timer), &(_name)); } \
			!(_name); \
		}))
	
	#define sync_waitnonzero_timer(_name, _timer) \
		(__extension__({ \
			if( !(_name) ) { sync_nonzero_timer(&(_timer), &(_name)); } \
			(_name); \
		}))
	
	// event
	
	#define event_wait_timer(_name, _timer) \
		sync_waitnonzero_timer(_name, _timer)
	
	#define event_wait_reset_timer(_name, _timer) \
		(__extension__({ \
			uint8_t _success = sync_waitnonzero_timer(_name, _timer); \
			if( _success ) { (_name) = 0x00; } \
			(_success); \
		}))
	
	// mutex
	
	#define mutex_lock_timer(_name, _timer) \
		(__extension__({ \
			uint8_t _success = sync_waitzero_timer(_name, _timer); \
			if( _success ) { (_name) = 0xFF; } \
			(_success); \
		}))
	
	#define mutex_wait_timer(_name, _timer) \
		sync_waitzero_timer(_name, _timer)
	
	// semaphore
	
	#define semaphore_enter_timer(_name, _timer) \
		(__extension__({ \
			uint8_t _success = sync_waitnonzero_timer(_name, _timer); \
			if( _success ) { (_name) --; } \
			(_success); \
		}))
	
	#define semaphore_wait_timer(_name, _timer) \
		sync_waitnonzero_timer(_name, _timer)
	
	// multiple lock
	
	#define multi_wait_timer(_name, _timer) \
		sync_waitzero_timer(_name, _timer)
	
	#define multi_waitlock_timer(_name, _timer) \
		sync_waitnonzero_timer(_name, _timer)
	
	// switch
	
	#define switch_waiton_timer(_name, _timer) \
		sync_waitnonzero_timer(_name, _timer)
	
	#define switch_waitoff_timer(_name, _timer) \
		sync_waitzero_timer(_name, _timer)
	
	#define switch_off_waiton_timer(_name, _timer) \
		(__extension__({ \
			(_name) = 0x00; \
			sync_nonzero_timer(&(_timer), &(_name)); \
			(_name); \
		}))
	
	#define switch_on_waitoff_timer(_name, _timer) \
		(__extension__({ \
			(_name) = 0xFF; \
			sync_zero_timer(&(_timer), &(_name)); \
			!(_name); \
		}))
	
#else // __ASSEMBLER__
	
	
	.section TOS_SECTION
	
	
	.global sync_zero_timer // void (time_t* 24:25, sync_t* 22:23)
	sync_zero_timer:
		ldi   r18, pm_lo8(sync_zero_time_restore)
		ldi   r19, pm_hi8(sync_zero_time_restore)
		ldi   r30, pm_lo8(sync_timer_save)
		ldi   r31, pm_hi8(sync_timer_save)
		rjmp  tos_save
	
	
	.global sync_nonzero_timer // void (time_t* 24:25, sync_t* 22:23)
	sync_nonzero_timer:
		ldi   r18, pm_lo8(sync_nonzero_time_restore)
		ldi   r19, pm_hi8(sync_nonzero_time_restore)
		ldi   r30, pm_lo8(sync_timer_save)
		ldi   r31, pm_hi8(sync_timer_save)
		rjmp  tos_save
	
	
	sync_timer_save:
		std   Y+OFFSET_SYNC_TIME+0, r22
		std   Y+OFFSET_SYNC_TIME+1, r23
		movw  r30, r24
		ldd   r22, Z+0
		ldd   r23, Z+1
		ldd   r24, Z+2
		ldd   r25, Z+3
		ldd   r26, Z+4
		rjmp  sleep_save_ext
	
	
#endif // __ASSEMBLER__

#endif // __TOS_EXT_SYNC_TIMER_H__

#ifndef __TOS_LIB_ADC_H__
#define __TOS_LIB_ADC_H__

#if ! defined(ADC_ADPS) \
 || ! defined(ADC_YIELD) \
 || ! defined(ADC_SYNC)
	#error "not configured"
#endif

#if ADC_SYNC == 1 && ! defined(__TOS_EXT_SYNC_H__)
	#error "#include <tos/ext_sync.h>"
#endif

#ifndef __ASSEMBLER__
	
	#if ADC_SYNC == 1
		MUTEX(adc_mutex);
	#endif
	
	#if ADC_YIELD == 1
		#define adc_wait()  while( ADCSRA & (1 << ADSC) ) { yield(); }
	#else
		#define adc_wait()  while( ADCSRA & (1 << ADSC) ) { }
	#endif
	
	#define adc_get()     ADC
	#define adc_get_la()  ADCH
	
	void adc_start(uint8_t mux, bool left_adjust, bool free_run)
	{
		#if ADC_SYNC == 1
			mutex_lock(adc_mutex);
		#endif
		
		uint8_t adlar = ( left_adjust ? 1 : 0 ) << ADLAR;
		uint8_t adate = ( free_run    ? 1 : 0 ) << ADATE;
		
		#if defined(MUX5)
			#define ADC_MUX    ( (mux & 0b00011111) << MUX0 )
			#define ADC_MUX_B  ( (mux & 0b00100000) >> 5 << MUX5 )
		#elif defined(MUX4)
			#define ADC_MUX    ( (mux & 0b00011111) << MUX0 )
			#define ADC_MUX_B  0x00
		#elif defined(MUX3)
			#define ADC_MUX    ( (mux & 0b00001111) << MUX0 )
			#define ADC_MUX_B  0x00
		#endif
		
		ADMUX
			= ( 0 << REFS1 )  // 7 Reference Selection Bit 1
			| ( 1 << REFS0 )  // 6 Reference Selection Bit 0
			| ( adlar      )  // 5 ADC Left Adjust Result
			| ( ADC_MUX    ); // 0 Analog Channel and Gain Selection
		ADCSRB
			= ( 0 << ACME  )  // 6 Analog Comparator Multiplexer Enable
			| ( ADC_MUX_B  )  // 3 Analog Channel and Gain Selection
			| ( 0 << ADTS2 )  // 2 ADC Auto Trigger Source Bit 2
			| ( 0 << ADTS1 )  // 1 ADC Auto Trigger Source Bit 1
			| ( 0 << ADTS0 ); // 0 ADC Auto Trigger Source Bit 0
		ADCSRA
			= ( 1 << ADEN  )  // 7 ADC Enable
			| ( 1 << ADSC  )  // 6 ADC Start Conversion
			| ( adate      )  // 5 ADC Auto Trigger Enable
			| ( 1 << ADIF  )  // 4 ADC Interrupt Flag
			| ( 0 << ADIE  )  // 3 ADC Interrupt Enable
			| ( ADC_ADPS << ADPS0 );  // 2-0 ADC Prescaler Select
	}
	
	void adc_stop()
	{
		ADCSRA
			= ( 0 << ADEN  )  // 7 ADC Enable
			| ( 0 << ADSC  )  // 6 ADC Start Conversion
			| ( 0 << ADATE )  // 5 ADC Auto Trigger Enable
			| ( 0 << ADIF  )  // 4 ADC Interrupt Flag
			| ( 0 << ADIE  )  // 3 ADC Interrupt Enable
			| ( 0 << ADPS2 )  // 2 ADC Prescaler Select Bit 2
			| ( 0 << ADPS1 )  // 1 ADC Prescaler Select Bit 1
			| ( 0 << ADPS0 ); // 0 ADC Prescaler Select Bit 0
		
		#if ADC_SYNC == 1
			mutex_unlock(adc_mutex);
		#endif
	}
	
	uint16_t adc_read(uint8_t mux)
	{
		adc_start(mux, false, false);
		adc_wait();
		uint16_t adc = adc_get();
		adc_stop();
		return adc;
	}
	
#endif // __ASSEMBLER__

#endif // __TOS_LIB_ADC_H__

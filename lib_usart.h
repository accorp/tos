#ifndef __TOS_LIB_USART_H__
#define __TOS_LIB_USART_H__

#define PIN_RXD(_n, _L)  __JOIN2(PIN_RXD, _n)(_L)  // Receive pin
#define PIN_TXD(_n, _L)  __JOIN2(PIN_TXD, _n)(_L)  // Transmit pin
#define PIN_XCK(_n, _L)  __JOIN2(PIN_XCK, _n)(_L)  // Transfer Clock pin

#define UDR(_n)    __JOIN2(UDR, _n)          // USART I/O Data Register
#define UBRR(_n)   __JOIN2(UBRR, _n)         // USART Baud Rate Register
#define UBRRH(_n)  __JOIN3(UBRR, _n, H)      // USART Baud Rate Register HIGH
#define UBRRL(_n)  __JOIN3(UBRR, _n, L)      // USART Baud Rate Register LOW

#define UCSRA(_n)  __JOIN3(UCSR, _n, A)      // USART Control and Status Register A
#define UCSRB(_n)  __JOIN3(UCSR, _n, B)      // USART Control and Status Register B
#define UCSRC(_n)  __JOIN3(UCSR, _n, C)      // USART Control and Status Register C

// UCSRA
#define RXC(_n)    __JOIN2(RXC, _n)          // 7 Receive Complete
#define TXC(_n)    __JOIN2(TXC, _n)          // 6 Transmit Complete
#define UDRE(_n)   __JOIN2(UDRE, _n)         // 5 USART Data Register Empty
#define FE(_n)     __JOIN2(FE, _n)           // 4 Frame Error
#define DOR(_n)    __JOIN2(DOR, _n)          // 3 Data OverRun
#define UPE(_n)    __JOIN2(UPE, _n)          // 2 USART Parity Error
#define U2X(_n)    __JOIN2(U2X, _n)          // 1 USART Double Speed
#define MPCM(_n)   __JOIN2(MPCM, _n)         // 0 Multi-processor Communication Mode

// UCSRB
#define RXCIE(_n)  __JOIN2(RXCIE, _n)        // 7 RX Complete Interrupt Enable
#define TXCIE(_n)  __JOIN2(TXCIE, _n)        // 6 TX Complete Interrupt Enable
#define UDRIE(_n)  __JOIN2(UDRIE, _n)        // 5 USART Data Register Empty Interrupt Enable
#define RXEN(_n)   __JOIN2(RXEN, _n)         // 4 Receiver Enable
#define TXEN(_n)   __JOIN2(TXEN, _n)         // 3 Transmitter Enable
#define UCSZ2(_n)  __JOIN3(UCSZ, _n, 2)      // 2 USART Character Size Bit 2
#define RXB8(_n)   __JOIN2(RXB8, _n)         // 1 Receive Data Bit 8
#define TXB8(_n)   __JOIN2(TXB8, _n)         // 0 Transmit Data Bit 8

// UCSRC
#define UMSEL1(_n) __JOIN3(UMSEL, _n, 1)     // 7 USART Mode Select Bit 1
#define UMSEL0(_n) __JOIN3(UMSEL, _n, 0)     // 6 USART Mode Select Bit 0
#define UPM1(_n)   __JOIN3(UPM, _n, 1)       // 5 USART Parity Mode Bit 1
#define UPM0(_n)   __JOIN3(UPM, _n, 0)       // 4 USART Parity Mode Bit 0
#define USBS(_n)   __JOIN2(USBS, _n)         // 3 USART Stop Bit Select
#define UCSZ1(_n)  __JOIN3(UCSZ, _n, 1)      // 2 USART Character Size Bit 1
#define UCSZ0(_n)  __JOIN3(UCSZ, _n, 0)      // 1 USART Character Size Bit 0
#define UCPOL(_n)  __JOIN2(UCPOL, _n)        // 0 USART Clock Polarity

#define USART_RX_VECT(_n)    __JOIN3(USART, _n, _RX_vect)    // USART, Rx Complete
#define USART_TX_VECT(_n)    __JOIN3(USART, _n, _TX_vect)    // USART, Tx Complete
#define USART_UDRE_VECT(_n)  __JOIN3(USART, _n, _UDRE_vect)  // USART Data register Empty

#ifndef USART0_RX_vect
	#define USART0_RX_vect    USART_RX_vect
	#define USART0_TX_vect    USART_TX_vect
	#define USART0_UDRE_vect  USART_UDRE_vect
#endif

#ifndef __ASSEMBLER__
	
	// 8 data bits, no parity, one stop bit.
	#define USART_INIT_8N1(_n) \
		do { \
			UCSRA(_n) \
				= ( 0 << RXC(_n)    ) \
				| ( 0 << TXC(_n)    ) \
				| ( 0 << UDRE(_n)   ) \
				| ( 0 << FE(_n)     ) \
				| ( 0 << DOR(_n)    ) \
				| ( 0 << UPE(_n)    ) \
				| ( 0 << U2X(_n)    ) \
				| ( 0 << MPCM(_n)   ); \
			UCSRB(_n) \
				= ( 0 << RXCIE(_n)  ) \
				| ( 0 << TXCIE(_n)  ) \
				| ( 0 << UDRIE(_n)  ) \
				| ( 0 << RXEN(_n)   ) \
				| ( 0 << TXEN(_n)   ) \
				| ( 0 << UCSZ2(_n)  ) \
				| ( 0 << RXB8(_n)   ) \
				| ( 0 << TXB8(_n)   ); \
			UCSRC(_n) \
				= ( 0 << UMSEL1(_n) ) \
				| ( 0 << UMSEL0(_n) ) \
				| ( 0 << UPM1(_n)   ) \
				| ( 0 << UPM0(_n)   ) \
				| ( 0 << USBS(_n)   ) \
				| ( 1 << UCSZ1(_n)  ) \
				| ( 1 << UCSZ0(_n)  ) \
				| ( 0 << UCPOL(_n)  ); \
		} while( 0 )
	
#endif // __ASSEMBLER__

#endif // __TOS_LIB_USART_H__

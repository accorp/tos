#ifndef __TOS_LIB_FLOAT16_H__
#define __TOS_LIB_FLOAT16_H__

#ifndef __ASSEMBLER__
	
	typedef union
	{
		uint16_t u16;
		struct
		{
			uint16_t frac : 10;
			uint8_t  exp  :  5;
			uint8_t  sign :  1;
		};
	}
	half_t;
	
	typedef union
	{
		float    f32;
		uint32_t u32;
		struct
		{
			uint16_t frac_l : 12;
			uint8_t  frac_r : 1;
			uint16_t frac_h : 10;
			uint8_t  exp    :  8;
			uint8_t  sign   :  1;
		};
	}
	single_t;
	
	half_t single_to_half(float single)
	{
		single_t s = { .f32 = single };
		half_t half;
		uint16_t frac;
		int16_t exp;
		
		if( single < -65520.0 || 65520.0 < single )
		{
			exp = 0b11111;
			frac = 0;
		}
		else
		{
			if( s.exp == 0 )
			{
				exp = 0;
				frac = 0;
			}
			else if( s.exp == 0b11111111 )
			{
				exp = 0b11111;
				frac = s.frac_h;
			}
			else
			{
				exp = (int16_t)s.exp - 127;
				if( exp > 15 )
				{
					exp = 0b11111;
					frac = 0;
				}
				else if( exp < -14 )
				{
					exp = 0;
					frac = 0;
				}
				else
				{
					exp = exp + 15;
					frac = s.frac_r + s.frac_h;
					if( frac > 0b1111111111 )
					{
						frac = 0b1111111111;
					}
				}
			}
		}
		
		half.frac = frac;
		half.exp = exp;
		half.sign = s.sign;
		
		return half;
	}
	
	float half_to_single(half_t half)
	{
		single_t s;
		
		s.frac_l = 0;
		s.frac_r = 0;
		s.frac_h = half.frac;
		
		if( half.exp == 0 )
		{
			s.exp = 0;
		}
		else if( half.exp == 0b11111 )
		{
			s.exp = 0b11111111;
		}
		else
		{
			s.exp = (int16_t)half.exp - 15 + 127;
		}
		
		s.sign = half.sign;
		
		return s.f32;
	}
	
#endif // __ASSEMBLER__

#endif // __TOS_LIB_FLOAT16_H__

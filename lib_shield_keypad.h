#ifndef __TOS_LIB_SHIELD_KEYPAD_H__
#define __TOS_LIB_SHIELD_KEYPAD_H__

#if ! defined(SHIELD_KEYPAD_MUX)
	#error "not configured"
#endif

#ifndef __ASSEMBLER__
	
	#define KEY_NONE    0  // 255
	#define KEY_SELECT  1  // 180
	#define KEY_LEFT    2  // 120
	#define KEY_DOWN    3  // 76
	#define KEY_UP      4  // 32
	#define KEY_RIGHT   5  // 0
	
	uint8_t keypad_state()
	{
		uint8_t count[6] =
		{
			[KEY_NONE]   = 0,
			[KEY_SELECT] = 0,
			[KEY_LEFT]   = 0,
			[KEY_DOWN]   = 0,
			[KEY_UP]     = 0,
			[KEY_RIGHT]  = 0
		};
		uint8_t result = KEY_NONE;
		adc_start(SHIELD_KEYPAD_MUX, true, true);
		uint8_t i;
		for( i = 0; i < 5; i ++ )
		{
			sleep(MSEC(5));
			uint8_t adc = adc_get_la();
			uint8_t key;
			/**/ if( adc > 217 ) key = KEY_NONE;
			else if( adc > 150 ) key = KEY_SELECT;
			else if( adc >  98 ) key = KEY_LEFT;
			else if( adc >  54 ) key = KEY_DOWN;
			else if( adc >  16 ) key = KEY_UP;
			else                 key = KEY_RIGHT;
			count[key] ++;
			if( count[result] < count[key] )
			{
				result = key;
			}
		}
		adc_stop();
		return result;
	}
	
	bool keypad_repeat = false;
	uint8_t keypad_last = KEY_NONE;
	
	uint8_t keypad_get(bool can_repeat)
	{
		uint8_t result;
		if( can_repeat )
		{
			result = keypad_state();
			if( result == keypad_last )
			{
				uint8_t c = ( keypad_repeat ? 3 : 12 );
				do
				{
					result = keypad_state();
					keypad_repeat = ( result == keypad_last );
				}
				while( keypad_repeat && --c );
				if( result == KEY_NONE && c < 2 )
				{
					result = keypad_last;
					keypad_last = 0xFF;
					return result;
				}
			}
			else
			{
				keypad_repeat = false;
			}
		}
		else
		{
			do
			{
				result = keypad_state();
			}
			while( result == keypad_last );
		}
		while( result == KEY_NONE )
		{
			result = keypad_state();
		}
		keypad_last = result;
		return result;
	}
	
#endif // __ASSEMBLER__

#endif // __TOS_LIB_SHIELD_KEYPAD_H__

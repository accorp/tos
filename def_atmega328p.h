#ifndef __TOS_DEF_ATMEGA328P_H__
#define __TOS_DEF_ATMEGA328P_H__

#ifndef __AVR_ATmega328P__
	#error "MCU is not ATmega328P"
#endif

#ifndef __TOS_LIB_PIN_H__
	#error "#include <tos/lib_pin.h>"
#endif

// TIMER

#define PIN_OC0A(_A)     __JOIN2(D6, _A)
#define PIN_OC0B(_A)     __JOIN2(D5, _A)
#define PIN_OC1A(_A)     __JOIN2(B1, _A)
#define PIN_OC1B(_A)     __JOIN2(B2, _A)
#define PIN_ICP1(_A)     __JOIN2(B0, _A)
#define PIN_OC2A(_A)     __JOIN2(B3, _A)
#define PIN_OC2B(_A)     __JOIN2(D3, _A)

// USART

#define PIN_RXD0(_A)      __JOIN2(D0, _A)
#define PIN_TXD0(_A)      __JOIN2(D1, _A)
#define PIN_XCK0(_A)      __JOIN2(D4, _A)

// SPI

#define PIN_SCK(_A)      __JOIN2(B5, _A)
#define PIN_SS(_A)       __JOIN2(B2, _A)
#define PIN_MOSI(_A)     __JOIN2(B3, _A)
#define PIN_MISO(_A)     __JOIN2(B4, _A)

// TWI

#define PIN_SDA(_A)      __JOIN2(C4, _A)
#define PIN_SCL(_A)      __JOIN2(C5, _A)

// INT

#define PIN_INT0(_A)     __JOIN2(D2, _A)
#define PIN_INT1(_A)     __JOIN2(D3, _A)

// PCINT

#define PIN_PCINT0(_A)   __JOIN2(B0, _A)
#define PIN_PCINT1(_A)   __JOIN2(B1, _A)
#define PIN_PCINT2(_A)   __JOIN2(B2, _A)
#define PIN_PCINT3(_A)   __JOIN2(B3, _A)
#define PIN_PCINT4(_A)   __JOIN2(B4, _A)
#define PIN_PCINT5(_A)   __JOIN2(B5, _A)
#define PIN_PCINT6(_A)   __JOIN2(B6, _A)
#define PIN_PCINT7(_A)   __JOIN2(B7, _A)
#define PIN_PCINT8(_A)   __JOIN2(C0, _A)
#define PIN_PCINT9(_A)   __JOIN2(C1, _A)
#define PIN_PCINT10(_A)  __JOIN2(C2, _A)
#define PIN_PCINT11(_A)  __JOIN2(C3, _A)
#define PIN_PCINT12(_A)  __JOIN2(C4, _A)
#define PIN_PCINT13(_A)  __JOIN2(C5, _A)
#define PIN_PCINT14(_A)  __JOIN2(C6, _A)
#define PIN_PCINT16(_A)  __JOIN2(D0, _A)
#define PIN_PCINT17(_A)  __JOIN2(D1, _A)
#define PIN_PCINT18(_A)  __JOIN2(D2, _A)
#define PIN_PCINT19(_A)  __JOIN2(D3, _A)
#define PIN_PCINT20(_A)  __JOIN2(D4, _A)
#define PIN_PCINT21(_A)  __JOIN2(D5, _A)
#define PIN_PCINT22(_A)  __JOIN2(D6, _A)
#define PIN_PCINT23(_A)  __JOIN2(D7, _A)

// ADC

#define PIN_ADC0(_A)     __JOIN2(C0, _A)
#define PIN_ADC1(_A)     __JOIN2(C1, _A)
#define PIN_ADC2(_A)     __JOIN2(C2, _A)
#define PIN_ADC3(_A)     __JOIN2(C3, _A)
#define PIN_ADC4(_A)     __JOIN2(C4, _A)
#define PIN_ADC5(_A)     __JOIN2(C5, _A)

#endif // __TOS_DEF_ATMEGA328P_H__

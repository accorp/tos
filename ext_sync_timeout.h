#ifndef __TOS_EXT_SYNC_TIMEOUT_H__
#define __TOS_EXT_SYNC_TIMEOUT_H__

#include <tos/ext_sync_time.h>

#ifndef __ASSEMBLER__
	
	void sync_zero_timeout(uint32_t timeout, sync_t *sync);
	void sync_nonzero_timeout(uint32_t timeout, sync_t *sync);
	
	#define sync_waitzero_timeout(_name, _time) \
		(__extension__({ \
			if( (_name) ) { sync_zero_timeout((_time), &(_name)); } \
			!(_name); \
		}))
	
	#define sync_waitnonzero_timeout(_name, _time) \
		(__extension__({ \
			if( !(_name) ) { sync_nonzero_timeout((_time), &(_name)); } \
			(_name); \
		}))
	
	// event
	
	#define event_wait_timeout(_name, _time) \
		sync_waitnonzero_timeout(_name, _time)
	
	#define event_wait_reset_timeout(_name, _time) \
		(__extension__({ \
			uint8_t _success = sync_waitnonzero_timeout(_name, _time); \
			if( _success ) { (_name) = 0x00; } \
			(_success); \
		}))
	
	// mutex
	
	#define mutex_lock_timeout(_name, _time) \
		(__extension__({ \
			uint8_t _success = sync_waitzero_timeout(_name, _time); \
			if( _success ) { (_name) = 0xFF; } \
			(_success); \
		}))
	
	#define mutex_wait_timeout(_name, _time) \
		sync_waitzero_timeout(_name, _time)
	
	// semaphore
	
	#define semaphore_enter_timeout(_name, _time) \
		(__extension__({ \
			uint8_t _success = sync_waitnonzero_timeout(_name, _time); \
			if( _success ) { (_name) --; } \
			(_success); \
		}))
	
	#define semaphore_wait_timeout(_name, _time) \
		sync_waitnonzero_timeout(_name, _time)
	
	// multiple lock
	
	#define multi_wait_timeout(_name, _time) \
		sync_waitzero_timeout(_name, _time)
	
	#define multi_waitlock_timeout(_name, _time) \
		sync_waitnonzero_timeout(_name, _time)
	
	// switch
	
	#define switch_waiton_timeout(_name, _time) \
		sync_waitnonzero_timeout(_name, _time)
	
	#define switch_waitoff_timeout(_name, _time) \
		sync_waitzero_timeout(_name, _time)
	
	#define switch_off_waiton_timeout(_name, _time) \
		(__extension__({ \
			(_name) = 0x00; \
			sync_nonzero_timeout((_time), &(_name)); \
			(_name); \
		}))
	
	#define switch_on_waitoff_timeout(_name, _time) \
		(__extension__({ \
			(_name) = 0xFF; \
			sync_zero_timeout((_time), &(_name)); \
			!(_name); \
		}))
	
#else // __ASSEMBLER__
	
	
	.section TOS_SECTION
	
	
	.global sync_zero_timeout // void (uint32_t 22:23:24:25, sync_t* 20:21)
	sync_zero_timeout:
		ldi   r18, pm_lo8(sync_zero_time_restore)
		ldi   r19, pm_hi8(sync_zero_time_restore)
		ldi   r30, pm_lo8(sync_timeout_save)
		ldi   r31, pm_hi8(sync_timeout_save)
		rjmp  tos_save
	
	
	.global sync_nonzero_timeout // void (uint32_t 22:23:24:25, sync_t* 20:21)
	sync_nonzero_timeout:
		ldi   r18, pm_lo8(sync_nonzero_time_restore)
		ldi   r19, pm_hi8(sync_nonzero_time_restore)
		ldi   r30, pm_lo8(sync_timeout_save)
		ldi   r31, pm_hi8(sync_timeout_save)
		rjmp  tos_save
	
	
	sync_timeout_save:
		std   Y+OFFSET_SYNC_TIME+0, r20
		std   Y+OFFSET_SYNC_TIME+1, r21
		clr   r26
		cli
		add   r22, timereg_a
		adc   r23, timereg_b
		adc   r24, timereg_c
		adc   r25, timereg_d
		sei
		adc   r26, timereg_e
		rjmp  sleep_save_ext
	
	
#endif // __ASSEMBLER__

#endif // __TOS_EXT_SYNC_TIMEOUT_H__

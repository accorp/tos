#ifndef __TOS_LIB_EEPROM_H__
#define __TOS_LIB_EEPROM_H__

#if ! defined(EEPROM_DATA_BEGIN) \
 || ! defined(EEPROM_DATA_END) \
 || ! defined(EEPROM_DATA_SIZE) \
 || ! defined(EEPROM_SYNC) \
 || ! defined(EEPROM_CRC)
	#error "not configured"
#endif

#if EEPROM_CRC != 0
	#ifndef __TOS_LIB_CRC_H__
		#error "#include <tos/lib_crc.h>"
	#endif
#endif

#ifndef __ASSEMBLER__
	
	#include <stdlib.h>
	
	#if (0 <= EEPROM_CRC) && (EEPROM_CRC <= 2)
		#define EEPROM_STORE_SIZE  ( 1 + EEPROM_DATA_SIZE + EEPROM_CRC )
	#else
		#error "#define EEPROM_CRC  [0 .. 2]"
	#endif
	
	typedef union
	{
		uint8_t byte;
		struct
		{
			uint8_t a : 4;
			uint8_t b : 4;
		};
	}
	eeprom_mark_t;
	
	NOINIT uint16_t eeprom_data_offset;
	
	#if EEPROM_SYNC == 1
		MUTEX(eeprom_mutex);
	#endif
	
	#define eeprom_read_byte(_offset) \
		(__extension__({ \
			EEAR = (_offset); \
			EECR |= (1 << EERE); \
			EEDR; \
		}))
	
	void eeprom_write_byte(uint16_t offset, uint8_t byte);
	
	bool eeprom_init()
	{
		uint16_t offset = EEPROM_DATA_BEGIN;
		while( offset <= EEPROM_DATA_END - EEPROM_STORE_SIZE + 1 )
		{
			eeprom_mark_t mark = {.byte = eeprom_read_byte(offset)};
			if( mark.a != mark.b )
			{
				eeprom_data_offset = offset;
				return true;
			}
			offset += EEPROM_STORE_SIZE;
		}
		eeprom_data_offset = EEPROM_DATA_END;
		return false;
	}
	
	bool eeprom_read(void* data)
	{
		#if EEPROM_SYNC == 1
			mutex_lock(eeprom_mutex);
		#endif
		uint16_t count = EEPROM_DATA_SIZE;
		uint16_t offset = eeprom_data_offset;
		uint8_t* ptr = data;
		while( count -- )
		{
			*ptr ++ = eeprom_read_byte(++ offset);
		}
		#if EEPROM_CRC == 1
			uint8_t crc = eeprom_read_byte(++ offset);
		#elif EEPROM_CRC == 2
			uint8_t crc_lo = eeprom_read_byte(++ offset);
			uint8_t crc_hi = eeprom_read_byte(++ offset);
		#endif
		#if EEPROM_SYNC == 1
			mutex_unlock(eeprom_mutex);
		#endif
		#if EEPROM_CRC == 0
			return true;
		#elif EEPROM_CRC == 1
			return( crc8_calc(CDMA2000, data, EEPROM_DATA_SIZE) == crc );
		#elif EEPROM_CRC == 2
			return( crc16_calc(CDMA2000, data, EEPROM_DATA_SIZE) == (uint16_t)((crc_lo << 0) | (crc_hi << 8)) );
		#endif
	}
	
	void eeprom_write(void* data)
	{
		#if EEPROM_SYNC == 1
			mutex_lock(eeprom_mutex);
		#endif
		uint16_t next_offset = eeprom_data_offset + EEPROM_STORE_SIZE;
		if( next_offset > EEPROM_DATA_END - EEPROM_STORE_SIZE + 1 )
		{
			next_offset = EEPROM_DATA_BEGIN;
		}
		uint16_t count = EEPROM_DATA_SIZE;
		uint16_t offset = next_offset;
		uint8_t* ptr = data;
		while( count -- )
		{
			eeprom_write_byte(++ offset, *ptr ++);
		}
		#if EEPROM_CRC == 1
			eeprom_write_byte(++ offset, crc8_calc(CDMA2000, data, EEPROM_DATA_SIZE));
		#elif EEPROM_CRC == 2
			uint16_t crc = crc16_calc(CDMA2000, data, EEPROM_DATA_SIZE);
			eeprom_write_byte(++ offset, (crc >> 0));
			eeprom_write_byte(++ offset, (crc >> 8));
		#endif
		
		eeprom_mark_t mark;
		
		mark.byte = eeprom_read_byte(next_offset);
		mark.a = mark.a & mark.b;
		mark.b = mark.a;
		if( mark.byte == 0x00 )
		{
			mark.byte = 0b11111110;
		}
		else
		{
			if( mark.a & 0b0001 )
			{
				mark.a &= 0b1110;
			}
			else if( mark.a & 0b0010 )
			{
				mark.a &= 0b1101;
			}
			else if( mark.a & 0b0100 )
			{
				mark.a &= 0b1011;
			}
			else if( mark.a & 0b1000 )
			{
				mark.a &= 0b0111;
			}
		}
		eeprom_write_byte(next_offset, mark.byte);
		
		mark.byte = eeprom_read_byte(eeprom_data_offset);
		mark.a = mark.a & mark.b;
		mark.b = mark.a;
		eeprom_write_byte(eeprom_data_offset, mark.byte);
		
		eeprom_data_offset = next_offset;
		#if EEPROM_SYNC == 1
			mutex_unlock(eeprom_mutex);
		#endif
	}
	
#else // __ASSEMBLER__
	
	#define eecr   _SFR_IO_ADDR(EECR)
	#define eedr   _SFR_IO_ADDR(EEDR)
	#define eearl  _SFR_IO_ADDR(EEARL)
	#define eearh  _SFR_IO_ADDR(EEARH)
	
	
	.section .text
	
	
	.global eeprom_write_byte // void (uint16_t 24:25, uint8_t 22)
	eeprom_write_byte:
		out   eearh, r25
		out   eearl, r24
		sbi   eecr, EERE
		in    r23, eedr
		cp    r22, r23
		breq  eeprom_write_byte_ret
		out   eedr, r22
		ldi   r26, (0b01 << EEPM0)
		cpi   r22, 0xFF
		breq  eeprom_write_byte_start
		ldi   r26, (0b10 << EEPM0)
		and   r23, r22
		cp    r22, r23
		breq  eeprom_write_byte_start
		ldi   r26, (0b00 << EEPM0)
	eeprom_write_byte_start:
		out   eecr, r26
		cli
		sbi   eecr, EEMPE
		sei
		sbi   eecr, EEPE
		jmp   eeprom_wait
	eeprom_write_byte_ret:
		ret
	
	
	.section TOS_SECTION
	
	
	.global eeprom_wait // void ()
	eeprom_wait:
		ldi   r18, pm_lo8(eeprom_restore)
		ldi   r19, pm_hi8(eeprom_restore)
		ldi   r30, pm_lo8(tos_next)
		ldi   r31, pm_hi8(tos_next)
		rjmp  tos_save
	
	
	eeprom_restore:
		sbic  eecr, EEPE
		rjmp  tos_next
		rjmp  tos_restore
	
	
#endif // __ASSEMBLER__

#endif // __TOS_LIB_EEPROM_H__

#ifndef __TOS_EXT_SYNC_TIME_H__
#define __TOS_EXT_SYNC_TIME_H__

#ifndef __TOS_EXT_SYNC_H__
	#error "#include <tos/ext_sync.h>"
#endif

#if THREAD_EXT_SIZE < 7
	#error "#define THREAD_EXT_SIZE  7"
#endif

#ifndef __TOS_EXT_TIMER_H__
	#error "#include <tos/ext_timer.h>"
#endif

#ifdef __ASSEMBLER__
	
	#define OFFSET_SYNC_TIME  (OFFSET_TIME + 5)
	
	
	.section TOS_SECTION
	
	
	sync_zero_time_restore:
		ldd   r26, Y+OFFSET_SYNC_TIME+0
		ldd   r27, Y+OFFSET_SYNC_TIME+1
		ld    r18, X
		tst   r18
		breq  sync_time_restore
		rjmp  sleep_restore
	
	
	sync_nonzero_time_restore:
		ldd   r26, Y+OFFSET_SYNC_TIME+0
		ldd   r27, Y+OFFSET_SYNC_TIME+1
		ld    r18, X
		tst   r18
		brne  sync_time_restore
		rjmp  sleep_restore
	
	
	sync_time_restore:
		rjmp  tos_restore
	
	
#endif // __ASSEMBLER__

#endif // __TOS_EXT_SYNC_TIME_H__

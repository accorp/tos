#ifndef __TOS_LIB_PCF857X_H__
#define __TOS_LIB_PCF857X_H__

#if ! defined(__TOS_LIB_TWI_H__)
	#error "#include <tos/lib_twi.h>"
#endif

#ifndef __ASSEMBLER__
	
	// PCF8574
	
	#define PCF8574_ADDR_BASE  ( 0b0100 << 3 )
	
	bool pcf8574_write(uint8_t addr, uint8_t port)
	{
		if( !twi_start(PCF8574_ADDR_BASE | addr, TWI_WRITE) ) return false;
		if( !twi_write(port) ) return false;
		twi_stop();
		return true;
	}
	
	
	// PCF8575
	
	#define PCF8575_ADDR_BASE  ( 0b0100 << 3 )
	
	bool pcf8575_write(uint8_t addr, uint8_t port_0, uint8_t port_1)
	{
		if( !twi_start(PCF8575_ADDR_BASE | addr, TWI_WRITE) ) return false;
		if( !twi_write(port_0) ) return false;
		if( !twi_write(port_1) ) return false;
		twi_stop();
		return true;
	}
	
#endif // __ASSEMBLER__

#endif // __TOS_LIB_PCF857X_H__
